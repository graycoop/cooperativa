<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEstadoCivil extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOESTADOCIVIL';
    protected $primaryKey   = 'ID_TIPOESTADOCIVIL';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOESTADOCIVIL', 'TIPO_ESTADOCIVIL', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
