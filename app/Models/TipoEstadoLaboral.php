<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEstadoLaboral extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOESTADOLABORAL';
    protected $primaryKey   = 'ID_TIPOESTADOLABORAL';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOESTADOLABORAL', 'TIPO_ESTADOLABORAL', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
