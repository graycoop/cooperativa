<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuUsers extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'menus_users';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'user_id', 'vue_name',
    ];

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
}
