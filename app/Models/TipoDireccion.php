<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDireccion extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPODIRECCION';
    protected $primaryKey   = 'ID_TIPODIRECCION';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPODIRECCION', 'TIPO_DIRECCION', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
