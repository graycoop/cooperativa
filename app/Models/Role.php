<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'roles';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'name', 'description', 'id_estado',
    ];

    public function users()
	{
	    return $this
	        ->belongsToMany('App\Models\User')
	        ->withTimestamps();
	}
}
