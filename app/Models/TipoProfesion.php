<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoProfesion extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOPROFESION';
    protected $primaryKey   = 'ID_TIPOPROFESION';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOPROFESION', 'TIPO_PROFESION', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
