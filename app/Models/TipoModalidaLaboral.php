<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoModalidaLaboral extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOMODALIDADLABORAL';
    protected $primaryKey   = 'ID_TIPOMODALIDADLABORAL';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOACTIVIDADECONOMICA', 'TIPO_MODALIDADLABORAL', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
