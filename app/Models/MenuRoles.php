<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuRoles extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'menus_roles';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'role_id', 'vue_name',
    ];

    public function roles(){
        return $this->belongsToMany('App\Models\Role');
    }
}
