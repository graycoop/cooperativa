<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDocIdentidad extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPODOCIDENTIDAD';
    protected $primaryKey   = 'ID_TIPODOCIDENTIDAD';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPODOCIDENTIDAD', 'TIPO_DOCIDENTIDAD', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
