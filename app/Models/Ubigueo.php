<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ubigueo extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'UBIGEO';
    public $timestamps      = false;
    public $incrementing    = false;

    protected $fillable = [
        'DPTO', 'PROV', 'DIST', 'UBIGEO',
    ];
}
