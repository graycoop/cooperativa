<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'permissions';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'name', 'description',
    ];

    public function users()
    {
        return $this
            ->belongsToMany('App\Models\User')
            ->withTimestamps();
    }
}
