<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CentroLaboral extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'CENTROLABORAL';
    protected $primaryKey   = 'ID_CENTROLABORAL';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_CENTROLABORAL', 'ID_PERSONA', 'ID_ORGANIZACION', 'ID_AGENCIA', 'ID_ROL', 'FECHA_INGRESO', 'ID_TIPOCATEGORIA', 'AREA', 'CARGO', 'CODIGO', 'ID_TIPOESTADOLABORAL', 'PRINCIPAL', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];

    public function organizacion(){
        return $this->hasOne('App\Models\Organizaciones', 'ID_ORGANIZACION','ID_ORGANIZACION');
    }

    public function categorialaboral(){
        return $this->hasOne('App\Models\TipoCategoriaLaboral', 'ID_TIPOCATEGORIA','ID_TIPOCATEGORIA');
    }
}
