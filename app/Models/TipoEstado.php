<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEstado extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOESTADO';
    protected $primaryKey   = 'ID_TIPOESTADO';
    public $timestamps      = false;

    protected $fillable = [
        'ID_TIPOESTADO', 'TIPO_ESTADO', 'COLOR_ESTADO',
    ];
}
