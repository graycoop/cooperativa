<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPersonaJuridica extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOPERSONAJURIDICA';
    protected $primaryKey   = 'ID_TIPOPERSONAJURIDICA';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOPERSONAJURIDICA', 'TIPO_PERSONAJURIDICA', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
