<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoNacionalidad extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPONACIONALIDAD';
    protected $primaryKey   = 'ID_TIPONACIONALIDAD';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPONACIONALIDAD', 'TIPO_NACIONALIDAD', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
