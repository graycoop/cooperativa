<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'EMAILS';
    protected $primaryKey   = 'ID_EMAIL';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_EMAIL', 'ID_PERSONA', 'ID_TIPOEMAIL', 'EMAIL', 'NOTIFICACION', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];

    public function tipo(){
        return $this->hasOne('App\Models\TipoEmail', 'ID_TIPOEMAIL','ID_TIPOEMAIL');
    }
}
