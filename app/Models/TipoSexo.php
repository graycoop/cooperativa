<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoSexo extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOSEXO';
    protected $primaryKey   = 'ID_TIPOSEXO';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOSEXO', 'TIPO_SEXO', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
