<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Socios extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'SOCIOS';
    protected $primaryKey   = 'ID_SOCIO';
    public $incrementing    = false;
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_SOCIO', 'ID_COOP', 'ID_PERSONA', 'FECHA_INGRESO', 'ID_TIPOSOCIO', 'ID_AGENCIA', 'CUOTA_APORTE', 'OBSERVACIONES', 'ID_TIPOSITUACION', 'ID_TIPOMOTIVOBAJA', 'OBSERVACION_BAJA', 'FECHA_BAJA', 'USER_BAJA', 'ID_CARTA', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];

    public function persona(){
        return $this->hasOne('App\Models\Personas', 'ID_PERSONA','ID_PERSONA');
    }
}
