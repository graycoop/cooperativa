<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoCategoriaLaboral extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOCATEGORIALABORAL';
    protected $primaryKey   = 'ID_TIPOCATEGORIA';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOCATEGORIA', 'TIPO_CATEGORIA', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
