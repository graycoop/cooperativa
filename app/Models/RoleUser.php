<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'role_user';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'role_id', 'user_id',
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function role(){
        return $this->belongsTo('App\Models\Role');
    }
}
