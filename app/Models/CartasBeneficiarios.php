<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartasBeneficiarios extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'CARTABENEFICIARIOS';
    protected $primaryKey   = 'ID_CARTA';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_CARTA', 'ID_COOP', 'ID_SOCIO', 'FECHA_CARTA', 'NRO_CARTA', 'FPS', 'OBSERVACIONES', 'BAJA_FPS', 'FECHA_BAJAFPS', 'USER_BAJAFPS', 'MOTIVO_BAJAFPS', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
