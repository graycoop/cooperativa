<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDiscado extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPODISCADOS';
    protected $primaryKey   = 'ID_TIPODISCADO';
    public $timestamps      = false;

    protected $fillable = [
        'ID_TIPODISCADO', 'TIPO_DISCADO', 'CODIGO',
    ];
}
