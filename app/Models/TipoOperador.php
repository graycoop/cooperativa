<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoOperador extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOOPERADOR';
    protected $primaryKey   = 'ID_TIPOOPERADOR';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOOPERADOR', 'TIPO_OPERADOR', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
