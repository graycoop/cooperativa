<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agencia extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'AGENCIAS';
    protected $primaryKey   = ['ID_COOP', 'ID_AGENCIA'];
    public $incrementing    = false;
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_COOP', 'ID_AGENCIA', 'AGENCIA', 'DIRECCION', 'TELEFONO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
