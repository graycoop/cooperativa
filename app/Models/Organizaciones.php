<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organizaciones extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'ORGANIZACIONES';
    protected $primaryKey   = 'ID_ORGANIZACION';
    public $timestamps      = false;

    protected $fillable = [
        'ID_ORGANIZACION', 'ID_COOP', 'RUC', 'RAZON_SOCIAL', 'TELEFONO', 'CONTACTO', 'COD_DPTO', 'COD_PROV', 'COD_DIST', 'DIRECCION', 'FLG_CONVENIO', 'CUENTA_CTB', 'TIPO_ESTADO',
    ];
}
