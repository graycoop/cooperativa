<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personas extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'PERSONAS';
    protected $primaryKey   = 'ID_PERSONA';
    public $incrementing    = false;
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_PERSONA', 'ID_COOP', 'ID_AGENCIA', 'ID_TIPOPERSONERIA', 'ID_TIPODOCIDENTIDAD', 'NRO_DOCUMENTO', 'APELLIDO_PAT', 'APELLIDO_MAT', 'APELLIDO_CAS', 'NOMBRES', 'RAZON_SOCIAL', 'ID_TIPOSEXO', 'ID_TIPOESTADOCIVIL', 'FECHA_NACIMIENTO', 'LUGAR_NACIMIENTO', 'ID_TIPONACIONALIDAD', 'ID_TIPORESIDENCIA', 'ID_TIPOACTIVIDADECONOMICA', 'ID_TIPONIVELEDUCATIVO', 'ID_TIPOOCUPACION', 'ID_TIPOPROFESION', 'ID_TIPOMODALIDADLABORAL', 'ID_TIPOPERSONAJURIDICA', 'ID_TAMANOPERSONAJURIDICA', 'CONDICION_SUJETOOBLIGADO', 'ID_TIPOREGIMENCLIENTE', 'ID_DIRECCION', 'ID_CENTROLABORAL', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];

    public function cooperativa(){
        return $this->hasOne('App\Models\Cooperativas', 'ID_COOP','ID_COOP');
    }

    public function tipodocidentidad(){
        return $this->hasOne('App\Models\TipoDocIdentidad', 'ID_TIPODOCIDENTIDAD','ID_TIPODOCIDENTIDAD');
    }

    public function tipopersoneria(){
        return $this->hasOne('App\Models\TipoPersoneria', 'ID_TIPOPERSONERIA','ID_TIPOPERSONERIA');
    }

    public function tiposexo(){
        return $this->hasOne('App\Models\TipoSexo', 'ID_TIPOSEXO','ID_TIPOSEXO');
    }
}
