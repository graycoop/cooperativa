<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoNivelEducativo extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPONIVELEDUCATIVO';
    protected $primaryKey   = 'ID_TIPONIVELEDUCATIVO';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPONIVELEDUCATIVO', 'TIPO_NIVELEDUCATIVO', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
