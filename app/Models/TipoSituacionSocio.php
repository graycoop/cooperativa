<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoSituacionSocio extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOSITUACIONSOCIO';
    protected $primaryKey   = 'ID_TIPOSITUACIONSOCIO';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOSITUACIONSOCIO', 'TIPO_SITUACIONSOCIO', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
