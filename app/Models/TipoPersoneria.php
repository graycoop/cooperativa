<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPersoneria extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOPERSONERIA';
    protected $primaryKey   = 'ID_TIPOPERSONERIA';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOPERSONERIA', 'TIPO_PERSONERIA', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
