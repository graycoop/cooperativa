<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telefonos extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TELEFONOS';
    protected $primaryKey   = 'ID_TELEFONO';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TELEFONO', 'ID_PERSONA', 'ID_TIPOTELEFONO', 'ID_TIPOOPERADOR', 'ID_TIPODISCADO', 'TELEFONO', 'ANEXO', 'OBSERVACIONES', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];

    public function operador(){
        return $this->hasOne('App\Models\TipoOperador', 'ID_TIPOOPERADOR','ID_TIPOOPERADOR');
    }

    public function tipo(){
        return $this->hasOne('App\Models\TipoTelefono', 'ID_TIPOTELEFONO','ID_TIPOTELEFONO');
    }
}
