<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEmail extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOEMAIL';
    protected $primaryKey   = 'ID_TIPOEMAIL';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOEMAIL', 'TIPO_EMAIL', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
