<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TamañoPersonaJuridica extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TAMANOPERSONAJURIDICA';
    protected $primaryKey   = 'ID_TAMANOPERSONAJURIDICA';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TAMANOPERSONAJURIDICA', 'TAMANO_PERSONAJURIDICA', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
