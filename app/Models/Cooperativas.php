<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cooperativas extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'COOPERATIVAS';
    protected $primaryKey   = 'ID_COOP';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_COOP', 'RUC', 'RAZON_SOCIAL', 'REPRESENTANTE_LEGAL', 'DPTO', 'PROV', 'DIST', 'DIRECCION', 'TELEFONO1', 'TELEFONO2', 'URL_WEBSITE', 'URL_GRAYCOOP', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
