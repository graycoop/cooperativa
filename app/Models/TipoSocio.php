<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoSocio extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOSOCIO';
    protected $primaryKey   = 'ID_TIPOSOCIO';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOSOCIO', 'TIPO_SOCIO', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
