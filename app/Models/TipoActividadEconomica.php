<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoActividadEconomica extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'TIPOACTIVIDADECONOMICA';
    protected $primaryKey   = 'ID_TIPOACTIVIDADECONOMICA';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_TIPOACTIVIDADECONOMICA', 'TIPO_ACTIVIDADECONOMICA', 'DESC_CORTO', 'CODIGO', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];
}
