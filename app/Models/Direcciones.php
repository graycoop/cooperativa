<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Direcciones extends Model
{
    protected $connection   = 'graycoop';
    protected $table        = 'DIRECCIONES';
    protected $primaryKey   = 'ID_DIRECCION';
    public $timestamps      = true;
    const CREATED_AT        = 'FECHA_REG';
    const UPDATED_AT        = 'FECHA_ACT';

    protected $fillable = [
        'ID_DIRECCION', 'ID_PERSONA', 'ID_TIPODIRECCION', 'COD_DPTO', 'COD_PROV', 'COD_DIST', 'DIRECCION', 'REFERENCIA', 'PRINCIPAL', 'USER_REG', 'FECHA_REG', 'USER_ACT', 'FECHA_ACT', 'TIPO_ESTADO',
    ];

    public function tipo(){
        return $this->hasOne('App\Models\TipoDireccion', 'ID_TIPODIRECCION','ID_TIPODIRECCION');
    }
}
