<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formDatosPersonalesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->tipoPersona === '1'){
            $rules = [
                'tipoDocIdentidad' => 'required',
                'numeroDocumento' => 'required',
                'apellidoPaterno' => 'required',
                'apellidoMaterno' => 'required',
                'nombres' => 'required',
                'tipoSexo' => 'required',
                'tipoEstadoCivil' => 'required',
                'fecNacimiento' => 'required',
                'lugarNacimiento' => 'required',
                'tipoNacionalidad' => 'required',
                'tipoResidencia' => 'required',
                'tipoActividadEconomica' => 'required',
                'tipoNivelEducativo' => 'required',
                'tipoOcupacion' => 'required',
                'tipoProfesion' => 'required',
                'tipoModalidadLaboral' => 'required'
            ];
        }elseif ($this->tipoPersona === '2'){

        }

        return $rules;
    }

    public function messages()
    {
        if($this->tipoPersona === '1'){
            $rules = [
                'tipoDocIdentidad.required' => 'Debes eleguir un tipo de documento de identidad',
                'numeroDocumento.required' => 'Debes ingresar un número de documento',
                'apellidoPaterno.required' => 'Debes ingresar el apellido paterno',
                'apellidoMaterno.required' => 'Debes ingresar el apellido materno',
                'nombres.required' => 'Debes ingresar su nombre',
                'tipoSexo.required' => 'Debes eleguir el tipo de sexo',
                'tipoEstadoCivil.required' => 'Debes eleguir el tipo de estado civil',
                'fecNacimiento.required' => 'Debes ingresar una fecha de nacimiento',
                'lugarNacimiento.required' => 'Debes ingresar un lugar de nacimiento',
                'tipoNacionalidad.required' => 'Debes eleguir un tipo de nacionalidad',
                'tipoResidencia.required' => 'Debes eleguir un tipo de residencia',
                'tipoActividadEconomica.required' => 'Debes eleguir un tipo de actividad',
                'tipoNivelEducativo.required' => 'Debes eleguir un tipo de nivel educativo',
                'tipoOcupacion.required' => 'Debes eleguir un tipo de ocupacion',
                'tipoProfesion.required' => 'Debes eleguir un tipo de profesión',
                'tipoModalidadLaboral.required' => 'Debes eleguir un tipo de modalidad laboral'
            ];
        }
        return $rules;
    }
}
