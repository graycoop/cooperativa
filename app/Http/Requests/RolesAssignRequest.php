<?php

namespace App\Http\Requests;

use App\Models\RoleUser;
use Illuminate\Foundation\Http\FormRequest;

class RolesAssignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'checkUser' => 'required',
        ];
        if(!empty($this->checkUser)){
            $roleUsers = RoleUser::Select()
                ->whereIn('user_id', $this->checkUser)
                ->get()
                ->toArray();

            foreach ($roleUsers as $key => $value) {
                $rules = [
                    'checkUser.*'   => 'unique:role_user,user_id,'.$value['id']
                ];
            }
        }
        return $rules;
    }

    public function messages()
    {
        $rules = [
            'checkUser.required' => 'Debes eleguir por lo menos a un usuario',
        ];

        if(!empty($this->checkUser)){
            $roleUsers = RoleUser::Select()
                ->with('user')
                ->whereIn('user_id', $this->checkUser)
                ->get()
                ->toArray();

            foreach ($roleUsers as $key => $value) {
                $rules = [
                    'checkUser.*.unique'  => 'Algunos de los usuarios seleccionados, ya cuentan con un rol asignado'
                ];
            }
        }
        return $rules;
    }
}
