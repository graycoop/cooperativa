<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolesAssignRequest;
use App\Models\Menu;
use App\Models\MenuRoles;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RoleController extends ScoreController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        return view('elements/roles/index')->with(array(
            'titleModule'   => 'Administrar Roles',
            'iconModule'    => 'feather feather-users'
        ));
    }

    public function listRoles(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_rol_list         = $this->roles_list_query();
            $builderview            = $this->builderview($query_rol_list);
            $outgoingcollection     = $this->outgoingcollection($builderview);
            $list_roles             = $this->FormatDatatable($outgoingcollection);
            return $list_roles;
        }
    }

    protected function roles_list_query()
    {
        $roles_list_query = Role::Select()
            ->get()
            ->toArray();
        return $roles_list_query;
    }

    protected function builderview($role_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($role_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']           = $idList;
            $builderview[$posicion]['id_role']      = $query['id'];
            $builderview[$posicion]['name']         = ucwords(Str::lower($query['name']));
            $builderview[$posicion]['description']  = ucwords(Str::lower($query['description']));
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'            => $view['id'],
                'name'          => $view['name'],
                'description'   => $view['description'],
                'action'        => '<div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-warning btn-sm" onclick="responseModal('."'div.dialogGrayCoop','formRoles', { valueID: '".$view['id_role']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Rol" class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm" onclick="responseModal('."'div.dialogGrayCoopExtraLarge','formRolesAssing', { valueID: '".$view['id_role']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Asignar Usuarios" class="fa fa-group"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-indigo btn-sm" onclick="responseModal('."'div.dialogGrayCoopExtraLarge','formRolesMenu',{ valueID: '".$view['id_role']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Actualizar Menú" class="fa fa-bars"></i></a>
                                    </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formRoles(Request $request)
    {
        if($request->valueID == null){
            return view('elements/roles/form/form_roles')->with(array(
                'dataRole'          => '',
                'updateForm'        => false
            ));
        }else{
            $getRole = $this->getRole($request->valueID);
            return view('elements/roles/form/form_roles')->with(array(
                'dataRole'          => $getRole,
                'updateForm'        => true
            ));
        }
    }

    public function formRolesAssing(Request $request) {
        $getRole  = $this->getRole($request->valueID);
        $getData = $this->getData();
        $getUsersRole = $this->UsersRoles($getData['users'], $getData['role_users']);
        return view('elements/roles/form/form_roles_assing')->with(array(
            'idRole'        => $getRole[0]['id'],
            'nameRole'      => ucwords(Str::lower($getRole[0]['name'])),
            'UsersRole'     => $getUsersRole
        ));
    }

    public function formRolesMenu(Request $request){
        $getRole  = $this->getRole($request->valueID);
        $getMenuRoles = $this->getMenuRole($request->valueID);
        return view('elements/roles/form/form_roles_menu')->with(array(
            'idRole'        => $getRole[0]['id'],
            'nameRole'      => ucwords(Str::lower($getRole[0]['name'])),
            'menus'         => Menu::menus(),
            'checkMenu'     => $getMenuRoles
        ));
    }

    public function getRole($idRole){
        $role = Role::Select()
            ->where('id', $idRole)
            ->get()
            ->toArray();

        return $role;
    }

    public function getData(){
        $role = Role::Select()
            ->get()
            ->toArray();

        $users = User::Select()
            ->get()
            ->toArray();

        $role_users = RoleUser::Select()
            ->get()
            ->toArray();

        $data['role'] = $role;
        $data['users'] = $users;
        $data['role_users'] = $role_users;

        return $data;
    }

    public function getMenuRole($idRol) {
        $role_menus = MenuRoles::Select()
            ->where('role_id', $idRol)
            ->get()
            ->toArray();

        return $role_menus;
    }

    protected function UsersRoles($Users, $UsersRole)
    {
        $resultArray = $Users;
        foreach ($Users as $keyUser => $valUser) {
            foreach ($UsersRole as $keyUserRole => $valUserRole) {
                if ($valUser['id'] == $valUserRole['user_id']) {
                    $resultArray[$keyUser] = $valUser + array('UserRoles' => $valUserRole);
                }
            }
        }
        return $resultArray;
    }

    public function saveFormRoles(Request $request){
        if ($request->isMethod('post')) {
            $roleQuery = Role::updateOrCreate([
                'id'            => $request->roleID
            ], [
                'name'          => $request->nameRole,
                'description'   => $request->descriptionRole,
                'id_estado'     => $request->statusID
            ]);
            $action = ($request->roleID ? 'Actualizo' : 'Creo');
            if ($roleQuery) {
                return ['message' => 'Success', 'action' => $action, 'datatable' => 'listRoles'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormRolesAssing(RolesAssignRequest $request)
    {
        if ($request->isMethod('post')) {

            RoleUser::where('role_id', $request->roleID)->delete();

            if ($request->checkUser) {
                foreach ($request->checkUser as $keyUserRole => $valUserRole) {
                    $roleUserQuery = RoleUser::updateOrCreate([
                        'user_id' => $valUserRole,
                        'role_id' => $request->roleID
                    ], [
                        'user_id' => $valUserRole,
                        'role_id' => $request->roleID
                    ]);
                }
                if ($roleUserQuery) {
                    return ['message' => 'Success'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Success'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormRolesMenu(Request $request){
        if ($request->isMethod('post')) {
            MenuRoles::where('role_id', $request->roleID)->delete();
            if ($request->checkMenu) {
                foreach ($request->checkMenu as $keyRoleMenu => $valRoleMenu) {
                    $roleMenuQuery = MenuRoles::updateOrCreate([
                        'role_id'   => $request->roleID,
                        'vue_name'  => $valRoleMenu
                    ], [
                        'role_id'   => $request->roleID,
                        'vue_name'  => $valRoleMenu
                    ]);
                }
                if ($roleMenuQuery) {
                    return ['message' => 'Success'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Success'];
        }
        return ['message' => 'Error'];
    }
}
