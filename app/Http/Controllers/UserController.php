<?php

namespace App\Http\Controllers;

use App\Http\Requests\formAvatarRequest;
use App\Models\Menu;
use App\Models\MenuRoles;
use App\Models\MenuUsers;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\TipoEstado;
use App\Models\TipoDocIdentidad;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UserController extends ScoreController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        return view('elements/users/index')->with(array(
            'titleModule'       => 'Administrar Usuarios',
            'iconModule'        => 'feather feather-user'
        ));
    }

    public function listUsers(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_user_list        = $this->user_list_query();
            $builderview            = $this->builderview($query_user_list);
            $outgoingcollection     = $this->outgoingcollection($builderview);
            $list_users             = $this->FormatDatatable($outgoingcollection);
            return $list_users;
        }
    }

    protected function user_list_query()
    {
        $user_list_query = User::Select()
            ->with('status')
            ->get()
            ->toArray();
        return $user_list_query;
    }

    protected function status_list_query()
    {
        $status_list_query = TipoEstado::Select()
            ->get()
            ->toArray();
        return $status_list_query;
    }

    protected function builderview($user_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($user_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                   = $idList;
            $builderview[$posicion]['id_user']              = $query['id'];
            $builderview[$posicion]['avatar']               = $query['avatar'];
            $builderview[$posicion]['name']                 = ucwords(Str::lower($query['primer_nombre'])).' '.ucwords(Str::lower($query['segundo_nombre'])).' '.ucwords(Str::lower($query['primer_apellido'])).' '.ucwords(Str::lower($query['segundo_apellido']));
            $builderview[$posicion]['username']             = $query['username'];
            $builderview[$posicion]['status']               = $query['status']['TIPO_ESTADO'];
            $builderview[$posicion]['status_background']    = $query['status']['COLOR_ESTADO'];
            $builderview[$posicion]['session_id']           = $query['session_id'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $sessionUser = $view['session_id'] ? "user--online" : "user--offline";
            $outgoingcollection->push([
                'id'            => $view['id'],
                'avatar'        => '<span class="avatar thumb-xxs mr-b-0 '.$sessionUser.'"><img class="rounded-circle" src="'.asset($view['avatar']).'?version='.Carbon::now()->format('YmdHis').'"></span>',
                'name'          => $view['name'],
                'username'      => $view['username'],
                'status'        => '<span class="badge px-3 heading-font-family '.$view['status_background'].'">'.$view['status'].'</span>',
                'action'        => '<div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-warning btn-sm" onclick="responseModal('."'div.dialogGrayCoopExtraLarge','formUsers', { valueID: '".$view['id_user']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Usuario" class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-indigo btn-sm" onclick="responseModal('."'div.dialogGrayCoopExtraLarge','formUsersMenu', { valueID: '".$view['id_user']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Actualizar Menú" class="fa fa-bars"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-facebook btn-sm" onclick="responseModal('."'div.dialogGrayCoop','formUsersStatus', { valueID: '".$view['id_user']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Cambiar Estado" class="fa fa-refresh"></i></a>
                                    </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formAvatar(Request $request){
        if ($request->isMethod('get')) {
            $getUser = $this->getUser(Auth::user()->id);
            return view('elements/users/form/form_users_avatar')->with(array(
                'dataUser'  => $getUser
            ));
        }
        return view('errors/permission');
    }

    public function formUsers(Request $request){
        if($request->valueID == null){
            return view('elements/users/form/form_users')->with(array(
                'dataUser'          => '',
                'updateForm'        => false,
                'options'           => $this->getOptions()
            ));
        }else{
            $getUser = $this->getUser($request->valueID);
            return view('elements/users/form/form_users')->with(array(
                'dataUser'          => $getUser,
                'updateForm'        => true,
                'options'           => $this->getOptions()
            ));
        }
    }

    public function formUsersMenu(Request $request){
        $dataUser  = $this->getUser($request->valueID);
        $getMenuUsers = $this->getMenuUser($request->valueID);
        return view('elements/users/form/form_users_menu')->with(array(
            'dataUser'      => $dataUser,
            'menus'         => Menu::menus(),
            'checkMenu'     => $getMenuUsers
        ));
    }

    public function formUsersStatus(Request $request){
        $dataUser  = $this->getUser($request->valueID);
        $statusOptions = $this->getNotUserStatus($dataUser[0]['status']['ID_TIPOESTADO']);
        return view('elements/users/form/form_users_status')->with(array(
            'dataUser'      => $dataUser,
            'options'       => $statusOptions
        ));
    }

    public function saveFormAvatar(formAvatarRequest $request){
        if ($request->isMethod('post')) {

            $nameDirectory = public_path('img/avatars/');

            if (!File::exists($nameDirectory)) {
                $this->makeDirectory($nameDirectory);
            }

            $extensionFile = $request->file('avatarUser')->getClientOriginalExtension();
            $filename = Auth::user()->username.'.'.$extensionFile;

            if($request->avatarOriginal){
                $srcAvatar = $request->avatarOriginal;
                $userQuery = User::where('id', Auth::user()->id)
                    ->update([
                        'avatar'    => $srcAvatar
                    ]);
                if($userQuery){
                    return ['message' => 'Success'];
                }
            }else{
                $srcAvatar = 'img/avatars/'.$filename;
                $userQuery = User::where('id', Auth::user()->id)
                    ->update([
                        'avatar'    => $srcAvatar
                    ]);

                if ($request->file('avatarUser')->getClientOriginalExtension() == 'gif') {
                    $saveImage = copy($request->file('avatarUser')->getRealPath(), $nameDirectory.$filename);
                    $this->resetCookie('cookie_avatar', Auth::user()->avatar);
                }
                else {
                    $saveImage = Image::make($request->file('avatarUser')->getRealPath())->resize(512, 512)->save($nameDirectory . $filename);
                    $this->resetCookie('cookie_avatar', Auth::user()->avatar);
                }

                if($userQuery){
                    if($saveImage){
                        return ['message' => 'Success'];
                    }
                }
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }

    public function saveFormUsers(Request $request){
        if ($request->isMethod('post')) {

            $password = $request->password == $request->passwordValidate ? $request->password : Hash::make($request->password);

            $userQuery = User::updateOrCreate([
                'id'    => $request->userID
            ], [
                'primer_nombre'         => $request->firstName,
                'segundo_nombre'        => $request->secondName,
                'primer_apellido'       => $request->lastName,
                'segundo_apellido'      => $request->lastSecondName,
                'id_tipo_identidad'     => $request->typeIdentity,
                'numero_identidad'      => $request->numIdentity,
                'fecha_nacimiento'      => $request->birthDay,
                'email'                 => $request->email,
                'username'              => $request->username,
                'password'              => $password,
                'id_estado'             => $request->statusID
            ]);
            $userLastID = User::latest()->first();

            $userRolQuery = RoleUser::updateOrCreate([
                'user_id'   => $request->userID ? $request->userID : $userLastID['id']
            ], [
                'user_id'   => $request->userID ? $request->userID : $userLastID['id'],
                'role_id'   => $request->roleUser
            ]);
            if($request->checkRolMenu){
                MenuUsers::where('user_id', $request->userID ? $request->userID : $userLastID['id'])->delete();
                $menuRole = MenuRoles::Select()
                    ->where('role_id', $request->roleUser)
                    ->get()
                    ->toArray();

                foreach ($menuRole as $keyUserRole => $valUserRole) {
                    MenuUsers::Insert([
                        'user_id'   => $request->userID ? $request->userID : $userLastID['id'],
                        'vue_name'  => $valUserRole['vue_name']
                    ]);
                }
            }
            $action = ($request->userID ? 'Actualizo' : 'Creo');
            if ($userQuery) {
                if($userRolQuery){
                    return ['message' => 'Success', 'action' => $action, 'datatable' => 'listUsers'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormUsersMenu(Request $request){
        if ($request->isMethod('post')) {
            MenuUsers::where('user_id', $request->userID)->delete();
            if ($request->checkMenu) {
                foreach ($request->checkMenu as $keyRoleMenu => $valRoleMenu) {
                    $userMenuQuery = MenuUsers::updateOrCreate([
                        'user_id'   => $request->userID,
                        'vue_name'  => $valRoleMenu
                    ], [
                        'user_id'   => $request->userID,
                        'vue_name'  => $valRoleMenu
                    ]);
                }
                if ($userMenuQuery) {
                    return ['message' => 'Success'];
                }
                return ['message' => 'Error'];
            }
            return ['message' => 'Success'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormUsersStatus(Request $request){
        if ($request->isMethod('post')) {
            $usersQuery = User::where('id', $request->userID)
                ->update(['id_estado' => $request->statusUser]);
            if($usersQuery){
                return ['message' => 'Success', 'datatable' => 'listUsers'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function getUser($idUser){
        $user = User::Select()
            ->with('identity')
            ->with('status')
            ->with('roles')
            ->where('id', $idUser)
            ->get()
            ->toArray();
        return $user;
    }

    public function getOptions(){
        $typeIdentity = TipoDocIdentidad::Select()
                        ->get()
                        ->toArray();

        $roles = Role::Select()
                    ->get()
                    ->toArray();

        $options['typeIdentity'] = $typeIdentity;
        $options['roles'] = $roles;

        return $options;
    }

    public function getNotUserStatus($idStatus){
        $statusUser = TipoEstado::Select()
            ->whereNotIn('ID_TIPOESTADO',[$idStatus])
            ->get()
            ->toArray();
        return $statusUser;
    }

    public function getMenuUser($idUser) {
        $user_menus = MenuUsers::Select()
            ->where('user_id', $idUser)
            ->get()
            ->toArray();

        return $user_menus;
    }
}
