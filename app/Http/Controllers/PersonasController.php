<?php

namespace App\Http\Controllers;

use App\Http\Requests\formDatosPersonalesRequest;
use App\Models\CategoriaLaboral;
use App\Models\CentroLaboral;
use App\Models\Direcciones;
use App\Models\Emails;
use App\Models\EstadoLaboral;
use App\Models\Organizaciones;
use App\Models\Telefonos;
use App\Models\TipoDireccion;
use App\Models\TipoDiscado;
use App\Models\TipoEmail;
use App\Models\TipoOperador;
use App\Models\TipoTelefono;
use App\Models\Ubigueo;
use Illuminate\Support\Facades\DB;
use App\Models\Personas;
use App\Models\TamañoPersonaJuridica;
use App\Models\TipoActividadEconomica;
use App\Models\TipoDocIdentidad;
use App\Models\TipoEstadoCivil;
use App\Models\TipoModalidaLaboral;
use App\Models\TipoNacionalidad;
use App\Models\TipoNivelEducativo;
use App\Models\TipoOcupacion;
use App\Models\TipoPersonaJuridica;
use App\Models\TipoPersoneria;
use App\Models\TipoProfesion;
use App\Models\TipoSexo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PersonasController extends ScoreController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('elements/personas/index')->with(array(
            'titleModule'       => 'Buscador de Personas',
            'iconModule'        => 'feather feather-user'
        ));
    }

    public function listPersonas(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_persona_list         = $this->filterSearchPersona($request);
            $builderview                = $this->builderviewPersonas($query_persona_list);
            $outgoingcollection         = $this->outgoingcollectionPersonas($builderview);
            $list_personas              = $this->FormatDatatable($outgoingcollection);
            return $list_personas;
        }
    }

    protected function builderviewPersonas($persona_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($persona_list_query as $query) {
            $namePersona = (empty($query['RAZON_SOCIAL']) ? ucwords(Str::lower($query['NOMBRES'].' '.$query['APELLIDO_PAT'].' '.$query['APELLIDO_MAT'])) : ucwords(Str::lower($query['RAZON_SOCIAL'])));
            $idList ++;
            $builderview[$posicion]['id_persona']               = $query['ID_PERSONA'];
            $builderview[$posicion]['nombres']                  = $namePersona;
            $builderview[$posicion]['nro_documento']            = $query['NRO_DOCUMENTO'];
            $builderview[$posicion]['fec_nacimiento']           = $query['FECHA_NACIMIENTO'];
            $builderview[$posicion]['tipo_sexo']                = isset($query['tipo_sexo']) ? $query['tipo_sexo']['TIPO_SEXO'] : '-';
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionPersonas($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            if($view['tipo_sexo'] == '-') $iconSexo = '<span class="text-facebook"><i class="list-icon fa fa-briefcase" aria-hidden="true"></i> Empresas</span>';
            elseif ($view['tipo_sexo'] == 'MASCULINO') $iconSexo = '<span class="text-twitter"><i class="list-icon fa fa-male" aria-hidden="true"></i> Masculino</span>';
            else $iconSexo = '<span class="text-dribbble"><i class="list-icon fa fa-male" aria-hidden="true"></i> Femenino</span>';

            if($view['tipo_sexo'] == '-') $imgAvatar = asset('img/empresas.png');
            elseif ($view['tipo_sexo'] == 'MASCULINO') $imgAvatar = asset('img/masculino.png');
            else $imgAvatar = asset('img/femenino.png');

            $outgoingcollection->push([
                'id_persona'        => $view['id_persona'],
                'nombres'           => '<span class="avatar thumb-xxs mr-b-0"><img class="rounded-circle" src="'.$imgAvatar.'?version='.Carbon::now()->format('YmdHis').'"> '.$view['nombres'].'</span>',
                'nro_documento'     => $view['nro_documento'],
                'fec_nacimiento'    => '<label class="badge px-3 heading-font-family bg-custom">'.Carbon::parse($view['fec_nacimiento'])->format('d-m-Y').'</label>',
                'tipo_sexo'         => $iconSexo,
                'action'            => '<div class="btn-group">
                                            <a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="vmFront.loadOptionMenu('."'formPersonas', { idPersona : '".$view['id_persona']."' }".')"><i title="Editar Persona" class="fa fa-edit"></i></a>
                                        </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function filterSearchPersona($dataSearch){

        $wherePersona = array();

        $whereCustom = ['ID_COOP', '=', Auth::user()->id_coop];
        array_push($wherePersona, $whereCustom);

        if(isset($dataSearch[0]['value'])){
            $whereCustom = ['APELLIDO_PAT', 'like', '%'.$dataSearch[0]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[1]['value'])){
            $whereCustom = ['APELLIDO_MAT', 'like' ,'%'.$dataSearch[1]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[2]['value'])){
            $whereCustom = ['NOMBRES', 'like' ,'%'.$dataSearch[2]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[3]['value'])){
            $whereCustom = ['RAZON_SOCIAL', 'like' ,'%'.$dataSearch[3]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[4]['value'])){
            $whereCustom = ['NRO_DOCUMENTO', 'like' ,'%'.$dataSearch[4]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[5]['value'])){
            $whereCustom = ['CODIGO', 'like' ,'%'.$dataSearch[5]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[6]['value'])){
            $whereCustom = ['ID_PERSONA', 'like' ,'%'.$dataSearch[6]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }

        $listPersona = Personas::with('tipoSexo')->where($wherePersona)->get()->toArray();

        return $listPersona;
    }

    public function formPersonas(Request $request) {
        if($request->idPersona == null){
            return view('elements/personas/form/formPersona')->with(array(
                'titleModule'               => 'Agregar Persona',
                'iconModule'                => 'feather feather-user',
                'dataPersona'               => '',
                'updateForm'                => false,
                'selectOptions'             => $this->getOptions()
            ));
        }else{
            $getPersona = $this->getPersona($request->idPersona);
            return view('elements/personas/form/formPersona')->with(array(
                'titleModule'               => 'Editar Persona',
                'iconModule'                => 'feather feather-user',
                'dataPersona'               => $getPersona,
                'updateForm'                => true,
                'selectOptions'             => $this->getOptions()
            ));
        }
    }

    public function formTipoPersona(Request $request) {
        $tipoPersona = $request->idTipoPersona == 1 ? 'formPersonaNatural' : 'formPersonaJuridica';
        if($request->idPersona == null){
            return view('elements/personas/form/tipoPersona/'.$tipoPersona)->with(array(
                'selectOptions'             => $this->getOptions(),
                'dataPersona'               => '',
                'updateForm'                => false
            ));
        }else{
            $getPersona = $this->getPersona($request->idPersona);
            return view('elements/personas/form/tipoPersona/'.$tipoPersona)->with(array(
                'selectOptions'             => $this->getOptions(),
                'dataPersona'               => $getPersona,
                'updateForm'                => true
            ));
        }
    }

    public function getPersona($idPersona){
        $persona = Personas::Select()
            ->with('cooperativa')
            ->with('tipodocidentidad')
            ->with('tipopersoneria')
            ->where('ID_PERSONA', $idPersona)
            ->get()
            ->toArray();
        return $persona;
    }

    public function saveFormDatosPersonales(formDatosPersonalesRequest $request){
        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();

                if($request->personaID){
                    $personQueryPersona = Personas::where('ID_PERSONA', $request->personaID)
                    ->update([
                        'ID_TIPOPERSONERIA'             => $request->tipoPersona,
                        'ID_TIPODOCIDENTIDAD'           => $request->tipoDocIdentidad,
                        'NRO_DOCUMENTO'                 => $request->numeroDocumento,
                        'APELLIDO_PAT'                  => $request->apellidoPaterno ? Str::upper($request->apellidoPaterno) : '-',
                        'APELLIDO_MAT'                  => $request->apellidoMaterno ? Str::upper($request->apellidoMaterno) : '-',
                        'APELLIDO_CAS'                  => '-',
                        'NOMBRES'                       => $request->nombres ? Str::upper($request->nombres) : '-',
                        'RAZON_SOCIAL'                  => $request->razonSocial ? Str::upper($request->razonSocial) : '-',
                        'ID_TIPOSEXO'                   => $request->tipoSexo ? $request->tipoSexo : '0',
                        'ID_TIPOESTADOCIVIL'            => $request->tipoEstadoCivil ? $request->tipoEstadoCivil : '0',
                        'FECHA_NACIMIENTO'              => $request->fecNacimiento,
                        'LUGAR_NACIMIENTO'              => $request->lugarNacimiento ? Str::upper($request->lugarNacimiento) : '-',
                        'ID_TIPONACIONALIDAD'           => $request->tipoNacionalidad,
                        'ID_TIPORESIDENCIA'             => $request->tipoResidencia,
                        'ID_TIPOACTIVIDADECONOMICA'     => $request->tipoActividadEconomica,
                        'ID_TIPONIVELEDUCATIVO'         => $request->tipoNivelEducativo ? $request->tipoNivelEducativo : '0',
                        'ID_TIPOOCUPACION'              => $request->tipoOcupacion ? $request->tipoOcupacion : '0',
                        'ID_TIPOPROFESION'              => $request->tipoProfesion ? $request->tipoProfesion : '0',
                        'ID_TIPOMODALIDADLABORAL'       => $request->tipoModalidadLaboral ? $request->tipoModalidadLaboral : '0',
                        'ID_TIPOPERSONAJURIDICA'        => $request->tipoPersonaJuridica ? $request->tipoPersonaJuridica : '0',
                        'ID_TAMANOPERSONAJURIDICA'      => $request->tipoTamañoPersonaJuridica ? $request->tipoTamañoPersonaJuridica : '0',
                        'USER_ACT'                      => Auth::user()->id,
                        'FECHA_ACT'                     => Carbon::now(),
                    ]);

                    if(!$personQueryPersona){
                        DB::rollback();
                        return ['message' => 'Error'];
                    }

                    if($personQueryPersona){
                        DB::commit();
                        return ['message' => 'Success', 'action' => 'update', 'tipoPersona' => $request->tipoPersona, 'idPersona' => $request->personaID];
                    }
                }else{
                    $getIDPersona = DB::select('SELECT F_GET_ID_PERSONA ("'.Auth::user()->id_coop.'","'.Auth::user()->id_agencia.'") AS ID_PERSONA');

                    if(!$getIDPersona){
                        DB::rollback();
                        return ['message' => 'Error', 'getWhere' => 'GET_ID_PERSONA'];
                    }

                    $personQueryPersona = Personas::create([
                        'ID_PERSONA'                    => $getIDPersona[0]->ID_PERSONA,
                        'ID_COOP'                       => Auth::user()->id_coop,
                        'ID_AGENCIA'                    => Auth::user()->id_agencia,
                        'ID_TIPOPERSONERIA'             => $request->tipoPersona,
                        'ID_TIPODOCIDENTIDAD'           => $request->tipoDocIdentidad,
                        'NRO_DOCUMENTO'                 => $request->numeroDocumento,
                        'APELLIDO_PAT'                  => $request->apellidoPaterno ? Str::upper($request->apellidoPaterno) : '-',
                        'APELLIDO_MAT'                  => $request->apellidoMaterno ? Str::upper($request->apellidoMaterno) : '-',
                        'APELLIDO_CAS'                  => '-',
                        'NOMBRES'                       => $request->nombres ? Str::upper($request->nombres) : '-',
                        'RAZON_SOCIAL'                  => $request->razonSocial ? Str::upper($request->razonSocial) : '-',
                        'ID_TIPOSEXO'                   => $request->tipoSexo ? $request->tipoSexo : '0',
                        'ID_TIPOESTADOCIVIL'            => $request->tipoEstadoCivil ? $request->tipoEstadoCivil : '0',
                        'FECHA_NACIMIENTO'              => $request->fecNacimiento,
                        'LUGAR_NACIMIENTO'              => $request->lugarNacimiento ? Str::upper($request->lugarNacimiento) : '-',
                        'ID_TIPONACIONALIDAD'           => $request->tipoNacionalidad,
                        'ID_TIPORESIDENCIA'             => $request->tipoResidencia,
                        'ID_TIPOACTIVIDADECONOMICA'     => $request->tipoActividadEconomica,
                        'ID_TIPONIVELEDUCATIVO'         => $request->tipoNivelEducativo ? $request->tipoNivelEducativo : '0',
                        'ID_TIPOOCUPACION'              => $request->tipoOcupacion ? $request->tipoOcupacion : '0',
                        'ID_TIPOPROFESION'              => $request->tipoProfesion ? $request->tipoProfesion : '0',
                        'ID_TIPOMODALIDADLABORAL'       => $request->tipoModalidadLaboral ? $request->tipoModalidadLaboral : '0',
                        'ID_TIPOPERSONAJURIDICA'        => $request->tipoPersonaJuridica ? $request->tipoPersonaJuridica : '0',
                        'ID_TAMANOPERSONAJURIDICA'      => $request->tipoTamañoPersonaJuridica ? $request->tipoTamañoPersonaJuridica : '0',
                        'CONDICION_SUJETOOBLIGADO'      => '0',
                        'ID_TIPOREGIMENCLIENTE'         => '0',
                        'ID_DIRECCION'                  => '0',
                        'ID_CENTROLABORAL'              => '0',
                        'CODIGO'                        => '0',
                        'USER_REG'                      => Auth::user()->id,
                        'FECHA_REG'                     => Carbon::now(),
                        'USER_ACT'                      => Auth::user()->id,
                        'FECHA_ACT'                     => Carbon::now(),
                        'TIPO_ESTADO'                   => '001'
                    ]);

                    if(!$personQueryPersona){
                        DB::rollback();
                        return ['message' => 'Error', 'getWhere' => 'Create'];
                    }

                    if($getIDPersona){
                        if($personQueryPersona){
                            DB::commit();
                            return ['message' => 'Success', 'action' => 'create', 'tipoPersona' => $request->tipoPersona, 'idPersona' => $getIDPersona[0]->ID_PERSONA];
                        }
                    }
                }
            } catch(\Exception $e){
                DB::rollback();
                return ['message' => 'Error'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function listTelefonos(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_telefono_list            = $this->telefonosDiscado($this->telefono_query_list($request->idPersona), $this->getDiscado());
            $builderview                    = $this->builderviewTelefono($query_telefono_list);
            $outgoingcollection             = $this->outgoingcollectionTelefono($builderview);
            $list_telefonos                 = $this->FormatDatatable($outgoingcollection);
            return $list_telefonos;
        }
    }

    public function telefono_query_list($idPersona){
        $telefonos = Telefonos::Select()
            ->where('ID_PERSONA', $idPersona)
            ->with('operador')
            ->with('tipo')
            ->get()
            ->toArray();
        return $telefonos;
    }

    public function getDiscado(){
        $discado = TipoDiscado::Select()
            ->get()
            ->toArray();
        return $discado;
    }

    public function telefonosDiscado($Telefonos, $Discado)
    {
        $resultArray = $Telefonos;
        foreach ($Telefonos as $keyTelefono => $valTelefono) {
            foreach ($Discado as $keyDiscado => $valDiscado) {
                if ($valTelefono['ID_TIPODISCADO'] == $valDiscado['ID_TIPODISCADO']) {
                    $resultArray[$keyTelefono] = $valTelefono + array('discadoTelefono' => $valDiscado);
                }
            }
        }
        return $resultArray;
    }

    protected function builderviewTelefono($telefono_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($telefono_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['id_telefono']              = $query['ID_TELEFONO'];
            $builderview[$posicion]['telefono']                 = $query['TELEFONO'];
            $builderview[$posicion]['anexo']                    = $query['ANEXO'];
            $builderview[$posicion]['operador']                 = $query['operador']['TIPO_OPERADOR'];
            $builderview[$posicion]['discado']                  = $query['discadoTelefono']['TIPO_DISCADO'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionTelefono($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id_telefono'       => $view['id'],
                'telefono'          => $view['telefono'],
                'anexo'             => $view['anexo'],
                'operador'          => $view['operador'],
                'discado'           => $view['discado'],
                'action'            => '<div class="btn-group">
                                            <a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoopLarge','formTelefonos', { idTelefono: '".$view['id_telefono']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Telefono" class="fa fa-edit"></i></a>
                                        </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formTelefonos(Request $request) {
        if($request->idPersona){
            return view('elements/personas/form/telefonos/formTelefono')->with(array(
                'dataTelefono'              => '',
                'updateForm'                => false,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => $request->idPersona,
                'idTelefono'                => ''
            ));
        }else{
            $getTelefono = $this->getTelefono($request->idTelefono);
            return view('elements/personas/form/telefonos/formTelefono')->with(array(
                'dataTelefono'              => $getTelefono,
                'updateForm'                => true,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => '',
                'idTelefono'                => $request->idTelefono
            ));
        }
    }

    public function getTelefono($idTelefono){
        $telefono = Telefonos::Select()
            ->where('ID_TELEFONO',$idTelefono)
            ->get()
            ->toArray();
        return $telefono;
    }

    public function saveFormTelefonos(Request $request){
        if($request->isMethod('post')) {
            if($request->idPersona){
                $queryTelefono = Telefonos::create([
                    'ID_PERSONA'        => $request->idPersona,
                    'ID_TIPOTELEFONO'   => $request->tipoTelefono,
                    'ID_TIPOOPERADOR'   => $request->tipoOperador,
                    'ID_TIPODISCADO'    => $request->tipoDiscado,
                    'TELEFONO'          => $request->numero,
                    'ANEXO'             => $request->anexo,
                    'USER_REG'          => Auth::user()->id,
                    'FECHA_REG'         => Carbon::now(),
                    'TIPO_ESTADO'       => '001'
                ]);

                if($queryTelefono){
                    return ['message' => 'Success', 'action' => 'create', 'datatable' => 'listTelefonos'];
                }
                return ['message' => 'Error'];
            }else{
                $queryTelefono = Telefonos::where('ID_TELEFONO', $request->idTelefono)
                    ->update([
                        'ID_TIPOTELEFONO'   => $request->tipoTelefono,
                        'ID_TIPOOPERADOR'   => $request->tipoOperador,
                        'ID_TIPODISCADO'    => $request->tipoDiscado,
                        'TELEFONO'          => $request->numero,
                        'ANEXO'             => $request->anexo,
                        'USER_ACT'          => Auth::user()->id,
                        'FECHA_ACT'         => Carbon::now()
                    ]);

                if($queryTelefono){
                    return ['message' => 'Success', 'action' => 'update', 'datatable' => 'listTelefonos'];
                }
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }

    public function listEmails(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_email_list               = $this->email_query_list($request->idPersona);
            $builderview                    = $this->builderviewEmail($query_email_list);
            $outgoingcollection             = $this->outgoingcollectionEmail($builderview);
            $list_emails                    = $this->FormatDatatable($outgoingcollection);
            return $list_emails;
        }
    }

    public function email_query_list($idPersona){
        $emails = Emails::Select()
            ->where('ID_PERSONA', $idPersona)
            ->with('tipo')
            ->get()
            ->toArray();
        return $emails;
    }

    protected function builderviewEmail($query_email_list,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($query_email_list as $query) {
            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['id_email']                 = $query['ID_EMAIL'];
            $builderview[$posicion]['email']                    = $query['EMAIL'];
            $builderview[$posicion]['tipo']                     = $query['tipo']['TIPO_EMAIL'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionEmail($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id_email'          => $view['id'],
                'email'             => $view['email'],
                'tipo'              => $view['tipo'],
                'action'            => '<div class="btn-group">
                                            <a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoop','formEmails', { idEmail: '".$view['id_email']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Email" class="fa fa-edit"></i></a>
                                        </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formEmails(Request $request) {
        if($request->idPersona){
            return view('elements/personas/form/emails/formEmails')->with(array(
                'dataEmail'                 => '',
                'updateForm'                => false,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => $request->idPersona,
                'idEmail'                   => ''
            ));
        }else{
            $getEmail = $this->getEmail($request->idEmail);
            return view('elements/personas/form/emails/formEmails')->with(array(
                'dataEmail'                 => $getEmail,
                'updateForm'                => true,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => '',
                'idEmail'                   => $request->idEmail
            ));
        }
    }

    public function getEmail($idEmail){
        $email = Emails::Select()
            ->where('ID_EMAIL', $idEmail)
            ->get()
            ->toArray();
        return $email;
    }

    public function saveFormEmails(Request $request){
        if($request->isMethod('post')) {
            if($request->idPersona){
                $queryEmail = Emails::create([
                    'ID_PERSONA'        => $request->idPersona,
                    'ID_TIPOEMAIL'      => $request->tipoEmail,
                    'EMAIL'             => $request->email,
                    'NOTIFICACION'      => '1',
                    'USER_REG'          => Auth::user()->id,
                    'FECHA_REG'         => Carbon::now(),
                    'TIPO_ESTADO'       => '001'
                ]);

                if($queryEmail){
                    return ['message' => 'Success', 'action' => 'create', 'datatable' => 'listEmails'];
                }
                return ['message' => 'Error'];
            }else{
                $queryEmail = Emails::where('ID_EMAIL', $request->idEmail)
                    ->update([
                        'ID_TIPOEMAIL'      => $request->tipoEmail,
                        'EMAIL'             => $request->email,
                        'USER_ACT'          => Auth::user()->id,
                        'FECHA_ACT'         => Carbon::now()
                    ]);

                if($queryEmail){
                    return ['message' => 'Success', 'action' => 'update', 'datatable' => 'listEmails'];
                }
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }

    public function listDireccion(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_direccion_list           = $this->direccion_query_list($request->idPersona);
            $builderview                    = $this->builderviewDireccion($query_direccion_list);
            $outgoingcollection             = $this->outgoingcollectionDireccion($builderview);
            $list_emails                    = $this->FormatDatatable($outgoingcollection);
            return $list_emails;
        }
    }

    public function direccion_query_list($idPersona){
        $direcciones = Direcciones::Select()
            ->where('ID_PERSONA', $idPersona)
            ->with('tipo')
            ->get()
            ->toArray();
        return $direcciones;
    }

    protected function builderviewDireccion($query_direccion_list,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($query_direccion_list as $query) {
            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['id_direccion']             = $query['ID_DIRECCION'];
            $builderview[$posicion]['direccion']                = $query['DIRECCION'];
            $builderview[$posicion]['tipo']                     = $query['tipo']['TIPO_DIRECCION'];
            $builderview[$posicion]['principal']                = $query['PRINCIPAL'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionDireccion($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $iconPrincipal = $view['principal'] == 1 ? 'fa fa-check' : 'fa fa-close';
            $backgroundPrincipal = $view['principal'] == 1 ? 'bg-custom' : 'bg-danger';
            $outgoingcollection->push([
                'id_direccion'      => $view['id'],
                'direccion'         => $view['direccion'],
                'tipo'              => $view['tipo'],
                'action'            => '<div class="btn-group">
                                            <a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoopLarge','formDireccion', { idDireccion: '".$view['id_direccion']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Direccion" class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" class="btn '.$backgroundPrincipal.' text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoop','formPrincipalDireccion', { idDireccion: '".$view['id_direccion']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Seleccionar como Principal" class="'.$iconPrincipal.'"></i></a>
                                        </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formDireccion(Request $request) {
        if($request->idPersona){
            return view('elements/personas/form/direccion/formDireccion')->with(array(
                'dataDireccion'             => '',
                'updateForm'                => false,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => $request->idPersona,
                'idDireccion'               => '',
                'selectDepartamento'        => $this->selectDepartamento()
            ));
        }else{
            $getDireccion = $this->getDireccion($request->idDireccion);
            return view('elements/personas/form/direccion/formDireccion')->with(array(
                'dataDireccion'             => $getDireccion,
                'updateForm'                => true,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => '',
                'idDireccion'               => $request->idDireccion,
                'selectDepartamento'        => $this->selectDepartamento()
            ));
        }
    }

    public function getDireccion($idDireccion){
        $direccion = Direcciones::Select()
            ->where('ID_DIRECCION', $idDireccion)
            ->get()
            ->toArray();
        return $direccion;
    }

    public function getUbigeo($codDepartamento, $codProvincia, $codDistrito){
        $ubigeo = Ubigueo::Select()
            ->where('DPTO', $codDepartamento)
            ->where('PROV', $codProvincia)
            ->where('DIST', $codDistrito)
            ->get()
            ->toArray();

        return $ubigeo;
    }

    public function selectDepartamento(){
        $departamento = Ubigueo::Select()
            ->where('PROV', 00)
            ->where('DIST', 00)
            ->get()
            ->toArray();
        return $departamento;
    }

    public function selectProvincia(Request $request){
        $getProvincia = Ubigueo::Select()
            ->where([
                ['DPTO', $request->idDepartamento],
                ['PROV', '!=', 00],
                ['DIST', '=', 00]
            ])->get()->toArray();
        return view('elements/personas/form/direccion/utilities/ubigeo/selectProvincia')->with(array(
            'selectProvincia'   => $getProvincia,
            'selectedProvincia' => $request->selectProvincia
        ));
    }

    public function selectDistrito(Request $request){
        $getDistrito = Ubigueo::Select()
            ->where([
                ['DPTO', $request->idDepartamento],
                ['PROV', $request->idProvincia],
                ['DIST', '!=', 00]
            ])->get()->toArray();
        return view('elements/personas/form/direccion/utilities/ubigeo/selectDistrito')->with(array(
            'selectDistrito'   => $getDistrito,
            'selectedDistrito' => $request->selectDistrito
        ));
    }

    public function getOptions(){
        $tipoPersoneria = TipoPersoneria::Select()
            ->get()
            ->toArray();

        $tipoDocumento = TipoDocIdentidad::Select()
            ->get()
            ->toArray();

        $tipoSexo = TipoSexo::Select()
            ->get()
            ->toArray();

        $tipoEstadoCivil = TipoEstadoCivil::Select()
            ->get()
            ->toArray();

        $tipoNacionalidad = TipoNacionalidad::Select()
            ->get()
            ->toArray();

        $tipoActividadEconomica = TipoActividadEconomica::Select()
            ->get()
            ->toArray();

        $tipoNivelEducativo = TipoNivelEducativo::Select()
            ->get()
            ->toArray();

        $tipoPersonaJuridica = TipoPersonaJuridica::Select()
            ->get()
            ->toArray();

        $tamanoPersonaJuridica = TamañoPersonaJuridica::Select()
            ->get()
            ->toArray();

        $tipoOcupacion = TipoOcupacion::Select()
            ->get()
            ->toArray();

        $tipoProfesion = TipoProfesion::Select()
            ->get()
            ->toArray();

        $tipoModalidadLaboral = TipoModalidaLaboral::Select()
            ->get()
            ->toArray();

        $tipoTelefono = TipoTelefono::Select()
            ->get()
            ->toArray();

        $tipoOperador = TipoOperador::Select()
            ->get()
            ->toArray();

        $tipoDiscado = TipoDiscado::Select()
            ->get()
            ->toArray();

        $tipoEmail = TipoEmail::Select()
            ->get()
            ->toArray();

        $tipoDireccion = TipoDireccion::Select()
            ->get()
            ->toArray();

        $organizacion = Organizaciones::Select()
            ->get()
            ->toArray();

        $categoriaLaboral = CategoriaLaboral::Select()
            ->get()
            ->toArray();

        $estadoLaboral = EstadoLaboral::Select()
            ->get()
            ->toArray();

        $options['tipoPersoneria'] = $tipoPersoneria;
        $options['tipoDocumento'] = $tipoDocumento;
        $options['tipoSexo'] = $tipoSexo;
        $options['tipoEstadoCivil'] = $tipoEstadoCivil;
        $options['tipoNacionalidad'] = $tipoNacionalidad;
        $options['tipoActividadEconomica'] = $tipoActividadEconomica;
        $options['tipoNivelEducativo'] = $tipoNivelEducativo;
        $options['tipoPersonaJuridica'] = $tipoPersonaJuridica;
        $options['tamañoPersonaJuridica'] = $tamanoPersonaJuridica;
        $options['tipoOcupacion'] = $tipoOcupacion;
        $options['tipoProfesion'] = $tipoProfesion;
        $options['tipoModalidadLaboral'] = $tipoModalidadLaboral;
        $options['tipoTelefono'] = $tipoTelefono;
        $options['tipoOperador'] = $tipoOperador;
        $options['tipoDiscado'] = $tipoDiscado;
        $options['tipoEmail'] = $tipoEmail;
        $options['tipoDireccion'] = $tipoDireccion;
        $options['organizacion'] = $organizacion;
        $options['categoriaLaboral'] = $categoriaLaboral;
        $options['estadoLaboral'] = $estadoLaboral;

        return $options;
    }

    public function saveFormDireccion(Request $request){
        if($request->isMethod('post')) {
            $getUbigeo = $this->getUbigeo($request->codDepartamento,$request->codProvincia,$request->codDistrito);
            if($request->idPersona){
                $queryDireccion = Direcciones::create([
                    'ID_PERSONA'            => $request->idPersona,
                    'ID_TIPODIRECCION'      => $request->tipoDireccion,
                    'COD_DPTO'              => $getUbigeo[0]['DPTO'],
                    'COD_PROV'              => $getUbigeo[0]['PROV'],
                    'COD_DIST'              => $getUbigeo[0]['DIST'],
                    'DIRECCION'             => Str::upper($request->direccion),
                    'REFERENCIA'            => $request->referencia ? Str::upper($request->referencia) : '',
                    'PRINCIPAL'             => '0',
                    'USER_REG'              => Auth::user()->id,
                    'FECHA_REG'             => Carbon::now(),
                    'TIPO_ESTADO'           => '001'
                ]);

                if($queryDireccion){
                    return ['message' => 'Success', 'action' => 'create', 'datatable' => 'listDireccion'];
                }
                return ['message' => 'Error'];
            }else{
                $queryDireccion = Direcciones::where('ID_DIRECCION', $request->idDireccion)
                    ->update([
                        'ID_TIPODIRECCION'      => $request->tipoDireccion,
                        'COD_DPTO'              => $getUbigeo[0]['DPTO'],
                        'COD_PROV'              => $getUbigeo[0]['PROV'],
                        'COD_DIST'              => $getUbigeo[0]['DIST'],
                        'DIRECCION'             => Str::upper($request->direccion),
                        'REFERENCIA'            => $request->referencia ? Str::upper($request->referencia) : '',
                        'USER_ACT'              => Auth::user()->id,
                        'FECHA_ACT'             => Carbon::now()
                    ]);

                if($queryDireccion){
                    return ['message' => 'Success', 'action' => 'update', 'datatable' => 'listDireccion'];
                }
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }

    public function formPrincipalDireccion(Request $request){
        $getDireccion = $this->getDireccion($request->idDireccion);
        return view('elements/personas/form/direccion/formDireccionPrincipal')->with(array(
            'dataDireccion'             => $getDireccion
        ));
    }

    public function saveFormPrincipalDireccion(Request $request){
        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();

                $setPrincipal = Direcciones::where('ID_PERSONA', $request->personaID)->update(['PRINCIPAL' => 0]);

                if(!$setPrincipal){
                    DB::rollback();
                    return ['message' => 'Error', 'getWhere' => 'Update Principal'];
                }

                $personaUpdate = Personas::where('ID_PERSONA', $request->personaID)
                    ->update([
                        'ID_DIRECCION'  => $request->direccionID
                    ]);

                if(!$personaUpdate){
                    DB::rollback();
                    return ['message' => 'Error'];
                }

                $updatePrincipal = Direcciones::where('ID_DIRECCION', $request->direccionID)->update(['PRINCIPAL' => 1]);

                if(!$updatePrincipal){
                    DB::rollback();
                    return ['message' => 'Error'];
                }

                if($setPrincipal){
                    if($personaUpdate){
                        if($updatePrincipal){
                            DB::commit();
                            return ['message' => 'Success', 'datatable' => 'listDireccion'];
                        }
                    }
                }
            } catch(\Exception $e){
                DB::rollback();
                return ['message' => 'Error'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function listCentroLaboral(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_centro_laboral_list          = $this->centro_laboral_query_list($request->idPersona);
            $builderview                        = $this->builderviewCentroLaboral($query_centro_laboral_list);
            $outgoingcollection                 = $this->outgoingcollectionCentroLaboral($builderview);
            $list_centro_laboral                = $this->FormatDatatable($outgoingcollection);
            return $list_centro_laboral;
        }
    }

    public function centro_laboral_query_list($idPersona){
        $centroLaboral = CentroLaboral::Select()
            ->where('ID_PERSONA', $idPersona)
            ->with('organizacion')
            ->with('categorialaboral')
            ->get()
            ->toArray();
        return $centroLaboral;
    }

    protected function builderviewCentroLaboral($query_centro_laboral_list,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($query_centro_laboral_list as $query) {
            $idList ++;
            $builderview[$posicion]['id']                           = $idList;
            $builderview[$posicion]['id_centro_laboral']            = $query['ID_CENTROLABORAL'];
            $builderview[$posicion]['nombre']                       = $query['organizacion']['RAZON_SOCIAL'];
            $builderview[$posicion]['tipo']                         = $query['categorialaboral']['TIPO_CATEGORIALABORAL'];
            $builderview[$posicion]['principal']                    = $query['PRINCIPAL'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionCentroLaboral($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $iconPrincipal = $view['principal'] == 1 ? 'fa fa-check' : 'fa fa-close';
            $backgroundPrincipal = $view['principal'] == 1 ? 'bg-custom' : 'bg-danger';
            $outgoingcollection->push([
                'id_centro_laboral'     => $view['id'],
                'nombre'                => $view['nombre'],
                'tipo'                  => $view['tipo'],
                'action'                => '<div class="btn-group">
                                                <a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoopExtraLarge','formCentroLaboral', { idCentroLaboral: '".$view['id_centro_laboral']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Centro Laboral" class="fa fa-edit"></i></a>
                                                <a href="javascript:void(0)" class="btn '.$backgroundPrincipal.' text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoop','formPrincipalCentroLaboral', { idCentroLaboral: '".$view['id_centro_laboral']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Seleccionar como Principal" class="'.$iconPrincipal.'"></i></a>
                                            </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formCentroLaboral(Request $request){
        if($request->idPersona){
            return view('elements/personas/form/centroLaboral/formCentroLaboral')->with(array(
                'dataCentroLaboral'         => '',
                'updateForm'                => false,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => $request->idPersona,
                'idCentroLaboral'           => '',
                'selectDepartamento'        => $this->selectDepartamento()
            ));
        }else{
            $getCentroLaboral = $this->getCentroLaboral($request->idCentroLaboral);
            return view('elements/personas/form/centroLaboral/formCentroLaboral')->with(array(
                'dataCentroLaboral'         => $getCentroLaboral,
                'updateForm'                => true,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => '',
                'idCentroLaboral'           => $request->idCentroLaboral,
                'selectDepartamento'        => $this->selectDepartamento()
            ));
        }
    }

    public function saveFormCentroLaboral(Request $request){
        if ($request->isMethod('post')) {
            if($request->idPersona){
                if($request->checkOrganizacion == 'on'){
                    Organizaciones::create([
                        'RAZON_SOCIAL'  => Str::upper($request->nameOrganizacion),
                        'ID_COOP'       => Auth::user()->id_coop,
                        'RUC'           => '0',
                        'TELEFONO'      => '0',
                        'CONTACTO'      => '-',
                        'COD_DPTO'      => '15',
                        'COD_PROV'      => '10',
                        'COD_DIST'      => '10',
                        'DIRECCION'     => '-',
                        'FLG_CONVENIO'  => '0',
                        'CUENTA_CTB'    => '0',
                        'TIPO_ESTADO'   => '001'
                    ]);

                    $organizacionLastID = Organizaciones::select('ID_ORGANIZACION')->orderBy('ID_ORGANIZACION', 'desc')->first();
                }

                $idOrganizacion = $request->checkOrganizacion == 'on' ? $organizacionLastID->ID_ORGANIZACION : $request->idOrganizacion;

                $centroLaboral = CentroLaboral::create([
                    'ID_PERSONA'                => $request->idPersona,
                    'ID_ORGANIZACION'           => $idOrganizacion,
                    'ID_AGENCIA'                => Auth::user()->id_agencia,
                    'ID_ROL'                    => '001',
                    'FECHA_INGRESO'             => $request->fechaIngreso,
                    'ID_TIPOCATEGORIA'          => $request->categoriaLaboral,
                    'AREA'                      => $request->areaLaboral ? Str::upper($request->areaLaboral) : '-',
                    'CARGO'                     => $request->cargoLaboral ? Str::upper($request->cargoLaboral) : '-',
                    'CODIGO'                    => $request->codigoLaboral ? Str::upper($request->codigoLaboral) : '0000',
                    'ID_TIPOESTADOLABORAL'      => $request->estadoLaboral,
                    'PRINCIPAL'                 => 0,
                    'USER_REG'                  => Auth::user()->id,
                    'FECHA_REG'                 => Carbon::now(),
                    'USER_ACT'                  => Auth::user()->id,
                    'FECHA_ACT'                 => Carbon::now(),
                    'TIPO_ESTADO'               => '001'
                ]);

                if($centroLaboral){
                    return ['message' => 'Success', 'action' => 'create', 'datatable' => 'listCentroLaboral'];
                }else{
                    return ['message' => 'Error'];
                }
            }else{

                $centroLaboral = CentroLaboral::where('ID_CENTROLABORAL', $request->idCentroLaboral)->update([
                    'FECHA_INGRESO'             => $request->fechaIngreso,
                    'ID_TIPOCATEGORIA'          => $request->categoriaLaboral,
                    'AREA'                      => $request->areaLaboral,
                    'CARGO'                     => $request->cargoLaboral,
                    'CODIGO'                    => $request->codigoLaboral,
                    'ID_TIPOESTADOLABORAL'      => $request->estadoLaboral,
                    'USER_ACT'                  => Auth::user()->id,
                    'FECHA_ACT'                 => Carbon::now()
                ]);

                if($centroLaboral){
                    return ['message' => 'Success', 'action' => 'update', 'datatable' => 'listCentroLaboral'];
                }else{
                    return ['message' => 'Error'];
                }
            }
        }
        return ['message' => 'Error'];
    }

    public function getCentroLaboral($idCentroLaboral){
        $centroLaboral = CentroLaboral::Select()
            ->where('ID_CENTROLABORAL', $idCentroLaboral)
            ->get()
            ->toArray();
        return $centroLaboral;
    }

    public function listOrganizacion(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_organizacion_list            = $this->organizaciones_query_list();
            $builderview                        = $this->builderviewOrganizacion($query_organizacion_list);
            $outgoingcollection                 = $this->outgoingcollectionOrganizacion($builderview);
            $list_organizacion                  = $this->FormatDatatable($outgoingcollection);
            return $list_organizacion;
        }
    }

    public function organizaciones_query_list(){
        $organizacion = Organizaciones::Select()
            ->get()
            ->toArray();
        return $organizacion;
    }

    protected function builderviewOrganizacion($query_organizacion_list,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($query_organizacion_list as $query) {
            $idList ++;
            $builderview[$posicion]['id']                           = $idList;
            $builderview[$posicion]['id_organizacion']              = $query['ID_ORGANIZACION'];
            $builderview[$posicion]['nombre']                       = $query['RAZON_SOCIAL'];
            $builderview[$posicion]['ruc']                          = $query['RUC'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionOrganizacion($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id_organizacion'       => $view['id'],
                'nombre'                => $view['nombre'],
                'ruc'                   => $view['ruc'],
                'action'                => '<div class="btn-group">
                                                <a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoopLarge','formOrganizacion', { idOrganizacion: '".$view['id_organizacion']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Organizacion" class="fa fa-edit"></i></a>
                                            </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formOrganizacion(Request $request){
        if($request->idPersona){
            return view('elements/personas/form/organizacion/formOrganizacion')->with(array(
                'dataOrganizacion'          => '',
                'updateForm'                => false,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => $request->idPersona,
                'idOrganizacion'            => '',
                'selectDepartamento'        => $this->selectDepartamento()
            ));
        }else{
            $getOrganizacion = $this->getOrganizacion($request->idOrganizacion);
            return view('elements/personas/form/organizacion/formOrganizacion')->with(array(
                'dataOrganizacion'          => $getOrganizacion,
                'updateForm'                => true,
                'selectOptions'             => $this->getOptions(),
                'idPersona'                 => '',
                'idOrganizacion'            => $request->idOrganizacion,
                'selectDepartamento'        => $this->selectDepartamento()
            ));
        }
    }

    public function saveFormOrganizacion(Request $request){
        if ($request->isMethod('post')) {
            $getUbigeo = $this->getUbigeo($request->codDepartamento,$request->codProvincia,$request->codDistrito);
            if($request->idPersona){
                $organizacion = Organizaciones::create([
                    'ID_COOP'           =>  Auth::user()->id_coop,
                    'RUC'               =>  $request->numeroRUC,
                    'RAZON_SOCIAL'      =>  $request->razonSocial,
                    'TELEFONO'          =>  $request->telefonoOrganizacion,
                    'CONTACTO'          =>  $request->emailContacto,
                    'COD_DPTO'          =>  $getUbigeo[0]['DPTO'],
                    'COD_PROV'          =>  $getUbigeo[0]['PROV'],
                    'COD_DIST'          =>  $getUbigeo[0]['DIST'],
                    'DIRECCION'         =>  $request->direccion,
                    'FLG_CONVENIO'      =>  $request->flgConvenio,
                    'CUENTA_CTB'        =>  $request->cuentaCTB,
                    'TIPO_ESTADO'       =>  '001',
                ]);

                if($organizacion){
                    return ['message' => 'Success', 'action' => 'create', 'datatable' => 'listOrganizacion'];
                }else{
                    return ['message' => 'Error'];
                }
            }else{
                $organizacion = Organizaciones::where('ID_ORGANIZACION', $request->idCentroLaboral)->update([
                    'RUC'               =>  $request->numeroRUC,
                    'RAZON_SOCIAL'      =>  Str::upper($request->razonSocial),
                    'TELEFONO'          =>  $request->telefonoOrganizacion  ? $request->telefonoOrganizacion : '444444',
                    'CONTACTO'          =>  $request->emailContacto ? $request->emailContacto : 'correo@organizacion.com',
                    'COD_DPTO'          =>  $getUbigeo[0]['DPTO'],
                    'COD_PROV'          =>  $getUbigeo[0]['PROV'],
                    'COD_DIST'          =>  $getUbigeo[0]['DIST'],
                    'DIRECCION'         =>  $request->direccion ? $request->direccion : '-',
                    'FLG_CONVENIO'      =>  $request->flgConvenio,
                    'CUENTA_CTB'        =>  $request->cuentaCTB ? $request->cuentaCTB : '-'
                ]);

                if($organizacion){
                    return ['message' => 'Success', 'action' => 'update', 'datatable' => 'listOrganizacion'];
                }else{
                    return ['message' => 'Error'];
                }
            }
        }
        return ['message' => 'Error'];
    }

    public function formPrincipalCentroLaboral(Request $request){
        $getCentrolaboral = $this->getCentroLaboral($request->idCentroLaboral);
        return view('elements/personas/form/centroLaboral/formCentroLaboralPrincipal')->with(array(
            'dataCentroLaboral' => $getCentrolaboral
        ));
    }

    public function saveFormPrincipalCentroLaboral(Request $request){
        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();

                $setPrincipal = CentroLaboral::where('ID_PERSONA', $request->personaID)->update(['PRINCIPAL' => 0]);

                if(!$setPrincipal){
                    DB::rollback();
                    return ['message' => 'Error', 'getWhere' => 'Update Principal'];
                }

                $personaUpdate = Personas::where('ID_PERSONA', $request->personaID)
                    ->update([
                        'ID_CENTROLABORAL'  => $request->centroLaboralID
                    ]);

                if(!$personaUpdate){
                    DB::rollback();
                    return ['message' => 'Error'];
                }

                $updatePrincipal = CentroLaboral::where('ID_DIRECCION', $request->direccionID)->update(['PRINCIPAL' => 1]);

                if(!$updatePrincipal){
                    DB::rollback();
                    return ['message' => 'Error'];
                }

                if($setPrincipal){
                    if($personaUpdate){
                        if($updatePrincipal){
                            DB::commit();
                            return ['message' => 'Success', 'datatable' => 'listCentroLaboral'];
                        }
                    }
                }
            } catch(\Exception $e){
                DB::rollback();
                return ['message' => 'Error'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function getOrganizacion($idOrganizacion){
        $organizacion = Organizaciones::Select()
            ->where('ID_ORGANIZACION', $idOrganizacion)
            ->get()
            ->toArray();
        return $organizacion;
    }
}
