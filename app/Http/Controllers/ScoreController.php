<?php

namespace App\Http\Controllers;

use App\Models\MenuUsers;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;

class ScoreController extends Controller
{
    public function getUserInformation(){
        $user = User::Select()
            ->where('id', Auth::id())
            ->get()
            ->toArray();

        return $user;
    }

    public function getUserMenu(){
        $menu = MenuUsers::Select()
            ->where('user_id', Auth::id())
            ->get()
            ->toArray();

        return $menu;
    }

    public function getUtilitiesUser () {
        $total = User::Select()
            ->get()
            ->count();

        $total_month = User::Select()
            ->whereMonth('created_at', Carbon::now()->format('m'))
            ->get()
            ->count();

        $last = User::latest()->first();

        $userUtilities['total'] = $total;
        $userUtilities['total_month'] = $total_month;
        $userUtilities['last'] = $last;

        return $userUtilities;
    }

    protected function FormatDatatable($collection)
    {
        return DataTables::of($collection)
                    ->escapeColumns([])
                    ->make(true);
    }

    public function cookieAuthRemove(){
        Cookie::queue(Cookie::forget('cookie_username'));
        return redirect('/');
    }

    protected function makeDirectory($ubicacion){
        return File::makeDirectory($ubicacion, 0777, true);
    }

    protected function resetCookie($nameCookie, $setValueCookie){
        Cookie::queue(Cookie::forget($nameCookie));
        return Cookie::queue($nameCookie, $setValueCookie, 2147483647);
    }
}