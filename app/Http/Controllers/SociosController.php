<?php

namespace App\Http\Controllers;

use App\Models\CartasBeneficiarios;
use App\Models\Personas;
use App\Models\Socios;
use App\Models\TipoSexo;
use App\Models\TipoSituacionSocio;
use App\Models\TipoSocio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SociosController extends ScoreController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('elements/socios/index')->with(array(
            'titleModule'       => 'Buscador de Socios',
            'iconModule'        => 'feather feather-briefcase'
        ));
    }

    public function listSocios(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_socio_list           = $this->filterSearchSocio($request);
            $builderview                = $this->builderviewSocios($query_socio_list);
            $outgoingcollection         = $this->outgoingcollectionSocios($builderview);
            $list_socios                = $this->FormatDatatable($outgoingcollection);
            return $list_socios;
        }
    }

    protected function builderviewSocios($persona_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($persona_list_query as $query) {
            $namePersona = (empty($query['persona']['RAZON_SOCIAL']) ? ucwords(Str::lower($query['persona']['NOMBRES'].' '.$query['persona']['APELLIDO_PAT'].' '.$query['persona']['APELLIDO_MAT'])) : ucwords(Str::lower($query['persona']['RAZON_SOCIAL'])));
            $idList ++;
            $builderview[$posicion]['id_persona']               = $query['persona']['ID_PERSONA'];
            $builderview[$posicion]['nombres']                  = $namePersona;
            $builderview[$posicion]['nro_documento']            = $query['persona']['NRO_DOCUMENTO'];
            $builderview[$posicion]['fec_nacimiento']           = $query['persona']['FECHA_NACIMIENTO'];
            $builderview[$posicion]['tipo_sexo']                = $query['persona']['ID_TIPOSEXO'] != 0 ? $query['persona']['ID_TIPOSEXO'] : '-';
            $builderview[$posicion]['id_socio']                 = $query['persona']['ID_SOCIO'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionSocios($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            if($view['tipo_sexo'] == '-') $iconSexo = '<span class="text-facebook"><i class="list-icon fa fa-briefcase" aria-hidden="true"></i> Empresa</span>';
            elseif ($view['tipo_sexo'] == '1') $iconSexo = '<span class="text-twitter"><i class="list-icon fa fa-male" aria-hidden="true"></i> Masculino</span>';
            else $iconSexo = '<span class="text-dribbble"><i class="list-icon fa fa-male" aria-hidden="true"></i> Femenino</span>';

            if($view['tipo_sexo'] == '-') $imgAvatar = asset('img/empresas.png');
            elseif ($view['tipo_sexo'] == '1') $imgAvatar = asset('img/masculino.png');
            else $imgAvatar = asset('img/femenino.png');

            $btnSocio = $view['id_socio'] ? '<a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="vmFront.loadOptionMenu('."'formSocios', { idPersona : '".$view['id_persona']."' }, 'post'".')"><i title="Editar Socio" class="fa fa-edit"></i></a>' : '<a href="javascript:void(0)" class="btn bg-primary text-white btn-sm" onclick="vmFront.loadOptionMenu('."'formSocios', { idPersona : '".$view['id_persona']."' }, 'get'".')"><i title="Agregar Socio" class="fa fa-plus"></i></a>';

            $statusSocio = $view['id_socio'] ? '<label class="badge px-3 heading-font-family bg-success">Si Es Socio</label>' : '<label class="badge px-3 heading-font-family bg-danger">No Es Socio</label>';

            $outgoingcollection->push([
                'id_persona'        => $view['id_persona'],
                'nombres'           => '<span class="avatar thumb-xxs mr-b-0"><img class="rounded-circle" src="'.$imgAvatar.'?version='.Carbon::now()->format('YmdHis').'"> '.$view['nombres'].'</span>',
                'nro_documento'     => $view['nro_documento'],
                'fec_nacimiento'    => '<label class="badge px-3 heading-font-family bg-custom">'.Carbon::parse($view['fec_nacimiento'])->format('d-m-Y').'</label>',
                'tipo_sexo'         => $iconSexo,
                'status_socio'      => $statusSocio,
                'action'            => '<div class="btn-group">
                                            '.$btnSocio.'
                                        </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function filterSearchSocio($dataSearch){

        $wherePersona = array();

        $whereCustom = ['ID_COOP', '=', Auth::user()->id_coop];
        array_push($wherePersona, $whereCustom);

        if(isset($dataSearch[0]['value'])){
            $whereCustom = ['APELLIDO_PAT', 'like', '%'.$dataSearch[0]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[1]['value'])){
            $whereCustom = ['APELLIDO_MAT', 'like' ,'%'.$dataSearch[1]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[2]['value'])){
            $whereCustom = ['NOMBRES', 'like' ,'%'.$dataSearch[2]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[3]['value'])){
            $whereCustom = ['RAZON_SOCIAL', 'like' ,'%'.$dataSearch[3]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[4]['value'])){
            $whereCustom = ['NRO_DOCUMENTO', 'like' ,'%'.$dataSearch[4]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[5]['value'])){
            $whereCustom = ['CODIGO', 'like' ,'%'.$dataSearch[5]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }
        if(isset($dataSearch[6]['value'])){
            $whereCustom = ['ID_PERSONA', 'like' ,'%'.$dataSearch[6]['value'].'%'];
            array_push($wherePersona, $whereCustom);
        }

        $searchSocio = Socios::with('persona')->whereHas('persona', function($query) use ($wherePersona) {
            $query->where($wherePersona);
        })->get()->toArray();

        return $searchSocio;
    }

    public function formSocios(Request $request) {
        if ($request->isMethod('get')) {
            return view('elements/socios/form/formSocio')->with(array(
                'titleModule'               => 'Agregar Socio',
                'iconModule'                => 'feather feather-briefcase',
                'dataSocio'                 => '',
                'idPersona'                 => $request->idPersona,
                'updateForm'                => false,
                'selectOptions'             => $this->getOptions()
            ));
        }else{
            $getSocio = $this->getSocio($request->idPersona);
            return view('elements/socios/form/formSocio')->with(array(
                'titleModule'               => 'Editar Socio',
                'iconModule'                => 'feather feather-briefcase',
                'dataSocio'                 => $getSocio,
                'idPersona'                 => $request->idPersona,
                'updateForm'                => true,
                'selectOptions'             => $this->getOptions()
            ));
        }
    }

    public function getSocio($idPersona){
        $socio = Socios::Select()
            ->where('ID_PERSONA', $idPersona)
            ->get()
            ->toArray();

        return $socio;
    }

    public function getOptions(){
        $tipoSocio = TipoSocio::Select()
            ->get()
            ->toArray();

        $tipoSituacionSocio = TipoSituacionSocio::Select()
            ->get()
            ->toArray();

        $options['tipoSocio'] = $tipoSocio;
        $options['tipoSituacionSocio'] = $tipoSituacionSocio;

        return $options;
    }

    public function saveFormDatosSocios(Request $request){
        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();

                if($request->socioID){
                    $socioQuery = Socios::where('ID_SOCIO', $request->socioID)
                        ->update([
                            'FECHA_INGRESO'                 => $request->fechaIngreso,
                            'ID_TIPOSOCIO'                  => $request->tipoSocio,
                            'CUOTA_APORTE'                  => $request->cuotaAporte,
                            'OBSERVACIONES'                 => $request->textObservacion ? Str::upper($request->textObservacion) : '-',
                            'USER_ACT'                      => Auth::user()->id,
                            'FECHA_ACT'                     => Carbon::now(),
                        ]);

                    if(!$socioQuery){
                        DB::rollback();
                        return ['message' => 'Error'];
                    }

                    if($socioQuery){
                        DB::commit();
                        return ['message' => 'Success', 'action' => 'update', 'idSocio' => $request->socioID];
                    }
                }else{
                    $getIDSocio = DB::select('SELECT F_GET_ID_SOCIO ("'.Auth::user()->id_coop.'") AS ID_SOCIO');

                    if(!$getIDSocio){
                        DB::rollback();
                        return ['message' => 'Error', 'getWhere' => 'GET_ID_SOCIO'];
                    }

                    $personQueryPersona = Socios::create([
                        'ID_SOCIO'                      => $getIDSocio[0]->ID_SOCIO,
                        'ID_COOP'                       => Auth::user()->id_coop,
                        'ID_PERSONA'                    => $request->personaID,
                        'FECHA_INGRESO'                 => $request->fechaIngreso,
                        'ID_TIPOSOCIO'                  => $request->tipoSocio,
                        'ID_AGENCIA'                    => Auth::user()->id_agencia,
                        'CUOTA_APORTE'                  => $request->cuotaAporte,
                        'OBSERVACIONES'                 => $request->textObservacion ? Str::upper($request->textObservacion) : null,
                        'ID_TIPOSITUACION'              => 1,
                        'ID_TIPOMOTIVOBAJA'             => 0,
                        'OBSERVACION_BAJA'              => null,
                        'FECHA_BAJA'                    => null,
                        'USER_BAJA'                     => null,
                        'ID_CARTA'                      => null,
                        'USER_REG'                      => Auth::user()->id,
                        'FECHA_REG'                     => Carbon::now(),
                        'USER_ACT'                      => Auth::user()->id,
                        'FECHA_ACT'                     => Carbon::now(),
                        'TIPO_ESTADO'                   => '001'
                    ]);

                    if(!$personQueryPersona){
                        DB::rollback();
                        return ['message' => 'Error', 'getWhere' => 'Create'];
                    }

                    if($getIDSocio){
                        if($personQueryPersona){
                            DB::commit();
                            return ['message' => 'Success', 'action' => 'create', 'idSocio' => $getIDSocio[0]->ID_SOCIO];
                        }
                    }
                }
            } catch(\Exception $e){
                DB::rollback();
                return ['message' => 'Error'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function listCartasBeneficiarios(Request $request)
    {
        if ($request->isMethod('get')) {
            $query_carta_list           = $this->filterCartasBeneficiarios($request->idSocio);
            $builderview                = $this->builderviewCartasBeneficiarios($query_carta_list);
            $outgoingcollection         = $this->outgoingcollectionCartasBeneficiarios($builderview);
            $list_cartas                = $this->FormatDatatable($outgoingcollection);
            return $list_cartas;
        }
    }

    protected function builderviewCartasBeneficiarios($cartas_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($cartas_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['id_carta']                 = $query['ID_CARTA'];
            $builderview[$posicion]['id_socio']                 = $query['ID_SOCIO'];
            $builderview[$posicion]['nro_carta']                = $query['NRO_CARTA'];
            $builderview[$posicion]['fec_carta']                = $query['FECHA_CARTA'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionCartasBeneficiarios($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {

            $outgoingcollection->push([
                'id_carta'          => $view['id'],
                'nro_carta'         => $view['nro_carta'],
                'fec_carta'         => '<label class="badge px-3 heading-font-family bg-custom">'.Carbon::parse($view['fec_carta'])->format('d-m-Y').'</label>',
                'action'            => '<div class="btn-group">
                                            <a href="javascript:void(0)" class="btn bg-graycoop text-white btn-sm" onclick="responseModal('."'div.dialogGrayCoopLarge','formCartaBeneficiario', { idCarta: '".$view['id_carta']."'".' })" data-toggle="modal" data-target="#modalScore"><i title="Editar Carta" class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" class="btn bg-custom text-white btn-sm" onclick="vmFront.loadOptionMenu('."'formPersonas', { idCarta : '".$view['id_carta']."' }".')"><i title="Adicionar Beneficiarios" class="fa fa-group"></i></a>
                                        </div>',
            ]);
        }
        return $outgoingcollection;
    }

    public function filterCartasBeneficiarios($idSocio){
        $cartasBeneficiarios = CartasBeneficiarios::Select()
            ->where('ID_SOCIO', $idSocio)
            ->get()
            ->toArray();

        return $cartasBeneficiarios;
    }

    public function formCartaBeneficiario(Request $request){
        if($request->idSocio){
            return view('elements/socios/form/cartaBeneficiario/formCartaBeneficiario')->with(array(
                'dataCartaBeneficiario'         => '',
                'updateForm'                    => false,
                'idSocio'                       => $request->idSocio,
                'idCarta'                       => ''
            ));
        }else{
            $getOrganizacion = $this->getCartasBeneficiarios($request->idCarta);
            return view('elements/socios/form/cartaBeneficiario/formCartaBeneficiario')->with(array(
                'dataCartaBeneficiario'         => $getOrganizacion,
                'updateForm'                    => true,
                'idSocio'                       => '',
                'idCarta'                       => $request->idCarta
            ));
        }
    }

    public function getCartasBeneficiarios($idCarta){
        $cartasBeneficiarios = CartasBeneficiarios::Select()
            ->where('ID_CARTA', $idCarta)
            ->get()
            ->toArray();

        return $cartasBeneficiarios;
    }

    public function saveFormCartaBeneficiario(Request $request){
        if ($request->isMethod('post')) {
            if($request->idSocio){
                $cartaBeneficiario = CartasBeneficiarios::create([
                    'ID_COOP'           =>  Auth::user()->id_coop,
                    'ID_SOCIO'          =>  $request->idSocio,
                    'FECHA_CARTA'       =>  $request->fechaCarta,
                    'NRO_CARTA'         =>  $request->numeroCarta,
                    'FPS'               =>  $request->fpsCarta ? $request->fpsCarta : null,
                    'OBSERVACIONES'     =>  $request->observacionesCarta ? Str::upper($request->observacionesCarta) : null,
                    'BAJA_FPS'          =>  null,
                    'FECHA_BAJAFPS'     =>  null,
                    'USER_BAJAFPS'      =>  null,
                    'MOTIVO_BAJAFPS'    =>  null,
                    'USER_REG'          => Auth::user()->id,
                    'FECHA_REG'         => Carbon::now(),
                    'USER_ACT'          => Auth::user()->id,
                    'FECHA_ACT'         => Carbon::now(),
                    'TIPO_ESTADO'       => '001'
                ]);

                if($cartaBeneficiario){
                    return ['message' => 'Success', 'action' => 'create', 'datatable' => 'listCartasBeneficiarios'];
                }else{
                    return ['message' => 'Error'];
                }

            }else{

                $cartaBeneficiario = CartasBeneficiarios::where([
                    ['ID_CARTA', $request->idCarta]
                ])->update([
                    'FECHA_CARTA'       =>  $request->fechaCarta,
                    'NRO_CARTA'         =>  $request->numeroCarta,
                    'FPS'               =>  $request->fpsCarta ? $request->fpsCarta : null,
                    'OBSERVACIONES'     =>  $request->observacionesCarta ? Str::upper($request->observacionesCarta) : null,
                    'USER_ACT'          => Auth::user()->id,
                    'FECHA_ACT'         => Carbon::now()
                ]);

                if($cartaBeneficiario){
                    return ['message' => 'Success', 'action' => 'update', 'datatable' => 'listCartasBeneficiarios'];
                }else{
                    return ['message' => 'Error'];
                }

            }
        }
        return ['message' => 'Error'];
    }
}
