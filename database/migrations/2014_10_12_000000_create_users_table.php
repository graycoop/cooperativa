<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('primer_nombre');
            $table->string('segundo_nombre');
            $table->string('primer_apellido');
            $table->string('segundo_apellido');
            $table->unsignedInteger('id_tipo_identidad');
            $table->unsignedBigInteger('numero_identidad');
            $table->date('fecha_nacimiento');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('avatar')->default('img/avatars/user_default.png');
            $table->string('locale')->default(config('app.locale'));
            $table->string('session_id', 100)->nullable();
            $table->unsignedInteger('id_estado');
            $table->unsignedInteger('id_persona');
            $table->unsignedInteger('id_coop');
            $table->unsignedInteger('id_agencia');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
