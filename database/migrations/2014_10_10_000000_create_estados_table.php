<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TIPOESTADO', function (Blueprint $table) {
            $table->increments('ID_TIPOESTADO');
            $table->string('TIPO_ESTADO',15)->unique();
            $table->string('COLOR_ESTADO',30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TIPOESTADO');
    }
}
