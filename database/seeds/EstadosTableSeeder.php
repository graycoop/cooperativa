<?php

use Illuminate\Database\Seeder;
use App\Models\TipoEstado;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estado_1 = new TipoEstado();
        $estado_1->TIPO_ESTADO = 'Activo';
        $estado_1->COLOR_ESTADO = 'bg-success';

        $estado_2 = new TipoEstado();
        $estado_2->TIPO_ESTADO = 'Inactivo';
        $estado_2->COLOR_ESTADO = 'bg-danger';
    }
}
