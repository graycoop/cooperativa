<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $user = new User();
        $user->primer_nombre = 'User';
        $user->segundo_nombre = 'Test';
        $user->primer_apellido = 'Last';
        $user->segundo_apellido = 'Second';
        $user->id_tipo_identidad = '1';
        $user->numero_identidad = '76797459';
        $user->fecha_nacimiento = '1994-08-12';
        $user->email = 'user@example.com';
        $user->username = 'test';
        $user->password = bcrypt('12345');
        $user->id_estado = 1;
        $user->id_persona = 001;
        $user->id_coop = 0121;
        $user->id_agencia = 001;
        $user->save();
        $user->roles()->attach($role_user);

        $role_admin = Role::where('name', 'admin')->first();
        $admin = new User();
        $admin->primer_nombre = 'Admin';
        $admin->segundo_nombre = 'Score';
        $admin->primer_apellido = 'Last';
        $admin->segundo_apellido = 'Second';
        $admin->id_tipo_identidad = '1';
        $admin->numero_identidad = '76797459';
        $admin->fecha_nacimiento = '1994-08-12';
        $admin->email = 'admin@example.com';
        $admin->username = 'admin';
        $admin->password = bcrypt('12345');
        $admin->id_estado = 1;
        $admin->id_persona = 001;
        $admin->id_coop = 0121;
        $admin->id_agencia = 001;
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
