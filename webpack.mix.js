const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* Estilo General */

mix.sass('resources/assets/bonvue/scss/login.scss', 'public/css')
    .sass('resources/assets/bonvue/scss/front.scss', 'public/css')
    .sass('resources/assets/bonvue/scss/pace.scss', 'public/css')

/* Estilos y Javascript para el Login */

    .styles([
        'resources/assets/bonvue/vendors/material-icons/material-icons.css',
        'resources/assets/bonvue/vendors/linea-icons/styles.css',
        'resources/assets/bonvue/vendors/mono-social-icons/monosocialiconsfont.css',
        'resources/assets/bonvue/vendors/feather-icons/feather.css',
        'resources/utilities/css/login.css'
    ], 'public/css/securitec_login.css')

    .babel([
        'node_modules/pace-js/pace.js'
    ], 'public/js/node_login.js')

    .babel([
        'resources/utilities/js/helper_login.js'
    ], 'public/js/helper_login.js')

/* Fin Login */

/* Estilos y Javascript para el Front */

    .styles([
        'resources/assets/bonvue/vendors/material-icons/material-icons.css',
        'resources/assets/bonvue/vendors/linea-icons/styles.css',
        'resources/assets/bonvue/vendors/mono-social-icons/monosocialiconsfont.css',
        'resources/assets/bonvue/vendors/feather-icons/feather.css',
        'node_modules/mediaelement/src/css/mediaelementplayer.css',
        'node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.css',
        'node_modules/datatables/media/css/jquery.dataTables.css',
        'node_modules/vue2-animate/dist/vue2-animate.css',
        'node_modules/datedropper/datedropper.css',
        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
        'resources/assets/bonvue/vendors/bootstrap-select/css/bootstrap-select.css',
        'resources/assets/bonvue/vendors/dropzone/dropzone.css',
        'resources/utilities/js/datatables/plugins/responsive/responsive.bootstrap4.css',
        'resources/utilities/css/front.css'
    ], 'public/css/securitec_front.css')

    .js([
        'resources/assets/js/app.js'
    ], 'public/js/app_score.js')

    .babel([
        'node_modules/socket.io-client/socket.io.min.js',
        'node_modules/pace-js/pace.js',
        'node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.js',
        'node_modules/jquery-form-validator/form-validator/jquery.form-validator.js',
        'node_modules/datedropper/datedropper.js',
        'node_modules/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js',
        'resources/assets/bonvue/vendors/bootstrap-select/js/bootstrap-select.js',
        'resources/assets/bonvue/vendors/dropzone/dropzone.js',
        'node_modules/countup.js/countUp.js',
        'node_modules/countup.js/countUp-jquery.js'
    ], 'public/js/node_front.js')

    .babel([
        'resources/assets/bonvue/js/theme.js',
        'resources/utilities/js/cookie.js',
        'resources/utilities/js/helper_front.js'
    ], 'public/js/helper_front.js')

    .babel([
        'resources/utilities/js/datatables/plugins/responsive/dataTables.responsive.min.js',
        'resources/utilities/js/datatables/plugins/responsive/responsive.bootstrap4.min.js',
        'resources/utilities/js/datatables/language.js',
        'resources/utilities/js/datatables/columns.js',
        'resources/utilities/js/datatables.js'
    ], 'public/js/score_datatables.js')

    .babel([
        '.env.js',
        'resources/utilities/js/nodejs/initScore.js'
    ], 'public/js/scoreNode.js').version();

    /* Fin Front */

mix.copy('resources/utilities/js/vue'                               , 'public/js/vue')
    .copy('resources/assets/fonts'                                  , 'public/fonts')
    .copy('resources/assets/bonvue/vendors'                         , 'public/assets/utilities')
    .copy('node_modules/datedropper/dd-icon'                        , 'public/css/dd-icon')
    .copy('resources/utilities/js/form'                             , 'public/js/form')
    .copy('resources/utilities/favicon.png'                         , 'public/favicon.png')
    .copy('resources/assets/images'                                 , 'public/images')
    .copy('resources/assets/img'                                    , 'public/img');
