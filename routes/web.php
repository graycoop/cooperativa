<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {

    /* Routes Auth */
    Auth::routes();

    /* Routes Default */
    Route::get('/',                                     'HomeController@index')->name('home');
    Route::get('/home',                                 'HomeController@index')->name('home');
    Route::get('/getUserInformation',                   'ScoreController@getUserInformation')->name('getUserInformation');
    Route::get('/getUserMenu',                          'ScoreController@getUserMenu')->name('getUserMenu');
    Route::get('/getUtilitiesUser',                     'ScoreController@getUtilitiesUser')->name('getUtilitiesUser');

    /* Pruebas */
    Route::get('/blankPage',                            'HomeController@blankPage')->name('Inicio');

    /* RemoveAuthCookie */
    Route::get('/removeAuthCookie',                     'ScoreController@cookieAuthRemove')->name('removeAuthCookie');

    /* Route Avatar */
    Route::get('/formAvatar',                           'UserController@formAvatar')->name('formAvatar');
    Route::post('/saveFormAvatar',                      'UserController@saveFormAvatar')->name('saveFormAvatar');

    /* Route User */
    Route::get('/users',                                'UserController@index')->name('manageUsers');
    Route::get('/listUsers',                            'UserController@listUsers')->name('listUsers');
    Route::match(['get', 'post'],   '/formUsers',       'UserController@formUsers')->name('formUsers');
    Route::post('/formUsersMenu',                       'UserController@formUsersMenu')->name('formUsersMenu');
    Route::post('/formUsersStatus',                     'UserController@formUsersStatus')->name('formUsersStatus');
    Route::post('/saveFormUsers',                       'UserController@saveFormUsers')->name('saveFormUsers');
    Route::post('/saveFormUsersMenu',                   'UserController@saveFormUsersMenu')->name('saveFormUsersMenu');
    Route::post('/saveFormUsersStatus',                 'UserController@saveFormUsersStatus')->name('saveFormUsersStatus');

    /* Route Role */
    Route::get('/roles',                                'RoleController@index')->name('manageRoles');
    Route::get('/listRoles',                            'RoleController@listRoles')->name('listRoles');
    Route::match(['get', 'post'],   '/formRoles',       'RoleController@formRoles')->name('formRoles');
    Route::post('/formRolesAssing',                     'RoleController@formRolesAssing')->name('formRolesAssing');
    Route::post('/formRolesMenu',                       'RoleController@formRolesMenu')->name('formRolesMenu');
    Route::post('/saveFormRoles',                       'RoleController@saveFormRoles')->name('saveFormRoles');
    Route::post('/saveFormRolesAssing',                 'RoleController@saveFormRolesAssing')->name('saveFormRolesAssing');
    Route::post('/saveFormRolesMenu',                   'RoleController@saveFormRolesMenu')->name('saveFormRolesMenu');

    /* Route Personas */
    Route::get('/personas',                                 'PersonasController@index')->name('personas');
    Route::get('/listPersonas',                             'PersonasController@listPersonas')->name('listPersonas');
    Route::post('/formPersonas',                            'PersonasController@formPersonas')->name('formPersonas');
    Route::post('/formTipoPersona',                         'PersonasController@formTipoPersona')->name('formTipoPersona');
    Route::post('/saveFormDatosPersonales',                 'PersonasController@saveFormDatosPersonales')->name('saveFormDatosPersonales');
    Route::get('/listTelefonos',                            'PersonasController@listTelefonos')->name('listTelefonos');
    Route::match(['get', 'post'],   '/formTelefonos',       'PersonasController@formTelefonos')->name('formTelefonos');
    Route::post('/saveFormTelefonos',                       'PersonasController@saveFormTelefonos')->name('saveFormTelefonos');
    Route::get('/listEmails',                               'PersonasController@listEmails')->name('listEmails');
    Route::match(['get', 'post'],   '/formEmails',          'PersonasController@formEmails')->name('formEmails');
    Route::post('/saveFormEmails',                          'PersonasController@saveFormEmails')->name('saveFormEmails');
    Route::get('/listDireccion',                            'PersonasController@listDireccion')->name('listDireccion');
    Route::match(['get', 'post'],   '/formDireccion',       'PersonasController@formDireccion')->name('formDireccion');
    Route::post('/saveFormDireccion',                       'PersonasController@saveFormDireccion')->name('saveFormDireccion');
    Route::post('/formPrincipalDireccion',                  'PersonasController@formPrincipalDireccion')->name('formPrincipalDireccion');
    Route::post('/saveFormPrincipalDireccion',              'PersonasController@saveFormPrincipalDireccion')->name('saveFormPrincipalDireccion');
    Route::post('/selectProvincia',                         'PersonasController@selectProvincia')->name('selectProvincia');
    Route::post('/selectDistrito',                          'PersonasController@selectDistrito')->name('selectDistrito');
    Route::get('/listCentroLaboral',                        'PersonasController@listCentroLaboral')->name('listCentroLaboral');
    Route::match(['get', 'post'],   '/formCentroLaboral',   'PersonasController@formCentroLaboral')->name('formCentroLaboral');
    Route::post('/saveFormCentroLaboral',                   'PersonasController@saveFormCentroLaboral')->name('saveFormCentroLaboral');
    Route::post('/formPrincipalCentroLaboral',              'PersonasController@formPrincipalCentroLaboral')->name('formPrincipalCentroLaboral');
    Route::post('/saveFormPrincipalCentroLaboral',          'PersonasController@saveFormPrincipalCentroLaboral')->name('saveFormPrincipalCentroLaboral');
    Route::get('/listOrganizacion',                         'PersonasController@listOrganizacion')->name('listOrganizacion');
    Route::match(['get', 'post'],   '/formOrganizacion',    'PersonasController@formOrganizacion')->name('formOrganizacion');
    Route::post('/saveFormOrganizacion',                    'PersonasController@saveFormOrganizacion')->name('saveFormOrganizacion');

    /* Route Socios */
    Route::get('/socios',                                       'SociosController@index')->name('socios');
    Route::get('/listSocios',                                   'SociosController@listSocios')->name('listSocios');
    Route::match(['get', 'post'],   '/formSocios',              'SociosController@formSocios')->name('formSocios');
    Route::post('/saveFormDatosSocios',                         'SociosController@saveFormDatosSocios')->name('saveFormDatosSocios');
    Route::get('/listCartasBeneficiarios',                      'SociosController@listCartasBeneficiarios')->name('listCartasBeneficiarios');
    Route::match(['get', 'post'],   '/formCartaBeneficiario',   'SociosController@formCartaBeneficiario')->name('formCartaBeneficiario');
    Route::post('/saveFormCartaBeneficiario',                   'SociosController@saveFormCartaBeneficiario')->name('saveFormCartaBeneficiario');


});