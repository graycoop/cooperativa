<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/png" href="{!! asset('favicon.png?version='.date('YmdHis'))!!}" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no ,shrink-to-fit=no" name="viewport">
    <link href="{!! asset('css/securitec_login.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('css/login.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
</head>
<body class="body-bg-full error-page error-404">
    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            <main class="main-wrapper">
                <div class="page-title">
                    <h1 class="color-white">Oops !!</h1>
                </div>
                <h3 class="mr-b-5 color-white">Verificamos que actualmente te encuentras conectado, debes desconectarte primero para iniciar nuevamente tu sesión</h3>
                <hr>
                <a href="/login" class="btn btn-primary btn-rounded btn-block fw-700 text-uppercase"><i class="list-icon feather feather-refresh-ccw"></i> Regresar al Login</a>
            </main>
        </div>
    </div>
</body>