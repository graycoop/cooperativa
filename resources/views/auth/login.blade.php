@extends('layouts.LayoutLogin')

@section('content')
    @if(Cookie::get('cookie_username')) @include('layouts.loginElements.LoginAvatar') @endif
    <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="form-group" @if(Cookie::get('cookie_username')) style="display:none" @endif>
            <label>@lang('auth.formAuth.username')</label>
            <input id="username" type="text" class="form-control form-control-line {{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="@lang('auth.formAuth.username')" name="username" @if(Cookie::get('cookie_username')) value="{{ Cookie::get('cookie_username') }}" @else value="{{ old('username') }}" @endif>
            @if ($errors->has('username'))
                <div class="invalid-feedback"><strong><i class="fa fa-warning"></i> {{ $errors->first('username') }}</strong></div>
            @endif
        </div>
        <div class="form-group">
            <label>@lang('auth.formAuth.password')</label>
            @if(Cookie::get('cookie_username'))
                <input id="password" type="password" class="form-control form-control-line {{ $errors->has('password') || $errors->has('username') ? ' is-invalid' : '' }}" placeholder="@lang('auth.formAuth.password')" name="password">
                @if ($errors->has('password') || $errors->has('username'))
                    <div class="invalid-feedback"><strong><i class="fa fa-warning"></i> {{ $errors->first('password') ? $errors->first('password') : $errors->first('username') }}</strong></div>
                @endif
            @else
                <input id="password" type="password" class="form-control form-control-line {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="@lang('auth.formAuth.password')" name="password">
                @if ($errors->has('password'))
                    <div class="invalid-feedback"><strong><i class="fa fa-warning"></i> {{ $errors->first('password') }}</strong></div>
                @endif
            @endif
        </div>
        <div class="form-group">
            <div class="container">
                <div class="row">
                    <!--
                    <div class="col-md-6" style="display:none">
                        <div class="checkbox checkbox-primary">
                            <label class="d-flex">
                                <input type="checkbox" name="remember" checked> <span class="label-text">@lang('auth.formAuth.remenber')</span>
                            </label>
                        </div>
                    </div>
                    -->
                    <div class="col-md-12 text-center">
                        <div class="@if(Cookie::get('cookie_username')) btn-group mr-b-20 @endif">
                            <button id="submitLogin" type="submit" class="btn @if(!Cookie::get('cookie_username')) btn-block btn-lg mr-b-20 @endif btn-primary text-uppercase fs-12 fw-600"><i class="fa fa-sign-in"></i> @lang('auth.formAuth.buttonLogin')</button>
                            @if(Cookie::get('cookie_username')) <a href="/removeAuthCookie" id="changeUsername" class="btn btn-securitec text-uppercase fs-12 fw-600"><i class="fa fa-sign-out"></i> @lang('auth.formAuth.changeUser')</a> @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
