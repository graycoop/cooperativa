@extends('layouts.LayoutFront')
@section('content')
    <div id="container">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Cargando Plataforma</h5>
                        <div class="widget-actions">
                            <a href="javascript:void(0)"><i class="fa fa-cog fa-spin"></i></a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="cssload-loader"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
