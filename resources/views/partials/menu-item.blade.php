@if ($item['submenu'] == [])
    <li v-if="menu.indexOf('{{ $item["vue_name"] }}') >= 0">
        <a href="javascript:void(0);" v-on:click="loadOptionMenu('{{ $item['vue_name'] }}', {}, 'get')">
            <i class="list-icon {{ $item['icon'] }}"></i> <span class="hide-menu">{{ $item['name'] }} </span>
        </a>
    </li>
@else
    <li class="menu-item-has-children" v-if="menu.indexOf('{{ $item["vue_name"] }}') >= 0">
        <a href="javascript:void(0);">
            <i class="list-icon {{ $item['icon'] }}"></i> <span class="@if($hide) hide-menu @endif">{{ $item['name'] }}</span>
        </a>
        <ul class="list-unstyled sub-menu">
            @foreach ($item['submenu'] as $submenu)
                @if ($submenu['submenu'] == [])
                    <li v-if="menu.indexOf('{{ $submenu["vue_name"] }}') >= 0">
                        <a href="javascript:void(0);" v-on:click="loadOptionMenu('{{ $submenu['vue_name'] }}', {}, 'get')">
                            <i class="list-icon {{ $submenu['icon'] }}"></i> <span>{{ $submenu['name'] }} </span>
                        </a>
                    </li>
                @else
                    @include('partials.menu-item', [ 'item' => $submenu, 'hide' => false ])
                @endif
            @endforeach
        </ul>
    </li>
@endif