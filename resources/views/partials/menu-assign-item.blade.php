@if ($item['submenu'] == [])
    <li class="checkbox checkbox-primary">
        <label class="checkbox-checked">
            <input type="checkbox" name="checkMenu[]" value="{{ $item['vue_name'] }}" class="checkNew" {{ (collect($menuCheck)->pluck('vue_name')->contains($item['vue_name']) ? 'checked' : '') }}>
                <span class="label-text">
                    <a href="javascript:void(0);">
                        <i class="list-icon {{ $item['icon'] }}"></i> {{ $item['name'] }}
                    </a>
                </span>
        </label>
    </li>
@else
    <li class="menu-item-has-children checkbox checkbox-primary">
        <label class="checkbox-checked">
            <input type="checkbox" name="checkMenu[]" value="{{ $item['vue_name'] }}"
                   {{ (collect($menuCheck)->pluck('vue_name')->contains($item['vue_name']) ? 'checked' : '') }}
                   class="checkNew @if($subMenuCheck) checkGeneralSub @else checkGeneralSub_{{ $item['id'] }} checkMenuSub @endif"
                   onclick="@if($subMenuCheck) mark_all_menu('.checkGeneralSub', 'checkMenuSub') @else mark_all_menu('.checkGeneralSub_{{ $item['id'] }}', 'checkMenuSub_{{ $item['id'] }}') @endif">

            <span class="label-text">
                <a href="javascript:void(0);">
                    <i class="list-icon {{ $item['icon'] }}"></i> {{ $item['name'] }}
                </a>
            </span>
        </label>
        <ul class="sub-menu list-unstyled">
            @foreach ($item['submenu'] as $key => $submenu)
                @if ($submenu['submenu'] == [])
                    <li class="checkbox checkbox-primary">
                        <label class="checkbox-checked">
                            <input type="checkbox" name="checkMenu[]" value="{{ $submenu['vue_name'] }}"
                                   {{ (collect($menuCheck)->pluck('vue_name')->contains($submenu['vue_name']) ? 'checked' : '') }}
                                   class="checkNew checkMenuSub @if(!$subMenuCheck) checkMenuSub_{{ $submenu['parent'] }} @endif">

                            <span class="label-text">
                                <a href="javascript:void(0);">
                                    <i class="list-icon {{ $submenu['icon'] }}"></i> {{ $submenu['name'] }}
                                </a>
                            </span>
                        </label>
                    </li>
                @else
                    @include('partials.menu-assign-item', [ 'item' => $submenu, 'subMenuCheck' => false, 'menuCheck' => $checkMenu ])
                @endif
            @endforeach
        </ul>
    </li>
@endif