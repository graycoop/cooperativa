<div class="row">
    <div class="widget-holder col-md-12">
        <div class="widget-bg">
            <div class="widget-heading widget-heading-border">
                <h5 class="widget-title">Dashboard</h5>
                <div class="widget-actions">
                    <a href="javascript:void(0)"><i class="fa fa-cog"></i></a>
                </div>
            </div>
            <div class="widget-body">
                <p>You are logged in!</p>
            </div>
        </div>
    </div>
</div>
<script>
    vmFront.nameRoute = ucwords('{{ $titleModule }}')
</script>