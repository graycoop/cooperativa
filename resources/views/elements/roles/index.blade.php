<div class="row">
    <div class="widget-holder col-md-12">
        <div class="widget-bg">
            <div class="widget-heading widget-heading-border">
                <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
                <div class="widget-actions">
                    <a href="javascript:void(0)" onclick="responseModal('div.dialogGrayCoop','formRoles',{},'GET')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family animated bounceIn"><i class="list-icon fa fa-plus" aria-hidden="true"></i> Agregar Perfil</a>
                </div>
            </div>
            <div class="widget-body animated bounce">
                <table id="listRoles" class="table dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead class="bg-primary">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        loadDatatable()
    })
    function loadDatatable(){
        dataTables('listRoles', '/listRoles', {}, 'GET')
    }
    vmFront.nameRoute = ucwords('{{ $titleModule }}')
</script>