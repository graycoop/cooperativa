<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-info">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Asignar Usuarios [Rol {{ $nameRole }}]</h5>
    </div>
    <div class="modal-body">
        <div class="col-xs-12">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                </div>
                <input type="text" class="form-control" id="search_roles_user" placeholder="Ingrese el nombre o usuario a buscar">
            </div>
        </div>
        <form id="formRolesAssing">
            <div class="col-xs-12 mt-5">
                <table id="table_roles_user" class="table table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr class="thead-inverse bg-info">
                        <th class="text-center col-xs-2">
                            <input type="checkbox" class="checkGeneral" onclick="mark_all('.checkGeneral')">
                        </th>
                        <th class="col-xs-7">Nombre de Usuario</th>
                        <th class="col-xs-3">Username</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($UsersRole as $user)
                            @php $ifExistRole = (isset($user['UserRoles']) && $user['UserRoles']['role_id'] == $idRole ? true : false) @endphp
                            <tr class="text-dark @if($ifExistRole) table-success @else table-info @endif @if(!$ifExistRole) trNew @endif" id="tr_{{ $user['id'] }}">
                                <td class="col-xs-2 text-center">
                                    <input type="checkbox" name="checkUser[]" value="{{ $user['id'] }}" id="checkbox_{{ $user['id'] }}" class="@if(!$ifExistRole) checkNew @endif"
                                       @if($ifExistRole)
                                           @if($user['UserRoles']['role_id'] == $idRole) checked @endif
                                       @endif onclick="markCheck('{{ $user['id'] }}')">
                                </td>
                                <td class="col-xs-7">
                                    {{ $user['primer_nombre'].' '.$user['segundo_nombre'].' '.$user['primer_apellido'].' '.$user['segundo_apellido'] }}
                                </td>
                                <td class="col-xs-3">
                                    {{ $user['username'] }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="alert alert-danger formError d-none"></div>
            <input type="hidden" name="roleID" value="{{ $idRole }}">
            <div class="col-xs-12 mt-1">
                <div class="row text-center text-dark">
                    <div class="col-md-4">
                        <span><i class="badge table-success py-1 px-2" aria-hidden="true">&nbsp;</i> Asignado</span>
                    </div>
                    <div class="col-md-4">
                        <span><i class="badge table-info py-1 px-2" aria-hidden="true">&nbsp;</i> No Asignado</span>
                    </div>
                    <div class="col-md-4">
                        <span><i class="badge table-warning py-1 px-2" aria-hidden="true">&nbsp;</i> Seleccionado</span>
                    </div>
                </div>
            </div>
            <div class="text-center mr-b-30 mt-3">
                <button type="submit" class="btn btn-primary btnForm ripple"><i class="list-icon feather feather-save"></i> Guardar</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formRoles.js') !!}"></script>
<script>
    hideErrorForm('.formError')
    searchTable('#table_roles_user','#search_roles_user')
    clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')
</script>