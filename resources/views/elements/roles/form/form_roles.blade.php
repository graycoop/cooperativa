<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoop')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Rol</h5>
    </div>
    <div class="modal-body">
        <form id="formRole">
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" name="nameRole" class="form-control" placeholder="Ingresa el nombre del perfil" value="{{ $dataRole ? $dataRole[0]['name'] : '' }}">
            </div>
            <div class="form-group">
                <label>Descripción</label>
                <input type="text" name="descriptionRole" class="form-control" placeholder="Ingresa una breve descripción del perfil" value="{{ $dataRole ? $dataRole[0]['description'] : '' }}">
            </div>
            <div class="alert alert-danger formError d-none"></div>
            @if($updateForm === false)
                <div class="alert alert-icon alert-info">
                    <i class="list-icon feather feather-info"></i> <strong>Recuerda que al crear un nuevo perfil, debes asignarle a que Menús podra acceder</strong>
                </div>
            @endif
            <input type="hidden" name="roleID" value="{{ $dataRole ? $dataRole[0]['id'] : '' }}">
            <input type="hidden" name="statusID" value="{{ $dataRole ? $dataRole[0]['id_estado'] : '1' }}">
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoop')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formRoles.js') !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoop')
</script>