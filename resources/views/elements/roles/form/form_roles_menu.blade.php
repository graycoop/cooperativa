<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-info">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Actualizar Plantilla del Menú [Rol {{ $nameRole }}]</h5>
    </div>
    <div class="modal-body">
        <form id="formRoleMenu">
            <div class="container col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="sitemap-default sitemap">
                            <h5 class="box-title text-primary">Menú Del Sistema</h5>
                            <ul class="sitemap-list sitemap-list-icons list-unstyled">
                                <div class="checkbox checkbox-primary">
                                    <label class="checkbox-checked">
                                        <input type="checkbox" class="checkGeneral" onclick="mark_all_menu('.checkGeneral', 'checkNew')"> <span class="label-text">Seleccionar todos los menús</span>
                                    </label>
                                </div>
                                @foreach ($menus as $key => $item)
                                    @if ($item['parent'] != 0)
                                        @break
                                    @endif
                                    @include('partials.menu-assign-item', ['item' => $item, 'subMenuCheck' => true, 'menuCheck' => $checkMenu])
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="alert alert-icon alert-info border-info" role="alert">
                            <i class="list-icon feather feather-info"></i> <strong>Es importante tener en cuenta :</strong>
                            <ul class="mr-t-10">
                                <li>Este menú es una plantilla solamente para el perfil, para cuando se asigne a un usuario este se replique.</li>
                                <li>Cuando se realize un cambio, este no afecta a los usuarios ya que para ello se tiene un modulo adicional.</li>
                            </ul>
                        </div>
                        <div class="alert alert-danger formError d-none"></div>
                        <input type="hidden" name="roleID" value="{{ $idRole }}">
                        <div class="text-center mr-b-30">
                            <button type="submit" class="btn btn-primary btnForm ripple"><i class='list-icon feather feather-refresh-cw' aria-hidden='true'></i> Actualizar</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formRoles.js') !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')
</script>