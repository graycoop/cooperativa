<div class="widget-holder widget-sm widget-border-radius col-md-4">
    <div class="widget-bg">
        <div class="widget-heading bg-primary">
            <span class="widget-title my-0 color-white fs-12 fw-600">Total de Usuarios</span>
            <i class="widget-heading-icon feather feather-user"></i>
        </div>
        <div class="widget-body">
            <div class="counter-w-info">
                <div class="counter-title color-color-scheme">
                    <span class="counter" v-html="utilitiesUser.totalUsers"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="widget-holder widget-sm widget-border-radius col-md-4">
    <div class="widget-bg">
        <div class="widget-heading bg-primary">
            <span class="widget-title my-0 color-white fs-12 fw-600">Usuarios creados [ {{ ucwords(\Jenssegers\Date\Date::now()->format('F')) }} ]</span>
            <i class="widget-heading-icon feather feather-user"></i>
        </div>
        <div class="widget-body">
            <div class="counter-w-info">
                <div class="counter-title color-color-scheme">
                    <span class="counter" v-html="utilitiesUser.totalUsersMonth"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="widget-holder widget-sm widget-border-radius col-md-4">
    <div class="widget-bg">
        <div class="widget-heading bg-primary">
            <span class="widget-title my-0 color-white fs-12 fw-600">Ultimo Usuario Creado</span>
            <i class="widget-heading-icon feather feather-user"></i>
        </div>
        <div class="widget-body">
            <div class="counter-w-info">
                <div class="counter-title color-color-scheme">
                    <span class="counter" v-html="utilitiesUser.lastUser"></span>
                </div>
            </div>
        </div>
    </div>
</div>