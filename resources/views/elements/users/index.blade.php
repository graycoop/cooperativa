<div id="userUtilities" class="row">
    @include('elements.users.widget.index')
    <div class="widget-holder col-md-12">
        <div class="widget-bg">
            <div class="widget-heading widget-heading-border">
                <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
                <div class="widget-actions">
                    <a href="javascript:void(0)" onclick="responseModal('div.dialogGrayCoopExtraLarge','formUsers',{},'GET')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family animated bounceIn">
                        <i class="list-icon fa fa-plus" aria-hidden="true"></i> Agregar Usuario
                    </a>
                </div>
            </div>
            <div class="widget-body animated bounce">
                <table id="listUsers" class="table dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead class="bg-primary">
                    <tr>
                        <th>#</th>
                        <th>Avatar</th>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/userUtilitiesVue.js?version='.date('YmdHis'))!!}"></script>
<script>
    $(document).ready(function(){
        loadDatatable()
    })
    function loadDatatable(){
        dataTables('listUsers', '/listUsers', {}, 'GET')
    }
    vmFront.nameRoute = ucwords('{{ $titleModule }}')
</script>