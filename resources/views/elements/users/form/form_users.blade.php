<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Usuario</h5>
    </div>
    <div class="modal-body">
        <form id="formUser">
            <div class="container col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Primer Nombre</label>
                            <input type="text" name="firstName" class="form-control" placeholder="Ingrese su primer nombre" value="{{ $dataUser ? $dataUser[0]['primer_nombre'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label>Segundo Nombre</label>
                            <input type="text" name="secondName" class="form-control" placeholder="Ingrese su segundo nombre" value="{{ $dataUser ? $dataUser[0]['segundo_nombre'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label>Primer Apellido</label>
                            <input type="text" name="lastName" class="form-control" placeholder="Ingrese su primer apellido" value="{{ $dataUser ? $dataUser[0]['primer_apellido'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                            <input type="text" name="lastSecondName" class="form-control" placeholder="Ingrese su segundo apellido" value="{{ $dataUser ? $dataUser[0]['segundo_apellido'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label>Fecha de Nacimiento</label>
                            <input type="text" name="birthDay" class="form-control datepicker" value="{{ $dataUser ? $dataUser[0]['fecha_nacimiento'] : \Carbon\Carbon::now()->format('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tipo de Documento</label>
                            <select name="typeIdentity" class="form-control selectpicker show-tick">
                                <option value="">Seleccione un tipo de documento</option>
                                @foreach($options['typeIdentity'] as $key => $value)
                                    <option value="{{ $value['ID_TIPODOCIDENTIDAD'] }}" @if($dataUser) {{ $dataUser[0]['id_tipo_identidad'] === $value['ID_TIPODOCIDENTIDAD'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['DESC_CORTO'])) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Número de Documento</label>
                            <input type="text" name="numIdentity" class="form-control" placeholder="Ingrese su número de documento" value="{{ $dataUser ? $dataUser[0]['numero_identidad'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label>Correo Electronico</label>
                            <input type="text" name="email" class="form-control" placeholder="Ingrese su correo electronico" value="{{ $dataUser ? $dataUser[0]['email'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label class="text-danger">Usuario</label>
                            <input type="text" name="username" class="form-control" placeholder="Ingrese su usuario" value="{{ $dataUser ? $dataUser[0]['username'] : '' }}">
                        </div>
                        <div class="form-group">
                            <label class="text-danger">Contraseña</label>
                            <input id="inputRead" type="password" name="password" class="form-control" placeholder="Ingrese su contraseña" value="{{ $dataUser ? $dataUser[0]['password'] : '' }}" {{ $updateForm === true ? "readonly" : "" }}>
                            @if($updateForm === true)
                                <input id="inputRecover" type="text" name="passwordValidate" value="{{ $dataUser[0]['password'] }}" hidden>
                            @endif
                            @if($updateForm === true)
                                <div class="checkbox checkbox-primary">
                                    <label class="checkbox-checked">
                                        <input id="checkRead" name="changePassword" type="checkbox" onclick="markReadOnly('#checkRead','#inputRead','#inputRecover')"> <span class="label-text">¿Deseas cambiar la contraseña?</span>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Perfil del Usuario</label>
                            <select name="roleUser" class="form-control selectpicker show-tick">
                                <option value="">Seleccione un perfil</option>
                                @foreach($options['roles'] as $key => $value)
                                    <option value="{{ $value['id'] }}" @if($dataUser) {{ $dataUser[0]['roles'][0]['id'] === $value['id'] ? 'selected' : '' }} @endif>{{ ucwords($value['name']) }}</option>
                                @endforeach
                            </select>
                            <div class="checkbox checkbox-primary">
                                <label class="checkbox-checked">
                                    <input name="checkRolMenu" type="checkbox" {{ $updateForm === true ? '' : 'checked' }}> <span class="label-text">¿Deseás pasar el menú del Perfil?</span>
                                </label>
                            </div>
                        </div>
                        <div class="alert alert-icon alert-info border-info" role="alert">
                            <i class="list-icon feather feather-info"></i> <strong>Es importante tener en cuenta :</strong>
                            <ul class="mr-t-10">
                                <li>Que al elegir si se desea pasar el menu por defecto, este actualizara al usuario con el menu del perfil, si no se elige se tendra que seleccionar el menu de manera manual.</li>
                                <li>Para actualizar la contraseña se hara de forma independiente en otra opción por seguridad.</li>
                            </ul>
                        </div>
                        <div class="alert alert-danger formError d-none"></div>
                        <input type="hidden" name="userID" value="{{ $dataUser ? $dataUser[0]['id'] : '' }}">
                        <input type="hidden" name="statusID" value="{{ $dataUser ? $dataUser[0]['id_estado'] : '1' }}">
                        <div class="text-center mr-b-30">
                            <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formUsers.js') !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')
    initDatePicker('.datepicker', {
        autoclose: true,
        format: 'yyyy-mm-dd',
        language: '{{ config('app.locale') }}',
        showOnFocus: true
    })
    initSelectPicker('.selectpicker')
</script>