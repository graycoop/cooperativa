<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoop')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Cambiar Estado [{{ $dataUser[0]['username'] }}]</h5>
    </div>
    <div class="modal-body">
        <form id="formStatusUser">
            <div class="form-group">
                <label>Recuerda que cambiaras el estado <span class="badge px-3 heading-font-family {{ $dataUser[0]['status']['COLOR_ESTADO'] }}">{{ $dataUser[0]['status']['TIPO_ESTADO'] }}</span> a :</label>
                <select name="statusUser" class="form-control selectpicker show-tick">
                    <option value="">Seleccione un estado</option>
                    @foreach($options as $key => $value)
                        <option data-content="<span class='badge px-3 heading-font-family {{ $value['COLOR_ESTADO'] }}'>{{ mb_strtoupper($value['TIPO_ESTADO']) }}</span>" value="{{ $value['ID_TIPOESTADO'] }}">{{ $value['TIPO_ESTADO'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="alert alert-danger formError d-none"></div>
            <input type="hidden" name="userID" value="{{ $dataUser[0]['id'] }}">
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple"><i class='list-icon feather feather-refresh-cw' aria-hidden='true'></i> Actualizar</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formUsers.js') !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoop')
    initSelectPicker('.selectpicker')
</script>