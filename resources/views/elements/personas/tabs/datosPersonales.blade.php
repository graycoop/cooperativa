<div id="formVueDatosPersonales">
    <form id="formDatosPersonales">
        <div class="container col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tipo de Persona</label>
                        <select name="tipoPersona" class="form-control form-control-sm selectpicker show-tick" @change="changeTipoPersona">
                            @foreach($selectOptions['tipoPersoneria'] as $key => $value)
                                <option value="{{ $value['ID_TIPOPERSONERIA'] }}"
                                    @if($dataPersona)
                                        {{ $dataPersona[0]['ID_TIPOPERSONERIA'] === $value['ID_TIPOPERSONERIA'] ? 'selected' : '' }}
                                    @endif>{{ ucwords(\Illuminate\Support\Str::lower($value['TIPO_PERSONERIA'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tipo de Documento</label>
                        <select name="tipoDocIdentidad" class="form-control form-control-sm selectpicker show-tick">
                            <option value="">Seleccione el tipo de documento</option>
                            @foreach($selectOptions['tipoDocumento'] as $key => $value)
                                <option value="{{ $value['ID_TIPODOCIDENTIDAD'] }}"
                                @if($dataPersona)
                                    {{ $dataPersona[0]['tipodocidentidad']['ID_TIPODOCIDENTIDAD'] === $value['ID_TIPODOCIDENTIDAD'] ? 'selected' : '' }}
                                @endif>{{ $value['DESC_CORTO'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nro de Documento</label>
                        <input type="text" name="numeroDocumento" class="form-control form-control-md" placeholder="Ingrese el numero de documento" value="{{ $dataPersona ? $dataPersona[0]['NRO_DOCUMENTO'] : '' }}">
                    </div>
                    <input type="hidden" name="personaID" value="{{ $dataPersona ? $dataPersona[0]['ID_PERSONA'] : '' }}">
                </div>
            </div>
            <div id="loadFormDatosPersonales"></div>
        </div>
    </form>
</div>
<script src="{!! asset('js/vue/form/formDatosPersonalesVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    $(document).ready(function() {
        initSelectedPickerFormDatosPersonales()
        initDatePicketFormPersona()
        loadDatosPersonales()
        @if(!$updateForm)
            vmFormDatosPersonales.loadFormTipoPersona('1')
        @else
            vmFormDatosPersonales.loadFormTipoPersona('{{ $dataPersona[0]['ID_TIPOPERSONERIA'] }}','{{ $dataPersona[0]['ID_PERSONA'] }}')
        @endif
    })
    function loadDatosPersonales(){
        vmFront.nameRoute = ucwords('Datos Personales')
    }
    function initSelectedPickerFormDatosPersonales(){
        initSelectPicker('.selectpicker', {
            style: "btn-default btn-sm"
        })
    }
    function initDatePicketFormPersona(){
        initDateRangePicker('.datepicker', {
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        })
    }
</script>