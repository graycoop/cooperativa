<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-body">
            <div class="tabs">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link tabDataTableCentroLaboral" href="#dataTableCentroLaboral" data-toggle="tab" aria-expanded="true" onclick="loadCentroLaboralDatatable()">
                            Lista Centro Laboral
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tabDataTableOrganizacion" href="#dataTableOrganizacion" data-toggle="tab" aria-expanded="true" onclick="loadOrganizacionDatatable()">
                            Lista Organizaciones
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="dataTableCentroLaboral" aria-expanded="false">
                        @include('elements.personas.tabs.utils.listCentroLaboral')
                    </div>
                    <div class="tab-pane" id="dataTableOrganizacion" aria-expanded="false">
                        @include('elements.personas.tabs.utils.listOrganizaciones')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.tabDataTableCentroLaboral').click()
    })
</script>