<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title">&nbsp;</h5>
            <div class="widget-actions">
                <a href="javascript:void(0)" onclick="responseModal('div.dialogGrayCoopLarge','formDireccion', { idPersona: '{{ $dataPersona[0]['ID_PERSONA'] }}' }, 'GET')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="list-icon fa fa-plus" aria-hidden="true"></i> Agregar Direccion
                </a>
            </div>
        </div>
        <div class="widget-body">
            <table id="listDireccion" class="table dt-responsive nowrap" cellspacing="0" width="100%">
                <thead class="bg-primary">
                <tr>
                    <th>#</th>
                    <th>Direccion</th>
                    <th>Tipo</th>
                    <th>Acciones</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script>
    function loadDireccionDatatable(){
        dataTables('listDireccion', '/listDireccion', { idPersona: '{{ $dataPersona[0]['ID_PERSONA'] }}' }, 'GET')
        vmFront.nameRoute = ucwords('Direcciones')
    }
</script>