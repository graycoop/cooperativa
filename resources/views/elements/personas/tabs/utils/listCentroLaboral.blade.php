<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title">&nbsp;</h5>
            <div class="widget-actions">
                <a href="javascript:void(0)" onclick="responseModal('div.dialogGrayCoopExtraLarge','formCentroLaboral', { idPersona: '{{ $dataPersona[0]['ID_PERSONA'] }}' }, 'GET')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family">
                    <i class="list-icon fa fa-plus" aria-hidden="true"></i> Agregar Centro Laboral
                </a>
            </div>
        </div>
        <div class="widget-body">
            <table id="listCentroLaboral" class="table dt-responsive nowrap" cellspacing="0" width="100%">
                <thead class="bg-primary">
                <tr>
                    <th>#</th>
                    <th>Organizacion</th>
                    <th>Categoria Laboral</th>
                    <th>Acciones</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script>
    function loadCentroLaboralDatatable(){
        dataTables('listCentroLaboral', '/listCentroLaboral', { idPersona: '{{ $dataPersona[0]['ID_PERSONA'] }}' }, 'GET')
        vmFront.nameRoute = ucwords('Lista de Centro Laboral')
    }
</script>