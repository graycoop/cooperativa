<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
            <div class="widget-actions">
                <a href="javascript:void(0)" class="badge bg-primary px-3 heading-font-family animated bounceIn" onclick="vmFront.loadOptionMenu('formPersonas')">
                    <i class="list-icon fa fa-plus" aria-hidden="true"></i> Agregar Persona
                </a>
            </div>
        </div>
        <div class="widget-body">
            @include('elements.personas.form.formSearchPersona')
        </div>
    </div>
</div>
<script>
    vmFront.nameRoute = ucwords('{{ $titleModule }}')
</script>