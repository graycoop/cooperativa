<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm ? 'Editar' : 'Agregar' }} Organizacion</h5>
    </div>
    <div class="modal-body">
        <form id="formOrganizacion">
            <div id="formVueDireccion" class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Razon Social</label>
                        <input type="text" name="razonSocial" class="form-control form-control-md" placeholder="Ingresa la razon social" value="{{ $idOrganizacion ? $dataOrganizacion[0]['RAZON_SOCIAL'] : '' }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Nº RUC</label>
                        <input type="text" name="numeroRUC" class="form-control form-control-md" placeholder="Ingresa el numero ruc" value="{{ $idOrganizacion ? $dataOrganizacion[0]['RUC'] : '' }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Telefono</label>
                        <input type="text" name="telefonoOrganizacion" class="form-control form-control-md" placeholder="Ingresa el numero" value="{{ $idOrganizacion ? $dataOrganizacion[0]['TELEFONO'] : '' }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Contacto</label>
                        <input type="text" name="emailContacto" class="form-control form-control-md" placeholder="Ingresa el contacto (email)" value="{{ $idOrganizacion ? $dataOrganizacion[0]['CONTACTO'] : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Departamento</label>
                        <select name="codDepartamento" class="form-control selectpicker show-tick" data-live-search="true" data-size="10" onchange="onChangeDepartamento(this.value)">
                            <option value="">Seleccione un departamento</option>
                            @foreach($selectDepartamento as $key => $value)
                                <option value="{{ $value['DPTO'] }}" @if($idOrganizacion) {{ $value['DPTO'] == $dataOrganizacion[0]['COD_DPTO'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['UBIGEO'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="selectProvincia" class="form-group">
                        <label>Provincia</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Esperando Departamento' selected></option>
                        </select>
                    </div>
                    <div id="selectLoadingProvincia" class="form-group d-none">
                        <label>Provincia</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Cargando Provincia' selected></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="selectDistrito" class="form-group">
                        <label>Distrito</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Esperando Provincia' selected></option>
                        </select>
                    </div>
                    <div id="selectLoadingDistrito" class="form-group d-none">
                        <label>Distrito</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Cargando Distrito' selected></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Direccion</label>
                        <input type="text" name="direccion" class="form-control form-control-md" placeholder="Ingresa la direccion" value="{{ $idOrganizacion ? $dataOrganizacion[0]['DIRECCION'] : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>FLG Convenio</label>
                        <select name="flgConvenio" class="form-control selectpicker show-tick" data-live-search="true">
                            <option value="">Seleccione la opcion</option>
                            <option value="1" @if($idOrganizacion) {{ $dataOrganizacion[0]['FLG_CONVENIO'] == 1 ? 'selected' : '' }} @endif>{{ ucfirst('SI') }}</option>
                            <option value="0" @if($idOrganizacion) {{ $dataOrganizacion[0]['FLG_CONVENIO'] == 0 ? 'selected' : '' }} @endif>{{ ucfirst('NO') }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cuenta CTB</label>
                        <input type="text" name="cuentaCTB" class="form-control form-control-md" placeholder="Ingresa la cuenta CTB" value="{{ $idOrganizacion ? $dataOrganizacion[0]['CUENTA_CTB'] : '' }}">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <input type="hidden" name="{{ $idPersona ? 'idPersona' : 'idCentroLaboral' }}" value="{{ $idPersona ? $idPersona : $idOrganizacion }}">
                <div class="text-center mr-b-30">
                    <button type="submit" class="btn btn-primary btnForm ripple">{!! $idOrganizacion ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger formError d-none"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/vue/form/formDireccionVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    @if($idOrganizacion)
        vmFormDireccion.changeDepartamento('{{ $dataOrganizacion[0]['COD_DPTO'] }}', '{{ $dataOrganizacion[0]['COD_PROV'] }}')
        vmFormDireccion.changeProvincia('{{ $dataOrganizacion[0]['COD_PROV'] }}', '{{ $dataOrganizacion[0]['COD_DIST'] }}')
    @endif

    function onChangeDepartamento(idProvincia){
        vmFormDireccion.changeDepartamento(idProvincia)
    }
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoopLarge')
    initSelectPicker('.selectpicker')
</script>