<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoop')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm ? 'Editar' : 'Agregar' }} Email</h5>
    </div>
    <div class="modal-body">
        <form id="formEmails">
            <div class="form-group">
                <label>Tipo de Email</label>
                <select name="tipoEmail" class="form-control selectpicker show-tick">
                    <option value="">Seleccione un tipo de email</option>
                    @foreach($selectOptions['tipoEmail'] as $key => $value)
                        <option value="{{ $value['ID_TIPOEMAIL'] }}" @if($idEmail) {{ $dataEmail[0]['ID_TIPOEMAIL'] === $value['ID_TIPOEMAIL'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['TIPO_EMAIL'])) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control form-control-md" placeholder="Ingresa un Email" value="{{ $idEmail ? $dataEmail[0]['EMAIL'] : '' }}">
            </div>
            <div class="alert alert-danger formError d-none"></div>
            <input type="hidden" name="{{ $idPersona ? 'idPersona' : 'idEmail' }}" value="{{ $idPersona ? $idPersona : $idEmail }}">
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple">{!! $idEmail ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoop')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoop')
    initSelectPicker('.selectpicker')
</script>