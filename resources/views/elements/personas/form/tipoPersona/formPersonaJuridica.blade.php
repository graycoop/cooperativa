<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Razon Social</label>
            <input type="text" name="razonSocial" class="form-control form-control-md" placeholder="Ingrese la razon social" value="{{ $dataPersona ? $dataPersona[0]['RAZON_SOCIAL'] : '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Fecha de Registro</label>
            <input type="text" name="fecNacimiento" class="form-control form-control-md datepicker" placeholder="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" value="{{ $dataPersona ? $dataPersona[0]['FECHA_NACIMIENTO'] : '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Nacionalidad</label>
            <select name="tipoNacionalidad" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="8">
                <option value="" selected disabled>Seleccione el tipo de nacionalidad</option>
                @foreach($selectOptions['tipoNacionalidad'] as $key => $value)
                    <option value="{{ $value['ID_TIPONACIONALIDAD'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPONACIONALIDAD'] === $value['ID_TIPONACIONALIDAD'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_NACIONALIDAD'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Residencia</label>
            <select name="tipoResidencia" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="8">
                <option value="" selected disabled>Seleccione el tipo de residencia</option>
                @foreach($selectOptions['tipoNacionalidad'] as $key => $value)
                    <option value="{{ $value['ID_TIPONACIONALIDAD'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPONACIONALIDAD'] === $value['ID_TIPONACIONALIDAD'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_NACIONALIDAD'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Actividad Economica</label>
            <select name="tipoActividadEconomica" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="8">
                <option value="" selected disabled>Seleccione el tipo de actividad economica</option>
                @foreach($selectOptions['tipoActividadEconomica'] as $key => $value)
                    <option value="{{ $value['ID_TIPOACTIVIDADECONOMICA'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOACTIVIDADECONOMICA'] === $value['ID_TIPOACTIVIDADECONOMICA'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_ACTIVIDADECONOMICA'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Persona Juridica</label>
            <select name="tipoPersonaJuridica" class="form-control form-control-sm selectpicker show-tick">
                <option value="" selected disabled>Seleccione el tipo de persona juridica</option>
                @foreach($selectOptions['tipoPersonaJuridica'] as $key => $value)
                    <option value="{{ $value['ID_TIPOPERSONAJURIDICA'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOPERSONAJURIDICA'] === $value['ID_TIPOPERSONAJURIDICA'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_PERSONAJURIDICA'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tamaño de Persona Juridica</label>
            <select name="tipoTamañoPersonaJuridica" class="form-control form-control-sm selectpicker show-tick">
                <option value="" selected disabled>Seleccione el tamaño de la persona juridica</option>
                @foreach($selectOptions['tamañoPersonaJuridica'] as $key => $value)
                    <option value="{{ $value['ID_TAMANOPERSONAJURIDICA'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TAMANOPERSONAJURIDICA'] === $value['ID_TAMANOPERSONAJURIDICA'] ? 'selected' : '' }}
                    @endif>{{ $value['TAMANO_PERSONAJURIDICA'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>&nbsp;</label>
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger formError d-none"></div>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script>
    $(document).ready(function() {
        initSelectedPickerFormDatosPersonales()
        initDatePicketFormPersona()
    })
    hideErrorForm('.formError')
</script>