<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Apellido Paterno</label>
            <input type="text" name="apellidoPaterno" class="form-control form-control-md" placeholder="Ingrese el apellido paterno" value="{{ $dataPersona ? $dataPersona[0]['APELLIDO_PAT'] : '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Apellido Materno</label>
            <input type="text" name="apellidoMaterno" class="form-control form-control-md" placeholder="Ingrese el apellido materno" value="{{ $dataPersona ? $dataPersona[0]['APELLIDO_MAT'] : '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Nombres</label>
            <input type="text" name="nombres" class="form-control form-control-md" placeholder="Ingrese los nombres" value="{{ $dataPersona ? $dataPersona[0]['NOMBRES'] : '' }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Sexo</label>
            <select name="tipoSexo" class="form-control form-control-sm selectpicker show-tick">
                <option value="">Seleccione el sexo</option>
                @foreach($selectOptions['tipoSexo'] as $key => $value)
                    <option value="{{ $value['ID_TIPOSEXO'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOSEXO'] === $value['ID_TIPOSEXO'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_SEXO'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Estado Civil</label>
            <select name="tipoEstadoCivil" class="form-control form-control-sm selectpicker show-tick">
                <option value="">Seleccione el estado civil</option>
                @foreach($selectOptions['tipoEstadoCivil'] as $key => $value)
                    <option value="{{ $value['ID_TIPOESTADOCIVIL'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOESTADOCIVIL'] === $value['ID_TIPOESTADOCIVIL'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_ESTADOCIVIL'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Fecha de Nacimiento</label>
            <input type="text" name="fecNacimiento" class="form-control form-control-md datepicker" placeholder="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" value="{{ $dataPersona ? $dataPersona[0]['FECHA_NACIMIENTO'] : '' }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Lugar de Nacimiento</label>
            <input type="text" name="lugarNacimiento" class="form-control form-control-md" placeholder="Ingrese el lugar de nacimiento" value="{{ $dataPersona ? $dataPersona[0]['LUGAR_NACIMIENTO'] : '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Nacionalidad</label>
            <select name="tipoNacionalidad" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="15">
                <option value="">Seleccione el tipo de nacionalidad</option>
                @foreach($selectOptions['tipoNacionalidad'] as $key => $value)
                    <option value="{{ $value['ID_TIPONACIONALIDAD'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPONACIONALIDAD'] === $value['ID_TIPONACIONALIDAD'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_NACIONALIDAD'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Residencia</label>
            <select name="tipoResidencia" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="15">
                <option value="">Seleccione el tipo de residencia</option>
                @foreach($selectOptions['tipoNacionalidad'] as $key => $value)
                    <option value="{{ $value['ID_TIPONACIONALIDAD'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPONACIONALIDAD'] === $value['ID_TIPONACIONALIDAD'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_NACIONALIDAD'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Actividad Economica</label>
            <select name="tipoActividadEconomica" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="15">
                <option value="">Seleccione el tipo de actividad economica</option>
                @foreach($selectOptions['tipoActividadEconomica'] as $key => $value)
                    <option value="{{ $value['ID_TIPOACTIVIDADECONOMICA'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOACTIVIDADECONOMICA'] === $value['ID_TIPOACTIVIDADECONOMICA'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_ACTIVIDADECONOMICA'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Nivel Educativo</label>
            <select name="tipoNivelEducativo" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="15">
                <option value="" selected disabled>Seleccione el tipo de nivel educativo</option>
                @foreach($selectOptions['tipoNivelEducativo'] as $key => $value)
                    <option value="{{ $value['ID_TIPONIVELEDUCATIVO'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPONIVELEDUCATIVO'] === $value['ID_TIPONIVELEDUCATIVO'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_NIVELEDUCATIVO'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Ocupacion</label>
            <select name="tipoOcupacion" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="15">
                <option value="">Seleccione el tipo de ocupacion</option>
                @foreach($selectOptions['tipoOcupacion'] as $key => $value)
                    <option value="{{ $value['ID_TIPOOCUPACION'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOOCUPACION'] === $value['ID_TIPOOCUPACION'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_OCUPACION'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Profesion</label>
            <select name="tipoProfesion" class="form-control form-control-sm selectpicker show-tick" data-live-search="true" data-size="15">
                <option value="">Seleccione el tipo de profesion</option>
                @foreach($selectOptions['tipoProfesion'] as $key => $value)
                    <option value="{{ $value['ID_TIPOPROFESION'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOPROFESION'] === $value['ID_TIPOPROFESION'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_PROFESION'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de Modalidad Laboral</label>
            <select name="tipoModalidadLaboral" class="form-control form-control-sm selectpicker show-tick">
                <option value="">Seleccione el tipo de modalidad laboral</option>
                @foreach($selectOptions['tipoModalidadLaboral'] as $key => $value)
                    <option value="{{ $value['ID_TIPOMODALIDADLABORAL'] }}"
                    @if($dataPersona)
                        {{ $dataPersona[0]['ID_TIPOMODALIDADLABORAL'] === $value['ID_TIPOMODALIDADLABORAL'] ? 'selected' : '' }}
                    @endif>{{ $value['TIPO_MODALIDADLABORAL'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>&nbsp;</label>
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger formError d-none"></div>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script>
    $(document).ready(function() {
        initSelectedPickerFormDatosPersonales()
        initDatePicketFormPersona()
    })
    hideErrorForm('.formError')
</script>