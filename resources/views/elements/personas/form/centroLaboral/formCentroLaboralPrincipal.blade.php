<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoop')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Cambiar Centro Laboral Principal</h5>
    </div>
    <div class="modal-body">
        <form id="formCentroLaboralPrincipal">
            <div class="form-group">
                <label>Recuerda que al actualizar, este centro laboral pasara a ser el principal. </label>
            </div>
            <div class="alert alert-danger formError d-none"></div>
            <input type="hidden" name="personaID" value="{{ $dataCentroLaboral[0]['ID_PERSONA'] }}">
            <input type="hidden" name="centroLaboralID" value="{{ $dataCentroLaboral[0]['ID_CENTROLABORAL'] }}">
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple"><i class='list-icon feather feather-refresh-cw' aria-hidden='true'></i> Actualizar</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoop')
    initSelectPicker('.selectpicker')
</script>