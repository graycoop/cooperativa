<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm ? 'Editar' : 'Agregar' }} Centro Laboral</h5>
    </div>
    <div class="modal-body">
        <form id="formCentroLaboral">
            <div id="formVueCentroLaboral" class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Organizacion</label>
                        <div id="selectOrganizacion">
                            <select name="idOrganizacion" class="form-control selectpicker show-tick" data-live-search="true">
                                <option value="">Seleccione una organizacion</option>
                                @foreach($selectOptions['organizacion'] as $key => $value)
                                    <option value="{{ $value['ID_ORGANIZACION'] }}" @if($idCentroLaboral) {{ $dataCentroLaboral[0]['ID_ORGANIZACION'] == $value['ID_ORGANIZACION'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['RAZON_SOCIAL'])) }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if(!$updateForm)
                            <input id="inputOrganizacion" type="text" name="nameOrganizacion" class="form-control form-control-md d-none" placeholder="Ingrese el nombre de la Organizacion" value="">
                            <div class="checkbox checkbox-primary">
                                <label class="checkbox-checked">
                                    <input id="checkOrganizacion" name="checkOrganizacion" type="checkbox" onclick="checkVisible('#checkOrganizacion','#selectOrganizacion','#inputOrganizacion')"> <span class="label-text">¿No encontraste la Org.?</span>
                                </label>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Categoria Laboral</label>
                        <select name="categoriaLaboral" class="form-control selectpicker show-tick" data-live-search="true">
                            <option value="">Seleccione una categoria laboral</option>
                            @foreach($selectOptions['categoriaLaboral'] as $key => $value)
                                <option value="{{ $value['ID_TIPOCATEGORIA'] }}" @if($idCentroLaboral) {{ $dataCentroLaboral[0]['ID_TIPOCATEGORIA'] == $value['ID_TIPOCATEGORIA'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['TIPO_CATEGORIALABORAL'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Estado Laboral</label>
                        <select name="estadoLaboral" class="form-control selectpicker show-tick" data-live-search="true">
                            <option value="">Seleccione un estado laboral</option>
                            @foreach($selectOptions['estadoLaboral'] as $key => $value)
                                <option value="{{ $value['ID_TIPOESTADOLABORAL'] }}" @if($idCentroLaboral) {{ $dataCentroLaboral[0]['ID_TIPOESTADOLABORAL'] == $value['ID_TIPOESTADOLABORAL'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['TIPO_ESTADOLABORAL'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Fecha de Ingreso</label>
                        <input type="text" name="fechaIngreso" class="form-control form-control-md datepicker" placeholder="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" value="{{ $idCentroLaboral ? $dataCentroLaboral[0]['FECHA_INGRESO'] : '' }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Area</label>
                        <input type="text" name="areaLaboral" class="form-control form-control-md" placeholder="Ingresa el area donde labora" value="{{ $idCentroLaboral ? $dataCentroLaboral[0]['AREA'] : '' }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" name="cargoLaboral" class="form-control form-control-md" placeholder="Ingresa el cargo asignado" value="{{ $idCentroLaboral ? $dataCentroLaboral[0]['CARGO'] : '' }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Codigo Laboral</label>
                        <input type="text" name="codigoLaboral" class="form-control form-control-md" placeholder="Ingresa el contacto (email)" value="{{ $idCentroLaboral ? $dataCentroLaboral[0]['CODIGO'] : '' }}">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <input type="hidden" name="{{ $idPersona ? 'idPersona' : 'idCentroLaboral' }}" value="{{ $idPersona ? $idPersona : $idCentroLaboral }}">
                <div class="text-center mr-b-30">
                    <button type="submit" class="btn btn-primary btnForm ripple">{!! $idCentroLaboral ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                    <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                    <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger formError d-none"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoopExtraLarge')
    initSelectPicker('.selectpicker')
    initDateRangePicker('.datepicker', {
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    })
</script>