<form id="formSearchPersona" class="mr-b-30">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="list-icon fa fa-user-o"></i></div>
                    <input type="text" name="apellidoPaterno" class="form-control" placeholder="Apellido Paterno">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="list-icon fa fa-user-o"></i></div>
                    <input type="text" name="apellidoMaterno" class="form-control" placeholder="Apellido Materno">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="list-icon fa fa-user"></i></div>
                    <input type="text" name="nombresPersona" class="form-control" placeholder="Nombres">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="list-icon fa fa-bank"></i></div>
                    <input type="text" name="razonSocial" class="form-control" placeholder="Razon Social">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="list-icon fa fa-address-card"></i></div>
                    <input type="text" name="numeroDocumento" class="form-control" placeholder="Nº Documento / Nº Ruc">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="list-icon fa fa-code"></i></div>
                    <input type="text" name="codigoPersona" class="form-control" placeholder="Codigo">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="list-icon fa fa-archive"></i></div>
                    <input type="text" name="idPersona" class="form-control" placeholder="ID Persona">
                </div>
            </div>
        </div>
        <div class="col-md-3 text-center">
            <button type="submit" class="btn btn-primary btnForm ripple"><i class="list-icon fa fa-search"></i> Buscar</button>
        </div>
        <div class="col-md-12">
            <div class="alert alert-danger formError d-none"></div>
        </div>
    </div>
</form>
<div class="mr-b-3">
    <div id="containerLoading" class="d-none">
        <div class="cssload-loader"></div>
    </div>
    <div id="containerTablePersonas" class="d-none">
        <table id="listPersonas" class="table responsive nowrap" cellspacing="0" width="100%">
            <thead class="bg-primary">
            <tr>
                <th>ID Persona</th>
                <th>Nombres</th>
                <th>Nro Documento</th>
                <th>Fec Nacimiento</th>
                <th>Genero</th>
                <th>Acciones</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    $('#listPersonas').on('init.dt', function () {
        setTimeout(function(){
            $('#containerLoading').addClass('d-none')
            $('#containerTablePersonas').removeClass('d-none')
        },500)
    })
</script>