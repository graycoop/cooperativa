<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
            <div class="widget-actions"></div>
        </div>
        <div class="widget-body">
            <div class="tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item" aria-expanded="true">
                        <a class="nav-link active" href="#datosPersonales" data-toggle="tab" aria-expanded="false" onclick="loadDatosPersonales()">
                            <i class="list-icon feather feather-briefcase"></i> Datos Personales
                        </a>
                    </li>
                    <li class="nav-item" aria-expanded="true">
                        <a class="nav-link {{ $updateForm ? '' : 'disabled' }}" href="#Direcciones" data-toggle="tab" aria-expanded="true" onclick="loadDireccionDatatable()">
                            <i class="list-icon feather feather-navigation"></i> Direcciones
                        </a>
                    </li>
                    <li class="nav-item" aria-expanded="true">
                        <a class="nav-link {{ $updateForm ? '' : 'disabled' }}" href="#Telefonos" data-toggle="tab" aria-expanded="false" onclick="loadTelefonoDatatable()">
                            <i class="list-icon feather feather-phone"></i> Telefonos
                        </a>
                    </li>
                    <li class="nav-item" aria-expanded="true">
                        <a class="nav-link {{ $updateForm ? '' : 'disabled' }}" href="#Emails" data-toggle="tab" aria-expanded="true" onclick="loadEmailDatatable()">
                            <i class="list-icon feather feather-at-sign"></i> Emails
                        </a>
                    </li>
                    <li class="nav-item" aria-expanded="true">
                        <a class="nav-link {{ $updateForm ? '' : 'disabled' }}" href="#centroLaboral" data-toggle="tab" aria-expanded="true">
                            <i class="list-icon feather feather-server"></i> Centro Laboral
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="datosPersonales" aria-expanded="false">
                        @include('elements.personas.tabs.datosPersonales')
                    </div>
                    <div class="tab-pane" id="Direcciones" aria-expanded="false">
                        @if($updateForm)
                            @include('elements.personas.tabs.direcciones')
                        @endif
                    </div>
                    <div class="tab-pane" id="Telefonos" aria-expanded="false">
                        @if($updateForm)
                            @include('elements.personas.tabs.telefonos')
                        @endif
                    </div>
                    <div class="tab-pane" id="Emails" aria-expanded="true">
                        @if($updateForm)
                            @include('elements.personas.tabs.emails')
                        @endif
                    </div>
                    <div class="tab-pane" id="centroLaboral" aria-expanded="true">
                        @if($updateForm)
                            @include('elements.personas.tabs.centroLaboral')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>