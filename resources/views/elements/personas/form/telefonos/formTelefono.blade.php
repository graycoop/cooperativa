<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm ? 'Editar' : 'Agregar' }} Telefono</h5>
    </div>
    <div class="modal-body">
        <form id="formTelefono">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tipo de Telefono</label>
                        <select name="tipoTelefono" class="form-control selectpicker show-tick">
                            <option value="">Seleccione un tipo de telefono</option>
                            @foreach($selectOptions['tipoTelefono'] as $key => $value)
                                <option value="{{ $value['ID_TIPOTELEFONO'] }}" @if($idTelefono) {{ $dataTelefono[0]['ID_TIPOTELEFONO'] === $value['ID_TIPOTELEFONO'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['TIPO_TELEFONO'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tipo de Operador</label>
                        <select name="tipoOperador" class="form-control selectpicker show-tick">
                            <option value="">Seleccione un tipo de telefono</option>
                            @foreach($selectOptions['tipoOperador'] as $key => $value)
                                <option value="{{ $value['ID_TIPOOPERADOR'] }}" @if($idTelefono) {{ $dataTelefono[0]['ID_TIPOOPERADOR'] === $value['ID_TIPOOPERADOR'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['TIPO_OPERADOR'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tipo de Discado</label>
                        <select name="tipoDiscado" class="form-control selectpicker show-tick" data-live-search="true" data-size="5">
                            <option value="">Seleccione un tipo de telefono</option>
                            @foreach($selectOptions['tipoDiscado'] as $key => $value)
                                <option value="{{ $value['ID_TIPODISCADO'] }}" @if($idTelefono) {{ $dataTelefono[0]['ID_TIPODISCADO'] === $value['ID_TIPODISCADO'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['TIPO_DISCADO'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Numero</label>
                        <input type="text" name="numero" class="form-control form-control-md" placeholder="Ingresa el numero de telefono" value="{{ $idTelefono ? $dataTelefono[0]['TELEFONO'] : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Anexo</label>
                        <input type="text" name="anexo" class="form-control form-control-md" placeholder="Ingresa un anexo" value="{{ $idTelefono ? $dataTelefono[0]['ANEXO'] : '' }}">
                    </div>
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <input type="hidden" name="{{ $idPersona ? 'idPersona' : 'idTelefono' }}" value="{{ $idPersona ? $idPersona : $idTelefono }}">
                        <div class="text-center mr-b-30">
                            <button type="submit" class="btn btn-primary btnForm ripple">{!! $idTelefono ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger formError d-none"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoopLarge')
    initSelectPicker('.selectpicker')
</script>