<div class="form-group">
    <label>Provincia</label>
    <select name="codProvincia" class="form-control selectpicker show-tick" data-live-search="true" data-size="10" onchange="onChangeProvincia(this.value)">
        <option value="">Seleccione una provincia</option>
        @foreach($selectProvincia as $key => $value)
            <option value="{{ $value['PROV'] }}" @if($selectedProvincia) {{ $value['PROV'] === $selectedProvincia ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['UBIGEO'])) }}</option>
        @endforeach
    </select>
</div>
<script>
    initSelectPicker('.selectpicker')
    function onChangeProvincia(idProvincia){
        vmFormDireccion.changeProvincia(idProvincia)
    }
</script>