<div class="form-group">
    <label>Distrito</label>
    <select name="codDistrito" class="form-control selectpicker show-tick" data-live-search="true" data-size="10">
        <option value="">Seleccione un distrito</option>
        @foreach($selectDistrito as $key => $value)
            <option value="{{ $value['DIST'] }}" @if($selectedDistrito) {{ $value['DIST'] == $selectedDistrito ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['UBIGEO'])) }}</option>
        @endforeach
    </select>
</div>
<script>
    initSelectPicker('.selectpicker')
</script>