<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm ? 'Editar' : 'Agregar' }} Direccion</h5>
    </div>
    <div class="modal-body">
        <form id="formDireccion">
            <div id="formVueDireccion" class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tipo de Direccion</label>
                        <select name="tipoDireccion" class="form-control selectpicker show-tick">
                            <option value="">Seleccione un tipo de direccion</option>
                            @foreach($selectOptions['tipoDireccion'] as $key => $value)
                                <option value="{{ $value['ID_TIPODIRECCION'] }}" @if($idDireccion) {{ $dataDireccion[0]['ID_TIPODIRECCION'] == $value['ID_TIPODIRECCION'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['TIPO_DIRECCION'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Departamento</label>
                        <select name="codDepartamento" class="form-control selectpicker show-tick" data-live-search="true" data-size="10" onchange="onChangeDepartamento(this.value)">
                            <option value="">Seleccione un departamento</option>
                            @foreach($selectDepartamento as $key => $value)
                                <option value="{{ $value['DPTO'] }}" @if($idDireccion) {{ $value['DPTO'] == $dataDireccion[0]['COD_DPTO'] ? 'selected' : '' }} @endif>{{ ucfirst(strtolower($value['UBIGEO'])) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="selectProvincia" class="form-group">
                        <label>Provincia</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Esperando Departamento' selected></option>
                        </select>
                    </div>
                    <div id="selectLoadingProvincia" class="form-group d-none">
                        <label>Provincia</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Cargando Provincia' selected></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="selectDistrito" class="form-group">
                        <label>Distrito</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Esperando Provincia' selected></option>
                        </select>
                    </div>
                    <div id="selectLoadingDistrito" class="form-group d-none">
                        <label>Distrito</label>
                        <select class="form-control selectpicker show-tick disabled">
                            <option data-content='<i class="list-icon fa fa-spin fa-spinner"></i> Cargando Distrito' selected></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Direccion</label>
                        <input type="text" name="direccion" class="form-control form-control-md" placeholder="Ingresa una direccion" value="{{ $idDireccion ? $dataDireccion[0]['DIRECCION'] : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Referencia</label>
                        <input type="text" name="referencia" class="form-control form-control-md" placeholder="Ingresa una referencia" value="{{ $idDireccion ? $dataDireccion[0]['REFERENCIA'] : '' }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="hidden" name="{{ $idPersona ? 'idPersona' : 'idDireccion' }}" value="{{ $idPersona ? $idPersona : $idDireccion }}">
                    <div class="text-center mr-b-30">
                        <button type="submit" class="btn btn-primary btnForm ripple">{!! $idDireccion ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger formError d-none"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formPersonas.js?version='.date('YmdHis')) !!}"></script>
<script src="{!! asset('js/vue/form/formDireccionVue.js?version='.date('YmdHis')) !!}"></script>
<script>
    @if($idDireccion)
        vmFormDireccion.changeDepartamento('{{ $dataDireccion[0]['COD_DPTO'] }}', '{{ $dataDireccion[0]['COD_PROV'] }}')
        vmFormDireccion.changeProvincia('{{ $dataDireccion[0]['COD_PROV'] }}', '{{ $dataDireccion[0]['COD_DIST'] }}')
    @endif

    function onChangeDepartamento(idProvincia){
        vmFormDireccion.changeDepartamento(idProvincia)
    }
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoopLarge')
    initSelectPicker('.selectpicker')
</script>