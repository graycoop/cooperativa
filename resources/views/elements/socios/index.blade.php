<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
            <div class="widget-actions"></div>
        </div>
        <div class="widget-body">
            @include('elements.socios.form.formSearchSocio')
        </div>
    </div>
</div>
<script>
    vmFront.nameRoute = ucwords('{{ $titleModule }}')
</script>