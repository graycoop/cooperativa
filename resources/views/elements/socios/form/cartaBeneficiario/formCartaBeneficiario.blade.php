<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm ? 'Editar' : 'Agregar' }} Carta Beneficiario</h5>
    </div>
    <div class="modal-body">
        <form id="formCartaBeneficiario">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Fecha Carta</label>
                        <input type="text" name="fechaCarta" class="form-control form-control-md datepicker" placeholder="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" value="{{ $idCarta ? $dataCartaBeneficiario[0]['FECHA_CARTA'] : '' }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nº Carta</label>
                        <input type="text" name="numeroCarta" class="form-control form-control-md" placeholder="Ingrese el numero de la carta" value="{{ $idCarta ? $dataCartaBeneficiario[0]['NRO_CARTA'] : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>FPS</label>
                        <input type="text" name="fpsCarta" class="form-control form-control-md" placeholder="Ingrese el FPS" value="{{ $idCarta ? $dataCartaBeneficiario[0]['FPS'] : '' }}">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Observaciones</label>
                        <input type="text" name="observacionesCarta" class="form-control form-control-md" placeholder="Ingrese una observacion" value="{{ $idCarta ? $dataCartaBeneficiario[0]['OBSERVACIONES'] : '' }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <input type="hidden" name="{{ $idCarta ? 'idCarta' : 'idSocio' }}" value="{{ $idCarta ? $idCarta : $idSocio }}">
                        <div class="text-center mr-b-30">
                            <button type="submit" class="btn btn-primary btnForm ripple">{!! $idCarta ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                            <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                            <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogGrayCoopLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger formError d-none"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formSocios.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogGrayCoopLarge')
    initDateRangePicker('.datepicker', {
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    })
</script>