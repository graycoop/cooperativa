<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title"><i class="{{ $iconModule }}"></i> {{ $titleModule }}</h5>
            <div class="widget-actions"></div>
        </div>
        <div class="widget-body">
            <div class="tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item" aria-expanded="true">
                        <a class="nav-link active" href="#datosSocio" data-toggle="tab" aria-expanded="false">
                            <i class="list-icon feather feather-briefcase"></i> Datos del Socio
                        </a>
                    </li>
                    <li class="nav-item" aria-expanded="true">
                        <a class="nav-link {{ $updateForm ? '' : 'disabled' }}" href="#cartasSocio" data-toggle="tab" aria-expanded="true" onclick="loadCartasDatatable()">
                            <i class="list-icon feather feather-mail"></i> Carta Beneficiarios
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="datosSocio" aria-expanded="false">
                        @include('elements.socios.tabs.datosSocio')
                    </div>
                    <div class="tab-pane" id="cartasSocio" aria-expanded="false">
                        @if($updateForm)
                            @include('elements.socios.tabs.cartasSocio')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>