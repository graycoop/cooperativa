<form id="formDatosSocios">
    <div class="container col-md-12">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha de Ingreso</label>
                    <input type="text" name="fechaIngreso" class="form-control form-control-md datepicker" placeholder="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" value="{{ $dataSocio ? $dataSocio[0]['FECHA_INGRESO'] : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Tipo de Socio</label>
                    <select name="tipoSocio" class="form-control form-control-sm selectpicker show-tick" @change="changeTipoPersona">
                        @foreach($selectOptions['tipoSocio'] as $key => $value)
                            <option value="{{ $value['ID_TIPOSOCIO'] }}"
                            @if($dataSocio)
                                {{ $dataSocio[0]['ID_TIPOSOCIO'] === $value['ID_TIPOSOCIO'] ? 'selected' : '' }}
                                    @endif>{{ ucwords(\Illuminate\Support\Str::lower($value['TIPO_SOCIO'])) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Cuota de Aporte</label>
                    <input type="text" name="cuotaAporte" class="form-control form-control-md" placeholder="Ingrese la cuota de aporte" value="{{ $dataSocio ? $dataSocio[0]['CUOTA_APORTE'] : '' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Observaciones</label>
                    <input type="text" name="textObservacion" class="form-control form-control-md" placeholder="Ingrese una breve observacion" value="{{ $dataSocio ? $dataSocio[0]['OBSERVACIONES'] : '' }}">
                    <input type="hidden" name="personaID" value="{{ $idPersona }}">
                    <input type="hidden" name="socioID" value="{{ $dataSocio ? $dataSocio[0]['ID_SOCIO'] : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <div class="text-center mr-b-30">
                        <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger formError d-none"></div>
            </div>
        </div>
    </div>
</form>
<script src="{!! asset('js/form/formSocios.js?version='.date('YmdHis')) !!}"></script>
<script>
    initDateRangePicker('.datepicker', {
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    })

    initSelectPicker('.selectpicker', {
        style: "btn-default btn-sm"
    })

    vmFront.nameRoute = ucwords('Datos del Socio')
</script>