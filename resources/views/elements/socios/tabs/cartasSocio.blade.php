<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-heading widget-heading-border">
            <h5 class="widget-title">&nbsp;</h5>
            <div class="widget-actions">
                <a href="javascript:void(0)" onclick="responseModal('div.dialogGrayCoopLarge','formCartaBeneficiario',{ idSocio: '{{ $dataSocio[0]['ID_SOCIO'] }}' },'GET')" data-toggle="modal" data-target="#modalScore" class="badge bg-primary px-3 heading-font-family animated bounceIn">
                    <i class="list-icon fa fa-plus" aria-hidden="true"></i> Agregar Carta Beneficiario
                </a>
            </div>
        </div>
        <div class="widget-body">
            <table id="listCartasBeneficiarios" class="table dt-responsive nowrap" cellspacing="0" width="100%">
                <thead class="bg-primary">
                <tr>
                    <th>#</th>
                    <th>Nº Carta</th>
                    <th>Fecha Carta</th>
                    <th>Acciones</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script>
    function loadCartasDatatable(){
        dataTables('listCartasBeneficiarios', '/listCartasBeneficiarios', { idSocio: '{{ $dataSocio[0]['ID_SOCIO'] }}' }, 'GET')
        vmFront.nameRoute = ucwords('Cartas Beneficiarios')
    }
</script>