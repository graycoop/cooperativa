<div class="text-center">
    <div class="col-md-12">
        <figure class="thumb-md" style="display: block !important;">
            <img src="{!! asset(Cookie::get('cookie_avatar')) !!}?version={!! date('YmdHis') !!}" class="rounded-circle" alt="">
        </figure>
        <h5 class="mt-0 mb-0 side-user-heading fw-500">{{ Cookie::get('cookie_username') }}</h5>
    </div>
</div>