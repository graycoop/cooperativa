<div class="chat-panel" hidden>
    <div class="card">
        <div class="card-header d-flex justify-content-between"><a href="javascript:void(0);"><i class="feather feather-message-square text-success"></i></a>  <span class="user-name heading-font-family fw-400">John Doe</span>
            <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="widget-chat-activity flex-1">
                <div class="messages scrollbar-enabled suppress-x">
                    <div class="message media reply">
                        <figure class="thumb-xs2 user--online">
                            <a href="#">
                                <img src="img/avatars/user_default.png" class="rounded-circle">
                            </a>
                        </figure>
                        <div class="message-body media-body">
                            <p>Epic Cheeseburgers come in all kind of styles.</p>
                        </div>
                        <!-- /.message-body -->
                    </div>
                    <!-- /.message -->
                    <div class="message media">
                        <figure class="thumb-xs2 user--online">
                            <a href="#">
                                <img src="img/avatars/user_default.png" class="rounded-circle">
                            </a>
                        </figure>
                        <div class="message-body media-body">
                            <p>Cheeseburgers make your knees weak.</p>
                        </div>
                        <!-- /.message-body -->
                    </div>
                    <!-- /.message -->
                    <div class="message media reply">
                        <figure class="thumb-xs2 user--offline">
                            <a href="#">
                                <img src="img/avatars/user_default.png" class="rounded-circle">
                            </a>
                        </figure>
                        <div class="message-body media-body">
                            <p>Cheeseburgers will never let you down.</p>
                            <p>They'll also never run around or desert you.</p>
                        </div>
                        <!-- /.message-body -->
                    </div>
                    <!-- /.message -->
                    <div class="message media">
                        <figure class="thumb-xs2 user--online">
                            <a href="#">
                                <img src="img/avatars/user_default.png" class="rounded-circle">
                            </a>
                        </figure>
                        <div class="message-body media-body">
                            <p>A great cheeseburger is a gastronomical event.</p>
                        </div>
                        <!-- /.message-body -->
                    </div>
                    <!-- /.message -->
                    <div class="message media reply">
                        <figure class="thumb-xs2 user--busy">
                            <a href="#">
                                <img src="img/avatars/user_default.png" class="rounded-circle">
                            </a>
                        </figure>
                        <div class="message-body media-body">
                            <p>There's a cheesy incarnation waiting for you no matter what you palete preferences are.</p>
                        </div>
                        <!-- /.message-body -->
                    </div>
                    <!-- /.message -->
                    <div class="message media">
                        <figure class="thumb-xs2 user--online">
                            <a href="#">
                                <img src="img/avatars/user_default.png" class="rounded-circle">
                            </a>
                        </figure>
                        <div class="message-body media-body">
                            <p>If you are a vegan, we are sorry for you loss.</p>
                        </div>
                        <!-- /.message-body -->
                    </div>
                    <!-- /.message -->
                </div>
                <!-- /.messages -->
            </div>
            <!-- /.widget-chat-acitvity -->
        </div>
        <!-- /.card-body -->
        <form action="javascript:void(0)" class="card-footer" method="post">
            <div class="d-flex justify-content-end"><i class="feather feather-plus-circle list-icon my-1 mr-3"></i>
                <textarea class="border-0 flex-1" rows="1" style="resize: none" placeholder="Type your message here"></textarea>
                <button class="btn btn-sm btn-circle bg-transparent" type="submit"><i class="feather feather-arrow-right list-icon fs-26 text-success"></i>
                </button>
            </div>
        </form>
    </div>
    <!-- /.card -->
</div>