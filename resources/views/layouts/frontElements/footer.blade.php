<div class="sidebar-module nav-contact-info mx-3 mt-4 mb-3">
    <div class="d-flex flex-column align-items-center">
        <i class="feather feather-shield fs-24 color-color-scheme"></i>
    </div>
    <!-- /.d-flex -->
    <div class="contact-info-body bg-content-color-contrast text-center">
        <p class="lh-20">@lang('custom.copyright') © {{ \Carbon\Carbon::now()->format('Y') }}
            <br><a class="text-primary" href="">Graycoop</a>
        </p>
    </div>
    <!-- /.contact-info-body -->
</div>