<div class="card">
    <header class="card-header d-flex align-items-center mb-0">
        <a href="javascript:void(0);">
            <i class="feather feather-bell color-color-scheme" aria-hidden="true"></i>
        </a>
        <span class="heading-font-family flex-1 text-center fw-400">Notifications</span>
        <a href="javascript:void(0);">
            <i class="feather feather-settings color-content"></i>
        </a>
    </header>
    <ul class="card-body list-unstyled dropdown-list-group">
        <li><a href="#" class="media"><span class="d-flex thumb-xs"><img src="img/avatars/user_default.png" class="rounded-circle" alt=""> </span><span class="media-body"><span class="heading-font-family media-heading">Dany Miles </span><span class="media-content">commented on your photo</span> <span class="user--online float-right my-auto"></span></span></a>
        </li>
        <li><a href="#" class="media"><span class="d-flex thumb-xs"><img src="img/avatars/user_default.png" class="rounded-circle" alt=""> </span><span class="media-body"><span class="heading-font-family media-heading">Emily Woodworth </span><span class="media-content">posted a photo on your wall.</span></span></a>
        </li>
        <li><a href="#" class="media"><span class="d-flex thumb-xs"><img src="img/avatars/user_default.png" class="rounded-circle" alt=""> </span><span class="media-body"><span class="heading-font-family media-heading">Palmer Kate </span><span class="media-content">just mentioned you in his post</span></span></a>
        </li>
    </ul>
    <footer class="card-footer text-center">
        <a href="javascript:void(0);" class="content-font-family text-uppercase fs-13">See all activity</a>
    </footer>
</div>