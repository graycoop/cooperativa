<a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown">
    <span class="avatar thumb-xs2">
        <img v-bind:src="userInformation.avatar ? getAvatar : 'img/loading_avatar.gif'" class="rounded-circle" alt="">
        <i class="feather feather-chevron-down list-icon"></i>
    </span>
</a>
<div class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
    <div class="card">
        <header class="card-header d-flex mb-0">
            <a href="javascript:void(0);" class="col-md-4 text-center">
                <i class="feather feather-user-plus align-middle"></i>
            </a>
            <a href="javascript:void(0);" class="col-md-4 text-center">
                <i class="feather feather-settings align-middle"></i>
            </a>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="col-md-4 text-center" data-toggle="tooltip" data-placement="bottom" title="Cerrar Sesión">
                <i class="feather feather-power align-middle"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </header>
    </div>
</div>