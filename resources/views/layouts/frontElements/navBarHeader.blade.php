<ul class="nav navbar-nav d-none d-lg-flex ml-2">
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown">
            <i class="feather feather-activity list-icon"></i>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card animated flipInY">
            @include('layouts.frontElements.header.notification')
        </div>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle ripple" data-toggle="dropdown">
            <i class="feather feather-alert-octagon list-icon"></i>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card animated flipInY">
            @include('layouts.frontElements.header.alerts')
        </div>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle ripple" data-toggle="dropdown">
            <i class="feather feather-globe list-icon"></i>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card animated flipInY">
            @include('vendor.language.frontFlags')
        </div>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle right-sidebar-toggle ripple" data-toggle="dropdown">
            <i class="feather feather-message-circle list-icon"></i>
        </a>
    </li>
</ul>

<ul class="nav navbar-nav d-lg-flex ml-2">
    <li class="dropdown">
        @include('layouts.frontElements.header.user')
    </li>
</ul>