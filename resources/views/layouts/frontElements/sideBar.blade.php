<aside class="site-sidebar scrollbar-enabled clearfix" data-suppress-scroll-x="true">
    @include('layouts.frontElements.sideBar.user')
    @include('layouts.frontElements.sideBar.menu')
    @include('layouts.frontElements.footer')
</aside>