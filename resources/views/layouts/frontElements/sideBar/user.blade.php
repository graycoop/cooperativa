<div class="side-user">
    <div class="col-sm-12 text-center p-0 clearfix">
        <div class="d-inline-block pos-relative mr-b-10">
            <a href="javascript:void(0)" onclick="responseModal('div.dialogGrayCoop','/formAvatar', {}, 'GET')" data-toggle="modal" data-target="#modalScore">
                <figure class="thumb-sm mr-b-0 user--online" data-toggle="tooltip" data-placement="right" title="Cambiar Avatar">
                    <img v-bind:src="userInformation.avatar ? getAvatar : 'img/loading_avatar.gif'" class="rounded-circle" alt="">
                </figure>
            </a>
            <a href="javascript:void(0)" class="text-muted side-user-link">
                <i class="feather feather-settings list-icon"></i>
            </a>
        </div>
        <div class="lh-14 mr-t-5"><a href="javascript:void(0)" class="hide-menu mt-3 mb-0 side-user-heading fw-500">{{ Auth::user()->full_name }}</a>
            <br><small class="hide-menu">{{ ucwords(Auth::user()->roles[0]->name) }}</small>
        </div>
    </div>
</div>