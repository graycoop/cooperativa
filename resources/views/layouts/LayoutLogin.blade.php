<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Graycoop</title>
        <link rel="icon" type="image/png" href="{!! asset('favicon.png?version='.date('YmdHis'))!!}" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no ,shrink-to-fit=no" name="viewport">
        <link href="{!! asset('css/securitec_login.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/login.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
        <link href="{!! asset('css/pace.css?version='.date('YmdHis'))!!}" rel="stylesheet" type="text/css">
    </head>
    <body class="body-bg-full profile-page">
        <div id="wrapper" class="wrapper">
            <div class="container mt-5">
                <div class="row">
                    <img class="logo-expand" alt="" src="{!! asset('img/logo-front.png?version='.date('YmdHis')) !!}">
                </div>
                @include('vendor.language.loginFlags')
                <div class="row mt-5">
                    <div class="col text-white d-none d-xl-block mt-lg-5">
                        <h1>@lang('custom.auth.phrase.textPhrase')</h1>
                        <h5>- <i class="fa fa-user"></i> @lang('custom.auth.phrase.autorPhrase')</h5>
                    </div>
                    <div class="col">
                        <div class="login-center">
                            @yield('content')
                        </div>
                        <footer class="col-sm-12 text-white text-center">
                            <p>
                                @lang('custom.copyright') © {{ \Carbon\Carbon::now()->format('Y') }} <a href="" class="text-primary m-l-5"><b>Graycoop</b></a>
                            </p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
        <script src="{!! asset('assets/utilities/popper/popper.min.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/app_score.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/node_login.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/helper_login.js?version='.date('YmdHis'))!!}"></script>
        <script>
            var textLoading = '@lang("auth.formAuth.loading")'
        </script>
    </body>
</html>