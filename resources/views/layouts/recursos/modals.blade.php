<!-- Modal Score -->
<div id="modalScore" class="modal fade" role="dialog">
    <div class="modal-dialog dialogGrayCoop"></div>
    <div class="modal-dialog dialogGrayCoopSmall modal-sm"></div>
    <div class="modal-dialog dialogGrayCoopLarge modal-lg"></div>
    <div class="modal-dialog dialogGrayCoopExtraLarge modal-xl"></div>
</div>