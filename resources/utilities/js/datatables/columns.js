/**
 * Created by Carlos on 15/12/2017.
 *
 * [columnsDatatable description]
 * @routes El ID de la tabla
 * @return Devuelve las columnas a tomarse
 */
const columnsDatatable = (route) => {
    let columns = ''
    if (route === 'listRoles') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'name', name: 'name', width: '120px'},
            {data: 'description', name: 'description'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listUsers') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'avatar', name: 'avatar', orderable: false, searchable: false, width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listPersonas') {
        columns = [
            {data: 'id_persona', name: 'id_persona'},
            {data: 'nombres', name: 'nombres', orderable: false},
            {data: 'nro_documento', name: 'nro_documento'},
            {data: 'fec_nacimiento', name: 'fec_nacimiento'},
            {data: 'tipo_sexo', name: 'tipo_sexo'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listTelefonos') {
        columns = [
            {data: 'id_telefono', name: 'id_telefono'},
            {data: 'telefono', name: 'telefono'},
            {data: 'anexo', name: 'anexo'},
            {data: 'operador', name: 'operador'},
            {data: 'discado', name: 'discado'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listEmails') {
        columns = [
            {data: 'id_email', name: 'id_email'},
            {data: 'email', name: 'email'},
            {data: 'tipo', name: 'tipo'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listDireccion') {
        columns = [
            {data: 'id_direccion', name: 'id_direccion'},
            {data: 'direccion', name: 'direccion'},
            {data: 'tipo', name: 'tipo'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listCentroLaboral') {
        columns = [
            {data: 'id_centro_laboral', name: 'id_centro_laboral'},
            {data: 'nombre', name: 'nombre'},
            {data: 'tipo', name: 'tipo'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listOrganizacion') {
        columns = [
            {data: 'id_organizacion', name: 'id_organizacion'},
            {data: 'nombre', name: 'nombre'},
            {data: 'ruc', name: 'ruc'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listSocios') {
        columns = [
            {data: 'id_persona', name: 'id_persona'},
            {data: 'nombres', name: 'nombres', orderable: false},
            {data: 'nro_documento', name: 'nro_documento'},
            {data: 'fec_nacimiento', name: 'fec_nacimiento'},
            {data: 'tipo_sexo', name: 'tipo_sexo'},
            {data: 'status_socio', name: 'status_socio'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listCartasBeneficiarios'){
        columns = [
            {data: 'id_carta', name: 'id_carta'},
            {data: 'nro_carta', name: 'nro_carta'},
            {data: 'fec_carta', name: 'fec_carta'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    return columns
}