/**
 * Created by Carlos on 15/12/2017.
 */
$('#formRole').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : 'saveFormRoles',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                let action = (data.action === 'create' ? 'agrego' : 'edito')
                alertaSimple('', 'Se '+ action +' correctamente el perfil', 'success', 2000)
                clearModal('modalScore', 'div.dialogGrayCoop')
                reloadDatatable(data.datatable)
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})

$('#formRolesAssing').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : 'saveFormRolesAssing',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                alertaSimple('', 'Se asignaron correctamente los perfiles', 'success', 2000)
                clearModal('modalScore', 'div.dialogGrayCoopExtraLarge')
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})

$('#formRoleMenu').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : 'saveFormRolesMenu',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                alertaSimple('', 'Se actualizaron los menú del Perfil correctamente', 'success', 2000)
                clearModal('modalScore', 'div.dialogGrayCoopExtraLarge')
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})