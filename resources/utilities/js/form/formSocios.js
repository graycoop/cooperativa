$('#formSearchSocios').submit(function(e) {
    e.preventDefault()
    let data = $(this).serializeArray()
    $('#containerLoading').removeClass('d-none')
    $('#containerTableSocios').addClass('d-none')
    dataTables('listSocios', '/listSocios', data, 'GET')
})

$('#formDatosSocios').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : 'saveFormDatosSocios',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                let action = (data.action === 'create' ? 'agrego' : 'edito')
                alertaSimple('', 'Se '+ action +' correctamente a al socio', 'success', 2000)
                if(data.action === 'create') vmFront.loadOptionMenu('formSocios', { idPersona : data.idSocio })
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})

$('#formCartaBeneficiario').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : 'saveFormCartaBeneficiario',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                let action = (data.action === 'create' ? 'agrego' : 'edito')
                alertaSimple('', 'Se '+ action +' correctamente la carta beneficiaria', 'success', 2000)
                clearModal('modalScore', 'div.dialogGrayCoopLarge')
                reloadDatatable(data.datatable)
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})