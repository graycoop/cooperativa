/**
 * Created by Carlos on 15/12/2017.
 *
 * [dataTables description]
 * @nombreDIV ID de la tabla
 * @routes Ruta de donde se tomara los datos
 * @return Estructura el Datatable ah tomarse
 */
const dataTables = (nombreDIV, routes, parameters = {}, typeRequest = 'POST') => {
    // Eliminación del DataTable en caso de que exista
    $(`#${nombreDIV}`).dataTable().fnDestroy()
    // Creacion del DataTable
    $(`#${nombreDIV}`).DataTable({
        'deferRender': true,
        'processing': true,
        'serverSide': true,
        'responsive': true,
        'ajax': {
            url: routes,
            type: typeRequest,
            data: parameters
        },
        'paging': true,
        'pageLength': 10,
        'lengthMenu': [10, 20, 40, 50, 100, 200, 300, 400, 500],
        'language': dataTables_lang(),
        'columns': columnsDatatable(nombreDIV)
    })
}