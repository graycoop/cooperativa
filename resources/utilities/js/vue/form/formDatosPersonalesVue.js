'use strict'

var vmFormDatosPersonalesData = {
    idPersona: ''
}

var vmFormDatosPersonales = new Vue({
    el: "#formVueDatosPersonales",
    data: vmFormDatosPersonalesData,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                let response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        changeTipoPersona: async function(e){
            $('#loadFormDatosPersonales').html(`<div class="row"><div class="widget-holder col-md-12"><div class="widget-bg"><div class="widget-body"><div class="cssload-loader"></div></div></div></div></div>`)
            $('#loadFormDatosPersonales').html(await this.sendUrlRequest(`/formTipoPersona`, { idTipoPersona: e.target.value, idPersona: this.idPersona }, 'get'))
        },
        loadFormTipoPersona: async function(tipoPersona, idPersona = null){
            $('#loadFormDatosPersonales').html(`<div class="row"><div class="widget-holder col-md-12"><div class="widget-bg"><div class="widget-body"><div class="cssload-loader"></div></div></div></div></div>`)
            $('#loadFormDatosPersonales').html(await this.sendUrlRequest(`/formTipoPersona`, { idTipoPersona: tipoPersona, idPersona: idPersona }))
        }
    }
})