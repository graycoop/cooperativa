'use strict'

var vmFormDireccionData = {
    idDepartamento: '',
    idProvincia: '',
    idDistrito: '',
    selectProvincia: '',
    selectDistrito: ''
}

var vmFormDireccion = new Vue({
    el: "#formVueDireccion",
    data: vmFormDireccionData,
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                let response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        changeDepartamento: async function(idDepartamento, selectProvincia = null){
            this.idDepartamento = idDepartamento
            this.selectProvincia = selectProvincia
            $('#selectProvincia').addClass('d-none')
            $('#selectDistrito').addClass('d-none')
            $('#selectLoadingProvincia').removeClass('d-none')
            $('#selectLoadingDistrito').removeClass('d-none')
            $('#selectProvincia').html(await this.sendUrlRequest(`/selectProvincia`, { idDepartamento: this.idDepartamento, selectProvincia: this.selectProvincia }))
            $('#selectLoadingProvincia').addClass('d-none')
            $('#selectProvincia').removeClass('d-none')
        },
        changeProvincia: async function(idProvincia, selectDistrito = null) {
            this.idProvincia = idProvincia
            this.selectDistrito = selectDistrito
            $('#selectDistrito').addClass('d-none')
            $('#selectLoadingDistrito').removeClass('d-none')
            $('#selectDistrito').html(await this.sendUrlRequest(`/selectDistrito`, { idDepartamento: this.idDepartamento, idProvincia: this.idProvincia, selectDistrito: this.selectDistrito }))
            $('#selectLoadingDistrito').addClass('d-none')
            $('#selectDistrito').removeClass('d-none')
        }
    }
})