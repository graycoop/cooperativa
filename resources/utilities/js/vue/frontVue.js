'use strict'

var objFrontData = {
    userInformation: '',
    menu: [],
    utilitiesUser: {
        totalUsers: '',
        totalUsersMonth: '',
        lastUser: ''
    },
    nameRoute: ''
}

var vmFront = new Vue({
    el: '#wrapper',
    data: function() {
        return objFrontData;
    },
    computed: {
        getAvatar: function(){
            return `${this.userInformation.avatar}?version=${moment().format('YmdHis')}`
        }
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                let response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        loadOptionMenu: async function (idMenu, parameterMenu = {}, typeRequest = 'post') {
            $('#container').html(`<div class="row"><div class="widget-holder col-md-12"><div class="widget-bg"><div class="widget-body"><div class="cssload-loader"></div></div></div></div></div>`)
            $('#container').html(await this.sendUrlRequest(`/${idMenu}`, parameterMenu, typeRequest))
        },
        getUserInformation: async function () {
            let userInformation = await this.sendUrlRequest('/getUserInformation', {}, 'get')
            this.userInformation = userInformation[0]
        },
        getUserMenu: async function () {
            let userMenu = await this.sendUrlRequest('/getUserMenu', {}, 'get')
            let menu = this.menu
            userMenu.forEach(function(value) {
                menu.push(value.vue_name)
            })
        },
        getUtilitiesUser: async function() {
            let userUtilites = await this.sendUrlRequest('/getUtilitiesUser', {}, 'get')
            this.utilitiesUser.lastUser = userUtilites.last.username
            this.utilitiesUser.totalUsers = userUtilites.total
            this.utilitiesUser.totalUsersMonth = userUtilites.total_month
        },
        loadAllMethods: function() {
            this.getUserInformation()
            this.getUserMenu()
            this.loadOptionMenu('blankPage', {}, 'get')
        }
    }
})