'use strict'

var vmUserUtilitiesData = {
    utilitiesUser: {
        totalUsers: '',
        totalUsersMonth: '',
        lastUser: ''
    }
}

var vmUserUtilities = new Vue({
    el: '#userUtilities',
    data: vmUserUtilitiesData,
    mounted: function () {
        this.getUtilitiesUser()
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}, typeRequest = 'post') {
            try {
                let response = await axios[typeRequest](`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        getUtilitiesUser: async function() {
            let userUtilites = await this.sendUrlRequest('/getUtilitiesUser', {}, 'get')
            this.utilitiesUser.lastUser = userUtilites.last.username
            this.utilitiesUser.totalUsers = userUtilites.total
            this.utilitiesUser.totalUsersMonth = userUtilites.total_month
        }
    }
})