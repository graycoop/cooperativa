'use strict'

var vmChatData = {
    listChat: []
}

var vmChat = new Vue({
    el: '#chatVue',
    data: vmChatData,
    mounted: function () {
        this.getChat()
    },
    computed: {
        isUser() {
            return this.listChat.map(function(item) {
                let replyUser = (item.users_chat.id === vmFront.userInformation.id ? 'message reply media' : 'message media')
                return replyUser
            })
        },
        isUserConnected() {
            return this.listChat.map(function(item) {
                let replyUser = (item.users_chat.session_id ? 'avatar thumb-xs2 mr-b-0 user--online' : 'avatar thumb-xs2 mr-b-0 user--offline')
                return replyUser
            })
        }
    },
    methods: {
        getChat: async function() {
            this.listChat = await vmFront.sendUrlRequest('/getChat', {}, 'get')
            setTimeout(function() { $('#chatContainer').scrollTop($("#chatContainer")[0].scrollHeight) }, 1)
        }
    }
})