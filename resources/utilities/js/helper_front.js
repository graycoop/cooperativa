// Agrega por defecto el crsf token en las peticiones ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

$(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' })
    $("body").popover({
        selector: '[data-toggle=popover]',
        html : true,
        content: function() {
            let content = $(this).attr("data-popover-content")
            return $(content).children(".popover-body").html()
        },
        title: function() {
            let title = $(this).attr("data-popover-content")
            return $(title).children(".popover-heading").html()
        }
    })
    $(document).on("click", ".popover .close" , function(){
        $(this).parents(".popover").popover('hide')
    })
    currentTime()
})

const alertaSimple = (titleAlerta = '', textoAlerta, tipoAalerta = null, tiempoAlerta = 2000, outsideClick = false, escapeKey = false, enterKey = false) => {
    swal({
        title: titleAlerta,
        html: textoAlerta,
        type: tipoAalerta,
        showConfirmButton: false,
        showCancelButton: false,
        allowOutsideClick: outsideClick,
        allowEscapeKey: escapeKey,
        allowEnterKey: enterKey,
        timer: tiempoAlerta
    }).then(
        function () {},
        function (dismiss) { }
    )
}

const responseModal = (nameRoute, routeLoad, parameters = {}, typeRequest = 'POST') => {
    let imageLoading = `<div class="cssload-loader"></div>`
    let token = $('meta[name="csrf-token"]').attr('content')
    $.ajax({
        type : typeRequest,
        url : routeLoad,
        data: parameters,
        beforeSend: function(){
            $(nameRoute).html(imageLoading)
        },
        success:function(data){
            $(nameRoute).html(data)
        }
    })
}

// Función para cerrar un modal y limpiar el cuerpo del mismo (Funciona con la función responseModal)
const clearModal = (modalID, bodyModal) => {
    $(bodyModal).html('').promise().done(function() {
        $('#'+modalID).modal('toggle')
    })
}

// Función para escuchar el evento cuando se cierra un modal y limpiar el cuerpo del mismo (Funciona con la función responseModal)
const clearModalClose = (modalID, bodyModal) => {
    $('#'+modalID).on('hidden.bs.modal', function () {
        $(bodyModal).html('')
    })
}

// Función que muestra un boton de carga cuando se ejecuta un formulario
const changeButtonForm = (btnShow, btnHide) => {
    $('.' + btnHide).removeClass('d-none')
    $('.' + btnShow).addClass('d-none')
}

// Función que muestra los errores que emite el controlador en un DIV
const showErrorForm = (data, formDiv) => {
    let errors = ''
    for(datos in data.responseJSON){
        errors += '<strong>' + data.responseJSON[datos] + '</strong><br>'
    }
    $(formDiv).fadeIn().html(
        '<span class="fa fa-close"></span> <strong>Error</strong>'+
        '<hr class="message-inner-separator">'+
        '<span>' + errors + '</span>'
    )
    $(formDiv).removeClass('d-none')
}

// Función para ocultar los errores cuando se ingresa en algun campo que se requeria data
const hideErrorForm = (formDiv) => {
    $("input[type=text],input[type=checkbox],input[type=password]").click(function() {
        $(formDiv).fadeOut().html()
    })
}

// Funcion para reutilizar el dropzone
const dropzoneAvatarInit = (message = '', url = '', alertMessage = '', modalID = '') => {
    $("#formAvatarUpload").dropzone({
        uploadMultiple: false,
        autoDiscover: false,
        maxFilesize: 2,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: 'image/*',
        dictDefaultMessage: message,
        url: url,
        params:  { _token: $('meta[name="csrf-token"]').attr('content') },
        thumbnailWidth: null,
        thumbnailHeight: null,
        init: function() {
            this.on("addedfile", function() {
                $('.dz-preview').css({"display":"block", "margin":"0 auto"})
                $('.dz-image').css({"width":"100%", "height":"auto"})
            }),
            this.on("thumbnail", function() {
                $('.dz-image').last().find('img').attr({width: '100%', height: '100%'})
            }),
            this.on("success", function(file, response) {
                $('.dz-preview').css({"display":"block", "margin":"0 auto"})
                $('.dz-image').css({"width":"100%", "height":"auto"})
                alertaSimple('', alertMessage, 'success')
                $(modalID).modal('toggle')
                vmFront.userInformation.avatar = ''
                vmFront.getUserInformation()
            })
        }
    })
}

const reloadDatatable = (idTable) => {
    return $(`#${idTable}`).DataTable().ajax.reload( null, false )
}

// Función para poder buscar en una tabla
const searchTable = (idTable, idSearch, filterAnnexed = '') => {
    $(idSearch).keyup(function(){
        _this = this
        $.each($(idTable + " tbody tr " + filterAnnexed), function() {
            let value = -1
            let valueSearch = $(_this).val()
            if($(this).text().toLowerCase().indexOf(valueSearch.toLowerCase()) === value) {
                $(this).hide()
            }else {
                $(this).show()
            }
        })
    })
}

// Función para seleccionar todos los checkbox con uno solo
const mark_all = (checkGeneral) => {
    if($(checkGeneral).is(':checked')){
        $("input.checkNew:checkbox").prop("checked", "checked")
        $('tr.trNew').addClass('table-warning')
        $('select.selectNew').removeAttr("disabled").val('1')
    }else{
        $("input.checkNew:checkbox").prop("checked", false)
        $('tr.trNew').removeClass('table-warning').addClass('info')
        $('select.selectNew').attr("disabled", true).val('-')
    }
}

// Función que deshabilita y coloca estilo a la fila de la tabla
const markCheck = (check) => {
    if($('#checkbox_'+check).is(':checked')){
        $('#tr_'+check).addClass('table-warning')
        $('#select_'+check).removeAttr("disabled").val('1')
    }else{
        $('#tr_'+check).removeClass('table-warning').addClass('info')
        $('#select_'+check).attr("disabled", true).val('-')
    }
}

const mark_all_menu = (checkGeneral, checkChildren) => {
    if($(checkGeneral).is(':checked')){
        $("input."+checkChildren+":checkbox").prop("checked", "checked")
    }else{
        $("input."+checkChildren+":checkbox").prop("checked", false)
    }
}

const initDatePicker = (idInput, options = {}) => {
    $(`${idInput}`).datepicker(options)
}

const initDateRangePicker = (idInput, options = {}) => {
    $(`${idInput}`).daterangepicker(options)
}

const initSelectPicker = (idInput, options = {}) => {
    $(`${idInput}`).selectpicker(options)
}

const markReadOnly = (checkRead, inputRead, inputRecover) => {
    if($(checkRead).is(':checked')){
        $(inputRead).val('')
        $(inputRead).prop("readonly",false)
    }else{
        $(inputRead).val($(inputRecover).val())
        $(inputRead).prop("readonly",true)
    }
}

/* Fix para el Datatable con Tabs */
$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust()
} )

const ucwords = (str) => {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase()
    })
}

const currentTime = () => {
    let today = moment()
    today.locale(vmFront.userInformation['locale'])
    let days = ucwords(today.format('DD'))
    let month = ucwords(today.format('MMMM'))
    let years = ucwords(today.format('YYYY'))
    let hours = today.hours()
    let minutes = today.minutes()
    let seconds = today.seconds()
    minutes = checkTime(minutes)
    seconds = checkTime(seconds)
    $('#dateServer').html(`${days} de ${month} del ${years}`)
    $('#hourServer').html(`${hours}:${minutes}:${seconds}`)
    let dateServer = setTimeout(currentTime, 500)
}

const checkTime = (i) => {
    if (i < 10) i = "0" + i
    return i
}

const showImagePreview = (input, divShowImage, avatarOriginalProveedor = '') => {
    if (input.files && input.files[0]) {
        let filerdr = new FileReader()
        filerdr.onload = function (e) {
            $(`${divShowImage}`).attr('src', e.target.result)
            $(`input[name=${avatarOriginalProveedor}]`).val('')
        }
        filerdr.readAsDataURL(input.files[0])
    }
}

const emojiArea = (idInput, options = {}) => {
    $(`${idInput}`).emojioneArea(options)
}

const checkVisible = (checkID, inputID, inputID2) => {
    if($(checkID).is(':checked')){
        $(inputID).addClass('d-none')
        $(inputID2).removeClass('d-none')
    }else{
        $(inputID2).addClass('d-none')
        $(inputID).removeClass('d-none')
    }
}