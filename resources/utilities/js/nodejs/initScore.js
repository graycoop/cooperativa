const socketNodejs = io.connect(serverScoreNode, {
    'reconnection': true,
    'reconnectionAttempts': 15,
    'reconnectionDelay': 9000,
    'reconnectionDelayMax': 9000
})

socketNodejs.on('connect', function () {
    swal.close()
    vmFront.loadAllMethods()
    console.log('socketNodejs connected!')
})

socketNodejs.on('connect_error', function () {
    swal.close()
    let i = 9
    let refreshIntervalId = setInterval(() => {
        alertaSimple('', `Fallo la conexión con el socket del Server NodeJS volveremos a reintentar en ${i} segundos!!!`, 'error', null, true, true, true)
        i--
        if (i === 0) clearInterval(refreshIntervalId)
    }, 1000)
    console.log('socketNodejs Connection Failed')
})

socketNodejs.on('disconnect', function () {
    alertaSimple('', 'Acabas de perder conexión con el socket del Server Nodejs !!!', 'error', null, true, true, true)
    console.log('socketNodejs Disconnected')
})