<?php
return [
    'auth' => [
        'phrase' => [
            'textPhrase' => 'If the opportunity does not call, build a door',
            'autorPhrase' => 'Milton Berle'
        ]
    ],

    'current' => [
        'language' => 'Current Language'
    ],

    'choose' => [
        'language' => 'Choose your Language'
    ],

    'copyright' => 'All rights reserved',
];