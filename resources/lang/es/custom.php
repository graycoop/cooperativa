<?php
return [
    'auth' => [
        'phrase' => [
            'textPhrase' => 'Si la oportunidad no llama, contruye una puerta',
            'autorPhrase' => 'Milton Berle'
        ]
    ],

    'current' => [
        'language' => 'Idioma Actual'
    ],

    'choose' => [
        'language' => 'Escoge tu Idioma'
    ],

    'copyright' => 'Todos los derechos reservados',
];